import styled from "styled-components";

const StoryContainer = styled.div`
  position: absolute;
  top: 10%;
  left: 5%;
  width: 70%;

  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  align-items: left;

  font-family: "IBM3270";

  overflow: scroll;

  color: white;
`;

const StoryHeaderContainer = styled.div``;

const StoryTitle = styled.div`
  font-size: 40px;

  margin: 5px 0px;
`;

const StoryAuthor = styled.div`
  font-size: 20px;

  margin: 5px 0px;
`;

const StoryTags = styled.div``;

const StoryContent = styled.div`
  margin: 0 auto;
`;

const StoryBackButton = styled.a`
  display: block;

  position: fixed;
  bottom: 5%;
  right: 10%;

  color: red;
  text-decoration: none;
`;

export {
  StoryContainer,
  StoryHeaderContainer,
  StoryTitle,
  StoryAuthor,
  StoryTags,
  StoryContent,
  StoryBackButton,
};
