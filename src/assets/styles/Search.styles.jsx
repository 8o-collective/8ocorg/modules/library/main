import React, { useState, useEffect } from "react";
import styled from "styled-components";

const SearchContainer = styled.div`
  position: fixed;
  top: 10%;
  right: 0%;
  width: 30%;
  height: 50%;

  border: 1px solid red;

  font-family: "IBM3270";
`;

const SearchDropdownContainer = styled.div`
  font-size: 20px;
  color: white;
`;

const SearchDropdownHeader = styled.div`
  background-color: black;

  border: 1px solid red;

  padding: 10px;

  cursor: pointer;
`;

const SearchDropdownListContainer = styled.div`
  display: flex;
  flex-direction: column;

  background-color: black;
  border: 1px solid red;

  position: absolute;
  width: 100%;
  top: 0px;
  left: 0px;

  z-index: 1;
`;

const SearchDropdownListItem = styled.div`
  color: white;

  padding: 10px;
`;

const SearchDropdown = ({ header, item, items, setItem }) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggling = () => setIsOpen(!isOpen);

  return (
    <SearchDropdownContainer>
      <SearchDropdownHeader onClick={toggling}>
        {item || header}
      </SearchDropdownHeader>
      {isOpen && (
        <SearchDropdownListContainer>
          {items.map((item, index) => (
            <SearchDropdownListItem
              key={index}
              onClick={() => {
                setItem(item);
                setIsOpen(false);
              }}
            >
              {item}
            </SearchDropdownListItem>
          ))}
        </SearchDropdownListContainer>
      )}
    </SearchDropdownContainer>
  );
};

const SearchTagsContainer = styled.ul`
  display: block;

  position: absolute;
  top: 15%;
  left: 0%;
  width: 100%;
  height: 70%;

  margin: 0px;
  padding: 0px;

  columns: 2;

  color: white;
`;

const SearchTagsItem = styled.li`
  display: block;

  color: ${(props) => (props.selected ? "red" : "white")};
  padding: 5px;

  cursor: pointer;
`;

const SearchTags = ({ selectedTag, tags, setTag }) => (
  <SearchTagsContainer>
    {tags.map((tag) => (
      <SearchTagsItem
        key={tag}
        selected={tag === selectedTag}
        onClick={() => setTag(tag)}
      >
        [{tag}]
      </SearchTagsItem>
    ))}
  </SearchTagsContainer>
);

const SearchAuthorContainer = styled.div`
  position: absolute;
  top: 50%;
  left: 0%;
  width: 100%;

  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const SearchAuthorSliderInput = styled.input.attrs(() => ({
  type: "range",
}))`
  appearance: none;
  background: transparent;
  width: 100%;
  height: 10px;

  border: 1px solid red;

  &::-webkit-slider-thumb {
    appearance: none;
    background: transparent;
    width: 10px;
    height: 10px;
    border-radius: 0px;
    border: 1px solid red;
    cursor: pointer;
  }

  &::-moz-range-thumb {
    appearance: none;
    background: transparent;
    width: 10px;
    height: 10px;
    border-radius: 0px;
    border: 1px solid red;
    cursor: pointer;
  }
`;

const SearchAuthorText = styled.div`
  text-align: center;
  font-size: 20px;

  color: white;
`;

const SearchAuthor = ({ selectedAuthor, authors, setAuthor }) => {
  useEffect(() => {
    setAuthor(authors[Math.floor(authors.length / 2) - 1]);
  }, []);

  return (
    <SearchAuthorContainer>
      <SearchAuthorText>{selectedAuthor}</SearchAuthorText>
      <SearchAuthorSliderInput
        min={0}
        max={authors.length - 1}
        value={authors.indexOf(selectedAuthor)}
        onChange={(e) => setAuthor(authors[e.target.value])}
      />
    </SearchAuthorContainer>
  );
};

// text input
const SearchTitleInput = styled.input.attrs(() => ({
  type: "text",
  placeholder: "title...",
}))`
  position: absolute;
  top: 50%;
  left: 10%;

  width: 80%;
  height: 5%;

  border: 1px solid red;
  background-color: black;
  color: white;
  font-family: "IBM3270";
`;

const SearchClearButton = styled.div`
  position: absolute;
  bottom: 0%;
  right: 0%;

  padding: 10px;

  cursor: pointer;

  color: white;
  text-align: center;

  border: 1px solid red;
  border-bottom: none;
  border-right: none;
`;

export {
  SearchContainer,
  SearchDropdown,
  SearchTags,
  SearchAuthor,
  SearchTitleInput,
  SearchClearButton,
};
