import React from "react";
import styled, { keyframes } from "styled-components";

const BOOK_FRONT_TITLE_NUMERATOR = 300;

const PAGE_DIMENSION_MULTIPLIER = 0.95;

const FONT_RATIO = 1.7; // width of font is approx. 1/2 of height of font

const calculateFontSize = (width, height, length) => {
  // calculate monospace font-size based on width and height of container and length of text
  return Math.min((width / length) * FONT_RATIO, height);
};

const BookshelfContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  position: absolute;
  width: ${(props) => props.width}px;

  border: 1px solid red;
  border-bottom: none;
`;

const BookshelfShelfContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: end;
  align-items: end;

  position: relative;

  height: ${(props) => props.height}px;

  &:after {
    position: absolute;
    top: 100%;
    left: 0px;
    content: "";
    height: 10px;
    width: 100%;
    border: 1px solid red;
    border-left: none;
    border-top: none;
  }
`;

const rotateAnimation = () => keyframes`
  0% { 
    rotate: y 0deg; 
  }
  100% { 
    rotate: y -360deg;
  }
`;

const BookshelfBookDetailsContainer = styled.div.attrs((props) => ({
  style: {
    width: `${props.width}px`,
    height: `${props.height}px`,

    left: `calc(50vw - ${props.width / 2}px)`,

    [props.location === -1 ? "bottom" : "top"]: props.location <= 0 && "0px",

    transform: `translateY(${props.location > 0 ? "-25" : "0"}%)`,
  },
}))`
  display: none;

  position: absolute;

  writing-mode: horizontal-tb;

  transform-style: preserve-3d;
  animation: ${rotateAnimation} 6s infinite linear;
`;

const BookshelfBookDetailsSide = styled.div.attrs((props) => {
  const getSideStyle = () => {
    if (props.side === "spine") {
      return {
        width: `${props.depth}px`,
        height: `${props.height}px`,
        left: `${(props.width - props.depth) / 2}px`,

        lineHeight: `${props.depth}px`,
        writingMode: "vertical-lr",
        fontSize: `${calculateFontSize(
          props.height,
          props.depth,
          props.title.length
        )}px`,

        transform: `rotateY(-90deg) translateZ(${props.width / 2}px)`,
      };
    } else if (props.side === "pages") {
      return {
        width: `${props.depth - 2}px`,
        height: `${props.height * PAGE_DIMENSION_MULTIPLIER}px`,
        left: `${(props.width - props.depth) / 2}px`,
        bottom: `${
          (props.height - props.height * PAGE_DIMENSION_MULTIPLIER) / 2
        }px`,

        transform: `rotateY( 90deg) translateZ(${
          (PAGE_DIMENSION_MULTIPLIER * props.width) / 2
        }px)`,
      };
    } else if (props.side === "top" || props.side === "bottom") {
      return {
        width: `${props.width * PAGE_DIMENSION_MULTIPLIER - 2}px`,
        height: `${props.depth}px`,
        top: `${(props.height - props.depth) / 2}px`,

        transform: `rotateX(${
          props.side === "bottom" ? "-90" : "90"
        }deg) translateZ(${(PAGE_DIMENSION_MULTIPLIER * props.height) / 2}px)`,
      };
    } else if (props.side === "front" || props.side === "back") {
      const getTitleFontSize = () => {
        let titleFontSize = calculateFontSize(
          props.width,
          props.height,
          props.title
            .split(" ")
            .map((e) => e.length)
            .reduce(
              (longest, current) => (current > longest ? current : longest),
              0
            )
        );

        const bookFrontTitleThreshold =
          BOOK_FRONT_TITLE_NUMERATOR / props.title.split(" ").length;
        titleFontSize = Math.min(titleFontSize, bookFrontTitleThreshold);

        return titleFontSize;
      };

      return {
        width: `${props.width}px`,
        height: `${props.height}px`,

        transform: `rotateY(${
          props.side === "front" ? "0" : "180"
        }deg) translateZ(${props.depth / 2}px)`,

        fontSize: `${props.side === "front" ? getTitleFontSize() : "0"}px`,
      };
    }
  };

  return {
    style: getSideStyle(),
  };
})`
  position: absolute;

  background-color: black;

  font-size: 40px;
  font-weight: bold;

  border: 1px solid red;
  color: white;
  text-align: center;
`;

const BookshelfBookDetailsBinding = styled.div.attrs((props) => ({
  style: {
    [props.side === "front" ? "left" : "right"]: `${props.width / 20}px`,
  },
}))`
  position: absolute;
  content: "";
  width: 1px;
  height: 100%;
  background-color: red;
`;

const BookshelfBookDetailsTitle = styled.div`
  position: absolute;
  top: 10%;
  left: 50%;

  width: 80%;

  transform: translateX(-50%);

  text-align: center;

  font-size: 100%;
  line-height: 100%;
`;

const BookshelfBookDetailsAuthor = styled.div`
  position: absolute;
  bottom: 5%;
  right: 50%;

  width: 80%;

  transform: translateX(50%);

  text-align: right;
  font-style: italic;

  font-size: 20px;
  line-height: 20px;
`;

const BookshelfBookDetails = ({ title, author, location, dimensions }) => (
  <BookshelfBookDetailsContainer {...dimensions} location={location}>
    <BookshelfBookDetailsSide {...dimensions} title={title} side="front">
      <BookshelfBookDetailsTitle>{title}</BookshelfBookDetailsTitle>
      <BookshelfBookDetailsAuthor>{author}</BookshelfBookDetailsAuthor>
      <BookshelfBookDetailsBinding {...dimensions} side="front" />
    </BookshelfBookDetailsSide>
    <BookshelfBookDetailsSide {...dimensions} side="back">
      <BookshelfBookDetailsBinding {...dimensions} side="back" />
    </BookshelfBookDetailsSide>
    <BookshelfBookDetailsSide {...dimensions} side="pages" />
    <BookshelfBookDetailsSide {...dimensions} title={title} side="spine">
      {title}
    </BookshelfBookDetailsSide>
    <BookshelfBookDetailsSide {...dimensions} side="top" />
    <BookshelfBookDetailsSide {...dimensions} side="bottom" />
  </BookshelfBookDetailsContainer>
);

// we subtract 2 here for borders
const BookshelfBookContainer = styled.div.attrs((props) => ({
  style: {
    width: `${props.width - 2}px`,
    height: `${props.height - 2}px`,

    lineHeight: `${props.width - 2}px`,
    fontSize: `${calculateFontSize(
      props.height,
      props.width,
      props.title.length
    )}px`,
  },
}))`
  cursor: pointer;

  border: 1px solid red;

  color: white;
  writing-mode: vertical-lr;
  text-align: center;
  font-family: IBM3270;

  &:hover ${BookshelfBookDetailsContainer} {
    display: block;
  }
`;

export {
  BookshelfContainer,
  BookshelfShelfContainer,
  BookshelfBookDetails,
  BookshelfBookContainer,
};
