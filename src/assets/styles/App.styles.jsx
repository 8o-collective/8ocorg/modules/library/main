import styled from "styled-components";

const AppContainer = styled.div`
  background-color: black;
  margin: 0px;
  position: fixed;
  width: 100%;
  height: 100%;
  top: 0px;
  left: 0px;
  overflow: scroll;
`;

export { AppContainer };
