title: Space Horses 11
author: Snow
tags: 
  - srs!!!
  - main-character-sighs
  - marketable-franchise

---

Nobody questions a single bad film. Sure, there are questions to ask, but to many, they simply aren't worth asking. Things like, "Where did they get the money to film this?" Simple little nagging thoughts like "I wonder if this actor is funding a cocaine habit." But those questions, those small and unimportant questions, evaporate in bewilderment when a sequel is released. Imagine the shock of the general public, then, when cult classic B-movie _Space Horses_ was picked up for a sequel.

The public, for what it's worth, was relatively forgiving. Perhaps they chuckled at the bullheadedness of the director, or the now-conspicuous cocaine habit of the main character's actor. Similar questions were passed around on the internet, this time with a little more gravity. People weren't asking "why" rhetorically anymore. Confused niche boards developed, ironic enjoyment became a national pastime. The curiosity of _Space Horses 2_ was featured on talk shows. Clips were played on _Good Morning America_.

Then _Space Horses 3_ came out, and the internet imploded. How did they make another one so quickly? Is the main actor visibly on cocaine during this scene? Is it getting better, or is that Stockholm Syndrome setting in?

But the film critics slowly, ever so slowly came to a consensus. Ahhh. 'Twas a three part critique of the modern format of film. It was an ironic work of genius. Little details in the other two surfaced, too nuanced to be spotted on a first viewing. The director was lauded -- their name was L.R. Cruxi. Surely a pseudonym for an artist of his or her caliber. They couldn't be found anywhere. Similarly, the actors were nowhere to be found online. Some speculated that every actor was simply Leonardo DiCaprio in progressively more extensive makeup. _Space Horses 3_ changed the landscape of film forever as a medium for art, a bold project that took years to complete, and it completed with a grand flourish.

So imagine the art world's surprise, the pure shock and breathlessness, when _Space Horses 4_ came out.

Critics faltered in their adulations. _Space Horses 4_ was more of the same. Trite, contrived. If it truly was an ironic masterpiece, then... surely, surely it was. There was simply a deeper meaning that needed to be found. Dig deeper in the script. Count numbers found in the background. Look for themes. Anything. The critics couldn't be wrong. _Nobody would make a Space Horses 4, particularly one that was_ this _bad, without a message to deliver._

And then, critics still reeling from the whiplash of _Space Horses 4_, came _Space Horses 5 and 6: double Trouble [sic]_ [sic].

Ahhhhhh. It was artistic sadism. It truly was a comment on the art industry, a continuation of the genius portrayed in _Space Horses 3_. Who the fuck would release a movie called _Space Horses 5 and 6: double Trouble [sic]_ [sic] without positively oozing irony? The director was praised once more. A star was added to the Hollywood Walk of Fame. Finally, the public had their answer.

Until _Space Horses 7_.

Then _Space Horses 8_.

Shortly thereafter came _Space Horses 9_.

Most recently, there was _Space Horses 10_.

This is a story I have meticulously cobbled together through nearly a decade of notes and some very strange coincidences. Allow me to say this: _Space Horses_ is not merely a series of bad films. It's a series of the _worst_ films, and I've devoted a decade to understanding the franchise and unearthing the motivations of its creator and actors.

What I found was stranger than _Space Horses_ could ever hope to be.

Soon, _Space Horses 11_ will release, and the world waits with bated breath. Allow me to spoil it for you: don't bate your breath. _Space Horses 11_ is no different from the others. At least, on the surface.

I have no doubts that you will read this and consider it fiction. That's okay with me. There's plenty of speculation on the internet about the _Space Horses_ franchise. It's the butt of jokes, subject of lofty analysis. Nothing can be said about the films that hasn't already been said before.

Except this:

_Space Horses_ was a cosmic mistake in the universe's endeavor to remove a virus from itself.

There's some road in some state somewhere. It isn't particularly interesting, except that it's used as a conduit for an eldritch un-being to communicate with a dead god. I won't describe it. Just imagine a boring road. You're probably close enough. It stretched from east to west.

Tumbling down the road at a modest pace came an SUV, headed west. It was blue, or another color similar to blue; like green, or red. It had passengers and a driver. Or, at least, a driver. The status of any passengers was questionable. The only important thing about the car was the license plate.

It was a vanity plate issued by the state of Florida. It said:

HEYBUD

Another vehicle, on the opposite side of the road headed east, tumbled back. It too had a vanity plate.

WUSSUP

It took a moment for the East to respond.

HAVEYO

Another car quickly followed.

UHEARD

ABOUTS

PACEHO

RSES11

The West responded with an emphatic
 OYABBY

This conversation continued like this for a couple hours. I'll spare you the format and just put it in quotes as though they were speaking normally to each other.

"I can't believe Cruxi is making another goddamn Space Horses," said West.

"Frankly, I'm impressed that Cruxi managed as many Space Horses as he already has without getting assassinated by some vengeful human," mused East. East used a ferrari for the last one, indicating a sigh.

"We can't rely on the little ones to do our work for us," stated West, sadly (here signified by a conclusion via an Impala). "That's why we're here in the first place."

"I'm worried we'll have to manifest again. I hate manifesting. Existing feels so weird."

"We gotta, though. How about coffee? You remember coffee, right?"

"Coffee wasn't so bad. Better than most Pragma. Alright, fine."

And so a car bearing the license plate HTFINE rolled into the parking lot of the cafe at which I worked as a barista.

Allow me to describe myself for the benefit of your imagination.

Picture your average high school jock. The sort who's very into football, and absolutely bullies freshmen. That's the one. Now, imagine this jock in his youth. He didn't yet have muscle, but his love for sport still persisted. He played a lot of soccer. In fact, in middle school, he was on the soccer team. He had a lot of friends on that team.

Now, picture that jock's mother. She was nearly as excited about soccer as he was. Especially because she not-so-secretly enjoyed lording her son's achievements over the heads of the other mothers in her book club. As an act of graciousness, she often gives rides to and from soccer meets.

After a particularly exciting day of children kicking a ball back and forth across a rectangle of grass, the jock's mother visits a grocery store. She intends to purchase sport beverages for the group she now refers to as The Nice Boys From The Soccer Team. In fact, she has a coupon for sport beverages handy. Unfortunately, they expired merely a day before.

I'm the one who's getting berated by the manager for offering "an unfriendly customer experience".

Now, roughly with an equivalent expression on my face, I stared blankly at the front of the cafe. It was somewhat in the middle of precisely nowhere, the outskirts of a small town I can't even remember the name of anymore.

I don't remember much from that life anyways, besides my decidedly unhealthy obsession with the Space Horses series.

The cafe was a fusion between a haunted house and a feudal serf's living hut. The floor was so incredibly dusty that the pattern that once existed there has faded into a fuzzy grey carpet. There was a desire path worn into the coating of dust that went directly from the door to the counter, and a slightly less worn path looping around the counter.

Several pieces of "art" hung from the walls in an attempt to create a cozy atmosphere. Bare fluorescent lights beat harshly onto the room from above like an unfeeling god. I stood behind the counter, reciting lines of dialogue from _Space Horses 4_ in my head.

The bell rang, signifying a visitor. I instinctively began to produce a cup of coffee, mind still mostly preoccupied by Space Horses. If I'd been paying even slightly more attention, I would have noticed a couple details about this visitor.

Firstly, their quantity was unclear. It was as though they couldn't decide whether to be one or two people. _Alright, so if Xeevius really was played by Leonardo DiCaprio, did they use a voice changer, or just dub over with a different voice actor...?_

Secondly, the face(s) of the visitor(s) w(asn't)/(eren't) very good at maintaining a human expression. It cycled rapidly between various dog, cat, and other assorted mammalian expressions before finally finding the human expression of "oh no I've just received news that my estranged grandfather has perished in a tragic fire and I never got to tell him that I didn't really love him all that much". _Was Xeevius knowing about the Space-Doping without Klurkox telling him about it a plot hole, or did Klurkox's actor just forget his line during their scene together?_

Thirdly, the visitor(s) opened their coffee order by reading my mind in an attempt at small talk. "Actually, fun fact, Klurkox was originally named Clorox Bleach, but the actor for Xeevius had a really specific speech impediment."

I nodded sagely. That sounded about right.

"Two dollars fifteen cents," I replied, handing the customer a cup of coffee. Wait... Customer? Customers...?

My brain started working. My eyes started seeing. Space Horses, for the first time that day, was abolished from my mind. Oh.

What I saw before me was not recorded in my memory. I think this was an attempt by my brain to shield my future self. I only remember screaming.

Oddly enough, I was still offering the thing(s) in front of me the cup of coffee. I think that somewhere within my adrenaline-addled brain, I believed that if the thing(s) took the coffee, they would go away. It worked for most customers.

"Oh, oops," said the thing(s) with a dusty wheeze. The surface of its skin burbled and gristled, and finally bulged (or a similar verb to "bulged", but somehow even more turgidly awful). I also do not remember what it/they shapeshifted into, but I do remember my screams getting louder.
 "Ahhh. Uhhh, hmm." It/they attempted another form. This was suitable for my brain. My screaming eased away. Across from the counter were standing two people: a woman in a suit and a man wearing khakis and flipflops. The woman looked middle-aged, edging over fifty or so, and she was thin with hawkish features. Her dull red hair was pulled behind her shoulders in a low pony tail. The man was younger, probably around mid-thirties, and he looked like your uncle. No, sorry, not even _like_ your uncle. It was an exact match. This man was walking around as a clone of your uncle. I'm very sorry to inform you, but your uncle's visage was stolen by a concept named East.

"Twooo coffee," said East. The noise of his words were emitted not from his mouth, but from a point in space uncomfortably close to my ear.

Judge me how you may, but the processing abilities of my brain short-circuited around this point. I think that my mind's best way of dealing with this situation was to presume it was a dream, and so it engaged the dream-logic paradigm. I think I'm still suffering from that engagement as I write this -- it's been several years and still my mind is convinced I am existent in a semi-dream state.

My own crises aside, I stared at the pair, then glacially turned back to the coffee machines. I'd need to brew another cup.

"Pardon me, sir, but you _were_ just thinking about Space Horses, weren't you?" asked West, before I knew that her name was West. Her voice was slightly raspy. It had an edge of command behind it, a certain severity of weight.

East looked at her meaningfully. Hopefully meaningfully, I mean. I don't actually know. But "East looked at her" simply doesn't have the same ring to it.

East then glanced around the cafe, attempting to take it in.

"This isn't a cafe," he muttered. I nodded politely at West.

"Yes, I was indeed thinking about Space Horses. Do you think it's really Leonardo DiCaprio under all that makeup or--"

"No, hold on, you were thinking about Space Horses _seriously_," interrupted West.

I pondered this accusation for a second. "I suppose I was. It's my favorite series of films." Space bulged in the cafe slightly, or another verb for "bulged" that had some existential weight behind it.

West and East shared another hopefully meaningful look.

"I'm West, and this is East," provided West, pointing at East. "You do enjoy Space Horses _ironically,_ correct?"

This gave me pause. I thought about my several viewings of the series. I asked my friends to watch with me but they kept talking over the important plot. I remember the indignance in response to their impudence. How could anyone dislike Space Horses?
 "No, entirely unironically," I replied. Space ripped open at their bulges, or another noun for "bulges" with a slightly less disturbing connotation, and coffee spilled from the gaps in the universe, and borne on that tidal wave of coffee was a newborn infant rapidly progressing through the stages of life until it was an adult. East watched it with some concern, but West looked angry.

"Oh, goddamn," she said. The freshly minted coffee adult spewed clothing onto itself from its mouth. It was clown garb. Oversized shoes. Red nose. The whole getup.
 "AT LAST," screeched the coffee clown. The rips in reality sealed themselves, and I suddenly realized that I never worked in a cafe in the first place. I worked at an electronics store. Why was I in a cafe?

"Hello, Cruxi," sighed East.

"Oh my word, if it isn't half of the cardinals. Here to see me, hmm? Look at that. Look at you. Behold. You lost the bet." I turned back towards the coffee machines, beginning to prepare yet another cup of coffee. This whole "popping into existence" business was horribly inconvenient. I'd prefer if they'd enter in batches, so that the first cup of coffee wouldn't be cold by the time the next one crab-walked into our universe from the void.

I offered the newcomer a cup of coffee. He took it gratefully, shaking coffee beans from his sleeve, and chugged it.

Now, this is when my brain started to realize something was truly wrong about this situation. In dreams, when there is an inconsistency great enough, it can shock you into lucidity. Later, when I talked to East about this, he told me that the Concepts called this lucidity-inducing inconsistency a Pragma. The Pragma in this situation was the manner in which the clown imbibed his coffee.

Who... who would chug coffee?
 I slowly sank to my knees. Nobody chugged coffee. This was no dream. Why would you chug coffee, and why would my brain invent that? The Pragma was set, and it rotted my brain, taking seed. I curled into something approaching a ball behind the counter. The coffee was really hot, too. Man just chugged it. The goddamn clown chugged a cup of scalding coffee, and he didn't even flinch.

Cruxi cackled. "You said... you said _not one person_, or I was free. And this fine gentleman-- say, what's your name son?" He squatted over me. I was struggling to continue existing as I realized that the events of the past few minutes had indeed actually happened. I choked out a wheeze, and Cruxi seemed to contemplate the noise for a second.

"What is that, German?"

West turned abruptly towards the door. "Alright, East. We've seen enough. Let's go. I'll wake the other cardinals. I think it's high time we burn this world to ash before Cruxi does whatever Cruxi does."

Cruxi frowned. "Hey, come on guys. Maybe I've changed." He slowly, ever so slowly raised a hand to his nose, and petrified, the concepts watched. Cruxi stared at them with a puppy-dog face, eyes moist, lips pouting, and honked his nose.

There was a sound something like a cash register eating a cat, and the universe flipped over. The cafe eroded into dust, the dust particles rotated, and an image slowly subsumed it and became reality. It was a room.

The room had white metal walls and a white metal ceiling. The floor was uncomfortable, probably due to being white metal, but at this point I was fairly numb to the experience of physical discomfort. There was one of those spaceship-looking doors with no knob on the end of the square room.

West sighed.

"Oh, good. Look. Cruxi put us in another dimension." She stared at me annoyedly. "I hope you're proud of yourself, kid."

East shrugged. "It's not his fault, West. Maybe he was born with garbage taste in films. Perhaps he's stupid. Any number of factors. We don't know that he _maliciously_ enjoyed Space Horses."

My brain fired a weak connection, and it was enough for a question to phrase itself and tumble off my tongue. "Hold on, you said his name was Cruxi? Was that... the writer and director of _the Space Horses chronicles?_"

West's eyebrows dipped in a feat of anger-induced acrobatics. "Yes. Yes, you idiot, that was the creator of Space Horses. And you just loosed him upon the world."

I mused over her words for a second. "I kinda thought that the creator of Space Horses was just a normal human."

"But.. why?" interjected East. "Why would you think that? How could you think that at all?"

"Well, what? Beethoven was a normal guy, wasn't he?"

East and West shared a third look that I'll describe as meaningful.

"Are all humans as dumb as you?" asked West.

"No -- probably about half of them are, though," I replied indignantly. How dare they insult my average intellect? It was good enough to get me by.

West checked the door.

"It's no use," she said. "The doors are stupidly designed. We'll never figure out how to use them." I looked at the door. It was plastered with a series of infographic signs, a tale of how to use the door in hieroglyph. It was almost impressive how disturbingly little was conveyed in such a supposedly intuitive format. It was as though the complete instructions for the door were run through Google Translate into a language that didn't exist, and the resulting garble text was shoved into the Wingdings font.

I stood, shaky on my feet. The dream logic had kicked back in now that Cruxi was gone and I was no longer thinking about what sort of person chugged coffee. I had my answer: the writer and director of Space Horses. My dream self knew that this made sense, and so my dream logic was once again internally consistent.

"Alright, well, let's find another way out," I offered. East and West looked at each other, and I was getting sick of it. "Could you two stop sharing glances that I presume are meaningful and just say whatever it is you're communicating via your eyes?

East sighed. "Alright. We aren't taking you with us." My eyebrows dipped down before my mind could completely process the sentence, as though my body were confused before I was.

"What do you mean? Wh-- this whole thing is your fault!"
 West snapped. "What do you mean our fault? You're the one who unironically enjoyed Space Horses. That was the one thing keeping Cruxi tethered to the void. That was the only thing that kept our world safe." Before I had a chance to respond to this outrageous series of accusations, I was interrupted by the pair of concepts very loudly thinking. East was the first to go -- he popped out of existence, leaving behind a manhole spinning in a wide slow circle like a massive coin, and a series of small metal disks that I recognized as some form of southeast Asian currency.

"Wait, come on--"

West followed shortly thereafter, leaving in her place a handful of dried corn kernels, a couple of pigeon feathers, and a dead rat. I regarded the piles with somewhat undeserved animosity. I was stuck in a too-chrome room with no way out save for a door that could only be operated by a successful IKEA mechanic.

I scooped up the currency and put it in my front apron pocket. The apron was vaguely green. It was sued into being less green after a lawsuit from a corporation that I would be sued for naming here. We didn't get new aprons, they merely paled under the political pressure of their own accord. Stains covered the apron, the apron covered a long-sleeved white shirt, the shirt covered me. This is around the point when I remembered I never worked at a cafe.

The small town in which I lived once had two cafes. Then it had one. Allow me to paint a picture for you -- one of them was a small-town cafe that catered to the specific tastes of the townspeople. It had been there for decades, and despite being mildly ramshackle, the people loved it as they would love a small and vulnerable kitten.

The other was from [REDACTED] corp.

Given a choice to either rally behind their small town's small cafe and stick it to the man or fall to corporate propaganda, my town gladly chose the latter.

This little bit of logic and realization leaked into my understanding, piercing my dream logic and drenching it in a soft and delightful coffee scent. It wasn't so drastic a Pragma as the now faded memory of a chugged hot coffee, but it was plenty to elicit enough lucidity for me to attempt to find a way out of the situation.

I observed the room, hoping there was something that I missed on my cursory analysis, like a helpful set of levers and pulleys or something. Alas, there was not. I was left with no choice but to approach the door.

Doors typically, in my limited experience, did not have personalities. This one certainly did. As I walked toward it, I could feel... _some_ sort of emotionemitting from it like some sort of essential oils scent diffuser. As I approached it, it opened -- slightly. A crack was formed in the center of the door, not even leaving enough room to peek through.

Before I could ponder what to do with this crack, the door answered for me.

"coooOOOOOIIINNNNN", the door agonizingly wheezed.

"...Pardon?" I asked the door, incredulously.

"Ahhhh... Ah waahhh... Ah want, I want, want... ccoooooIIiiiiInnnn," firmly concluded the door in a strange, slightly southern-accented falsetto. I thought it over for a second.

"Will you open if I give you a coin?"

The door excitedly opened and closed, producing a horrendous and entirely novel clack-grating noise. "cOiN, CoinNnn," it whined. "Coin coin coin coincoincoin," it proceeded. I nodded once, slowly. Hmm. Yes. This was happening, wasn't it?

I pulled a bit of the non-specific southeast Asian currency from my apron pocket. I'm not quite sure how the door sensed it, seeing as how it had no apparent cameras or eyes or any sensory organs at all, and so I was forced to assume that whatever angry god breathed life into this door had bestowed it with some kind of sense of smell. Regardless of _how_ specifically the door sensed my fistful of coins, it began somewhat violently clacking and slamming, calling for the coins like a baby bird clamoring for food from its mother.

I carefully inserted the coin, attempting to keep my fingers safe from the existential threat of pinching. The door moaned quietly and nearly sensually, then slid open.

That's when I met Chokey.

The first thing I noticed about Chokey was his business card, firmly planted in my face. The second thing I noticed was his breath. It was so thoroughly drenched with the scent of whiskey that it managed to skip the stage of intoxication and immediately give me a hangover.

Against my will, my eyes read the business card. It was the vague off-white of old drywall, and it emitted a mildew scent (I only learned this later, once my brain stopped processing the aroma of whiskey). The font was something analogous to Comic Sans, and the haphazard placement and sizing of the letters offered the distinct impression of a ransom letter.

_LURKSLOCK "CATCH ME IN THE MORGUE DOING COCAINE AT 9AM" CHOKEHENRY_

_CEO OF SMILEY CO., TROUBLEMAKER, PLOT DEVICE_

_IF I HAD FRIENDS THEY'D CALL ME CHOKEY_

_I AM LEGALLY MANDATED BY THE PFALANDIAN COUNCIL TO TELL YOU THAT I AM NOT A HERO'S JOURNEY MENTOR_

He withdrew the business card before I had a chance to read the paragraph of fine print at the bottom, and I was graced by my first look at Lurkslock Chokehenry's face. His left lower jaw drooped nearly down to his collarbone, male pattern baldness gleefully spat on his head, his eyes bulged and darted around in perpetual distraction, and his clothes were best described as "Victorian tramp-punk". He had a tattered greatcoat on that was striped black and white, as though it were cobbled from not-so-delicately sewn prison clothing. Upon closer observation (a difficult feat), a tiny top hat rested somehow on his head, the mechanism by which it remained affixed to his skull unknown. His pants were so patched that not a scrap of the original clothing remained, and his shoes were both left shoes -- but I later learned that this was because he indeed had two left feet.

I will not be writing about this particular epiphany. It's better that you don't know.

Chokey in motion was approximately as disturbing as a still image. His limbs moved together in unique ways. I've often pondered exactly how few muscle groups he was capable of controlling, because when he walked his arms waved in undulating, rapid patterns, and when he spoke emphatically he needed to restrain himself from using hand gestures, lest his feet carry him away. It apparently took concerted effort for him to both breathe and blink, but his wild red eyes and irregularly heaving chest betrayed the fact that he often lost track of both. He was bent in the middle as though afflicted by a tragic folding-machine incident in his youth, and despite the obvious necessity of a cane, he didn't have one. Instead, he simply grabbed at whatever was closest to him and pulled himself forward as a method of locomotion, occasionally stumbling to his knees and crawling forward before stumbling somehow back onto his feet, apparently oblivious to his own body's betrayal.

Chokey smiled at me either amicably or deviously. In the moment, I didn't really recognize it as a smile at all. His teeth were somehow immaculate, a pristine and straight row of beaming white teeth.

"Visitors in me broom closet," he said, to nobody in particular. "Positively frabjous."

"Positively wha--?"

Chokey pushed past me and entered the room. "BROOMSES! Come, I beckon ye," he said, stumbling stiffly as he spoke, gesturing vaguely at the walls like a windmill of flesh. A broom fell from the ceiling. It was a victim of futuristic design. It was strangely curved and chrome, and the end of the broom was best described as "minimalist", which is to say that this broom was mostly just a futuristic stick. Chokey caught the broom which surprising agility, which required his leg to undertake a maneuver that I was surprised didn't break it. He speared the dead rat on the broom and flung it at the wall, where it vaporized into a fine mist before dissipating entirely. He then fell to his knees and collected the feathers and corn kernels, stuffing them into various pockets.

"Excuse me sir, where am--"

"Is. Proper grammar-n. Gotta is, not am. You's not amming when you askes where that is bathroom," he grumbled in such a serious tone that my brain briefly mistook it for a comprehensible statement.

"...Ah."

He hefted the manhole cover, in the same motion producing a bungee cord from another mysterious pocket, and lashed it to his back like he was a tortoise. For all the world, it fit there like a glove, only enhancing the look. He began his exit, then apparently noticed me again. His hand twitched towards his belt, then he remembered something. He stared intensely at my apron for a second, then wheezed. He bounced up slightly, then down, and completing this odd dance was a half turn followed by a double take.

Without looking at me, he spoke from the corner of his mouth as though someone were listening. "Well, come on then, we'll's get you's to an place other than what's now here then, eh?" He concluded with a nearly grandfatherly chuckle.

What choice did I have? I reluctantly started to follow him.

"You's smell like Concept," he slyly muttered. "I know smell Concepts scent. But you ain't's you's Concept, eh? My Pragma sniffs em like sniffs Sniffs sniff."

I desperately attempted to avoid processing his words. Fortunately, due to their absurdity, it wasn't that hard.

He led me through a series of doors far less difficult than the first. Most yielded immediately, some open with a gentle prodding from Chokey's foot. The hallways were squat and dingy, a low ceiling and a caged light offering the distinct impression of a prison hallway. We walked for a few minutes in a straight line, and my legs had gotten so used to walking straight that I was caught off-guard when Chokey abruptly turned and violently kicked a door.

The door shuddered open with a meek "whyyyy". Behind it was... well, technically a bathroom. But what had been done to this bathroom was adjacent to war crimes in terms of severity. Then, Chokey fell forwards, flat on his face, and remained motionless.

I looked around helplessly for anything or anyone that could be of use. I had a sneaking suspicion in my gut that my unwanted guide had just expired.

A few more moments passed, my mind still caught in the tumultuous current of strange events, when my body, looking for something to occupy itself with, entered the bathroom of its own accord. I stepped over what I hoped wasn't Chokey's corpse.

When I entered the bathroom, Chokey stirred and groaned, then gathered himself up haphazardly and stood, looming behind me in the way that only someone several inches shorter than you can loom. I wasn't sure whether to be relieved or not that he was alive.

This question was rapidly answered for me as his cold hands clasped around my wrist from behind like irons.

My struggle was futile. Chokey shoved me forward and my shoes held no traction on the unfortunately slick tile. I slid towards a stall, and I vaguely remember Chokey cackling from some far off space.

I had reconciled with my inevitable death early in my youth. I essentially asked it not to bother me, and in turn, I would not bother it. This arrangement worked well up until this point, where my death seemed a lot more inevitable and a lot more death-like up close. Chokey shoved me into a stall, annihilating the stall door into splinters, and my mortality was forced to reckon with the Methuselah, the Medusa, the Gorgon and Chimera, the Ancient and Terrible, the Gnarled Corpse of the Ages Lost, the Eldritch that greeted me from The Beyond. This was a public toilet that hadn't been cleaned for centuries, and had since grown wise enough and so sentient as to produce its own filth. And very suddenly, my inevitable death was quite far away.

"BACK TO HELL, DEMON," screeched Chokey. "YOU'S TELL THE CONCEPTS STOPPING WHAT'M HAVE THE WEST AND EAST CARDINALS ON MY SHIP," he continued, ever reasonable.

I don't know how my neurons possibly processed this under the kind of stress I was under, but possibly guided by some sort of guardian angel, I screamed the right protest.

"I'm not a Concept! I know where East and West are!"

Chokey paused, my face held inches from the stench of my reckoning. He eased his grip, but before I had time to be relieved, it tightened again. "How'msem you get out on shiphere here in room closet of broom'sm then?"

I sighed weakly. "I cannot understand a fucking word you're saying."

Chokey stared at the wall thoughtfully. "How..." he swallowed a trailing phoneme reluctantly, "did you get on my--... _the_ ship?" Every word was strained and almost guttural, as though normal speech pained him. Knowing Chokey, it probably did.

"I was working at a cafe that I didn't work at and these two people came in named East and West who were really weird and then a clown came in with a lot of coffee and chugged coffee and the universe flipped over and we were in that room and then East and West turned into assorted objects and _please could you pull my face away a little_--"

Chokey obliged and I was given enough room to breathe. He released my hands and pondered my words -- I think. He might have simply had a small heart attack and rested for a second while he waited for his heart to reboot.

"Sounds like you's had a run-in with'm Cruxi humself," he said quietly. "Interestin'."

"Listen, man, I just want to go home, okay?" I whimpered. Chokey stared at me curiously.

"Home's? You's don't gots a home's, kiddo. I smell Concept, and my Sniff don't lies."

"What do you mean?"

"I means, Cruxi cheated."

My head spun with the various fumes tumbling in my head, and my body was numb. My eyes felt heavy and sluggish in their sockets, and iron cracked my skull from within.

"You's says you've don't work at'm cafe, ye catch?" he said. "Where'd you work? What'd you do fer livin' and what?"

I turned away from him, the scent of whiskey drowning away everything in my head.

"I don't remember," I answered honestly.

"Kiddo, you's even know yours name?" Chokey asked, more quietly. This was a far more effective Pragma. All at once I awoke from my dream.

"Eric," I lied, hoping he wouldn't sense my lie somehow. Of course he did.
 "Mm-mm," he mumbled, shaking his head.
 "Daniel?" I courageously asked, already knowing I was wrong.

"You's a Concept. I don't knows why a Cruxi thought'm you up, but I do know if he's out he's cheated. Like he cheated in blackjack's. Bastard."

I felt anger bubble up in my chest.

"What, so I just exist to enjoy Space Horses?" The Pragma slipped away and was replaced with a dull rage. "That asshole poofed me into existence without my consent in order to get out of jail?"

Chokey nodded sagely. "Sounds'm likes a thing bastards do, ye catch? How's bout you'm I teaches Cruxi a lesson?"

I'd later come to regret saying yes.

The fact that "ship" was in fact short for "spaceship" barely registered in my mind. Chokey led me through the hallways, feeding me information that was probably false about her spaceworthiness and encounters with space krakens. Only when we got to the glass enclosed bridge did I understand, truly, that it was a spaceship. It was pointed out among the stars, and behind us was the shadow of a planet that eclipsed us from the sun. The bridge was vast and empty. Apparently the captain of the ship, who Chokey angrily refused to name (instead preferring "technically-captains"), parked the ship behind a planet for the populace to sleep.

It was a battle-cruiseship. A military vessel designed for pleasure! And also wrath. It was outfitted with ion cannons that, according to Chokey, could "turns several menfolk into not-menfolks in instants less than seconds'm count". He spoke about the ion cannons with a disturbing reverence that I decided not to question.

The bridge was massive. It took nearly ten minutes to walk the length of the whole thing, during which I barraged Chokey with questions and he gave me extremely dubious answers.

"So what _is_ Cruxi, really?"

"MmmmMMMMMMMM--" said Chokey, getting progressively louder into a hum-scream before abruptly cutting off after tripping over a bench. He stood and decided not to elaborate.

"Where are we going, anyways?" I ventured.
 "Spaces kitchen," he answered matter-of-factly.

"Kitchen?"

"_Spaces kitchen,_" he corrected. "Needs us'm space carrot."

For the next thirty seconds or so, I remained silent, attempting to formulate a question in a tactful way.

"Why do... we need a space carrot?"

He looked up exasperatedly, which also happened to activate his shoulders muscle group, forcing his twirling arms into a "V" behind him.

"We's gotta summon'm Cruxi Concept," he muttered.

"Cruxi is a Concept too?" I asked, expositorially.

"Everythings'm whatnow a concept, kiddo," he replied, voice raising in the middle of the statement and falling at the end, the opposite of the natural English tonal contour that should have followed that statement.

"And you want to summon him with a carrot?"

"You'm summon any concept with'm rights Pragma, kiddo. Carrots activate the Pragmas. Makes'm your eyesights better."

I attempted to fit that statement into reason, and accomplished a mild aneurysm.

"'Sides," he continued, "We's'm not just using what a carrot, we's'm use a cuppa coffee in concurrsence." He paused briefly, absorbed in thought. "Bastard what loves coffee."

We reached the end of the bridge, and were met with an inconspicuous looking door with some kind of keypad besides it.

"Hrrnngg, damns it," choked Chokey. "Staffs only."

This set _several_ alarms off in my head.

"What? I thought you worked for the ship! Aren't you staff?"
 "I's'm _janitor_. Pays attention, kiddo. I's clean up. Doesn't mean'm I's hired and what." He searched around the keypad for some other form of entry. Nothing was apparent. He rasped out a frustrated wheeze.

"They'm keep changin' up the keyses, too. I steals _one key_ and surddenly'm whole ship's got new locks..." he trailed off into grumbling.

"Splan B," he abruptly interjected. "Carrots Heist'm." He wheeled off in a separate direction, and I felt compelled to follow. He tumbled forward with set determination in his eyes.

The bridge was a lot longer than it was wide. It only took about thirty seconds to cross, at which point Chokey stopped and stared up, like a pointer dog.

"Boost'm," he said. I followed his gaze and found a vent at the end of it. He produced a screwdriver from a pocket and held it in a manner similar to how I imagine one properly holds a sacrificial dagger.

What choice did I have? I kneeled on the ground and cupped my hands.

Chokey carefully ignored my cupped hands and clambered atop my back instead. His jerky movements and pokey bones gave the illusion of an immense cockroach skittering on top of me, and I fought back the impulse to heave him off.

I heard a series of grunts and bashing noises from above me, and I looked up to be greeted by the sight of Chokey eviscerating a vent with a screwdriver instead of using it to actually unscrew the grate. He worked quickly, snapping metal and pulling viciously at it, until finally the vent was flayed enough for him to crawl inside. After a couple of bangs and grunts from inside the vent, and a couple of kicks at the grate that I now felt quite sorry for, the corpse of the metal cover flew off the vent's entrance and skidded to a stop on the smooth floor of the bridge. Chokey's face appeared from the vent, beaming with his weirdly perfect smile, and he offered me his hand.

I grabbed it and he pulled me in, heaving my body into the vent as though I were weightless to him. He turned around inside the vent (a feat that took no small amount of time to accomplish) and began crawling, his odd clothing clanking against the sheet metal, and his shambling gait tapped out an irregular time signature. I mused whether any listeners below us would call an exterminator or an exorcist.

The maze of vents in the ship spiralled into an apparent infinity of odd-angled branches and pitch darkness. I realized at this point that I had no real concept for the size of the ship -- for all I knew, it really did stretch into an eternity of tangled ducts and passages. After the light cast into the vent from the bridge had faded, and a couple of minutes stumbling forward in the dark, Chokey paused for a second, and some illumination came from ahead from a source I couldn't discern, preceded by a noise that tipped the scales in favor of an exorcist.

It was a grating screech like a fork ran gleefully across a washboard, followed by an increasing whine like the filling capacitors of a camera flash, and the symphony concluded with a loud _fwoosh_, the _fwoosh_ of ignited gas fumes. The light ahead was a steady, unwavering white, almost clinical. A couple seconds after the light came into being, something banged on the vent form below and my heart stopped briefly (well, longer than usual -- the average person's heart stops at least once per second, usually more). Following the noise, an angry male voice shouted from below, muffled by the sheet metal.

"Lurkslock, I know you're up there, you goddamn bastard. Where's my money?"

Chokey froze, then continued scuttling -- much faster. It was actually difficult to keep up with him. Behind us, the banging and shouting faded, and Chokey muttered what I think was a long-winded string of profanities.

When the shouting finally faded into the background, all we were left with was the close, claustrophobic sound of our own movement, and the far, cold hum of the ship. The thrumming noise of the ship's engines was ever-present, but only in the silence could it really be heard. It was like the heartbeat of some ancient organism, and we were here laying our ears against its chest. It was almost comforting, and I found my breath slowing the match the incessant rhythm of the almost tidal hum.

But soon, another noise joined the pair, one neither far nor near. A syncopated tapping to our own shuffling rhythm. It grew louder, and closer. Until finally, Chokey suddenly stopped and grunted, and I almost rammed into him. I tried to peek past him and further into the vent, but he occupied too much space.

"...Hel?" confusedly half-whispered Chokey. A timid woman's voice responded.

"Oh. Uh, hey Chokey." It was mild and sweet -- I'm fairly certain this was true even outside the context of Chokey's horrifically grating gruntspeech.

"What's'm you doin' up in here this vent'm night time?" asked Chokey. He paused for a second, then added, suspiciously, "...right'm 'bove the kitchen s'well?"

"Uh, well, I could ask you the same," responded our fellow vent inhabitant. I tried to picture the speaker's face in my head. I won't mislead your expectations by describing them here; I was horrifically wrong.

"How's'm 'bout neithers of us's asks'm other questions we can'ts answer?"

After a moment of tension came the reply. "That's probably for the best. Can you help me get this grate open?"

One grate murder victim later, Chokey, myself, and our new friend dropped into a room. Some amount of stumbling and darkness-induced collision later, the lights came on, and I was given a chance to observe our environment.

It was a kitchen, a typical industrial kitchen with clay tile floors and stainless steel counters. It was clean and empty. Several large appliances lined the walls and WHAT THE FUCK IS THAT?!

There was an emaciated woman wearing chef's clothing -- she was skin and bones, her face pulled taut over her skull, lips stretched over yellowing teeth, her thin hair missing in clumps. Her stomach bulged outwards, and something within her skin writhed like living spaghetti, tubes travelling up and down her arms through her veins, some thicker around the middle than her actual arms were.

I lost my breath, and if I had anything in my stomach, it would have immediately been expulsed. I doubled over in horror.

"Thisums Chef Hel," knowingly stated Chokey. I wheezed. Chef Hel smiled politely at me.

"I'm just down here to try out a new recipe," she replied cordially, somewhat embarrassed. "Would you like to try it?"

Chokey's eyes lit up. "Ohoho, friend, you's gotta eat'm recipes at least once befer ye kick it," he said, scrambling to say it so quickly that the words came out even more jumbled than usual. "Hel's an _artisy-ist_."

I stared incredulously at Chokey, attempting to confirm that we were indeed seeing the same person. Meanwhile, Hel had already started cooking. I couldn't really think of anything to do other than stare and watch her work.

She rolled out some dough expertly, adding flour to increase the cohesiveness of the material, and pulled a series of strange looking fruits from a refrigerator. She tossed them into a blender and added sugar and water and various other substances. She worked like a whirlwind -- extraordinarily talented. That's when I realized that every movement she made was preceded by a tube gristling under her skin and guiding her motions.

She started heating the oven, then took the flattened dough and made small pockets, into which she poured the fruit filling. Despite myself, I was mildly appetized. She closed off the dough pockets, and repeated the process several times, before finally she was done and she pushed them into the oven.

Chokey turned to me, eyes glinting. "True'm master ats work, ye catch?" he bragged.

"So what are you here for?" asked Hel curiously, leaning on the counter casually across from us, to the furious protest of whatever worms were living inside her body.

"Space'm carrots," conspiratorially whispered Chokey. Hel looked somewhat taken aback.

"You know the captain doesn't take any shipments of space carrots anymore, right? Our stock is..." she paused, almost saying something else, "is gone."

Chokey's expression soured. I could hear the profanity without him giving voice to it.

"...what's a guy like you looking for space carrots for, anyways?" she asked suspiciously.

Chokey eyed her with equal suspicion. Both stared at each other like a duel from a spaghetti western directed by Cronenberg. The tension settled on the room like a viscous liquid, oozing from the corners of the room itself.

"Concept," yielded Chokey. "We're puttin's a Concepts on the ship, and usin' the space'm carrot for its Pragma."

Hel raised an eyebrow. At least, she attempted to, but there wasn't much skin left for that, so instead she just opened one eyelid slightly more. "What concept?"

Before Chokey could answer, a timer started beeping out the Starspangled Banner with grating sine waves. It, like so many before it, faltered on "the rocket's red glare", and trailed away into an embarrassed silence. Folks, check your vocal range before you sing a song like that.

Hel opened the oven and pulled the baked goods from within. The aroma was heavenly. She sprinkled powdered sugar onto them, and they looked like the pastry equivalent of supermodels.

Then, because there is no God seated above us (or if there is, it is a cruel and spiteful monster of wrath), she turned and coughed into her hand, and then attempted to sneakily sprinkle something from her hand onto the pastries, before gleefully offering them to us. They had pulsing, _writhing_ little translucent beads on top.

I felt my soul die in that moment, and it was resurrected briefly only to be tortured and perish once more when I saw Chokey rabidly swoop down upon his pastry and consume it without hesitation.

"Chokey, NO!" I yelled. But it was too late. I watched with morbid fascination as he gulped down what I presumed were parasitic worm eggs.

However, what I hadn't thought of in that moment was the efficacy of alcohol as a disinfectant. Chokey's blood alcohol percentage was higher than Jesus's -- where the latter's vinous ichor boasted at best 40 proof, Chokey's heart-liquid edged out moonshine in a competition for alcohol content.

The air surrounding Chokey's body was hot and heavy with alcohol. It thrummed with electricity, and the vapors became more intense, coalescing in the air until the clinical stench was overwhelming. A dark liquid formed in a shell around his body, and with an uncomfortable grunt, it popped -- liquid sprayed in every direction in a blast radius around Chokey, coating everything nearby in a fine mist of alcohol and something red.

It was incredible. Chokey's body had become so dependent on alcohol that it had developed some new form of immune system that had dissolved the eggs and expulsed them. Of course, I didn't realize this at the time; I was too busy screaming.

Chokey tipped his tiny top hat at Hel (thereby ruling out glue or nails as a method of affixation, leaving me with either magnetism or static electricity) and muttered "Thank'm fors the meal, ma'am." Hel smiled pleasantly, and all at once a realization rushed into my horror-wracked mind: this was a common occurrence between the pair.

Chokey's smile faded and he attempted a more business-like expression. He shook his head and straightened his greatcoat, an action that, when combined with the striking black and white pattern of the fabric, looked like an attempted hypnosis.

"Cruxi," he said, in a low tone. "To's answer'm question froms before meal." At first, in reaction, Hel displayed an expression that I think was shock, but this shock yielded to thoughtfulness, and finally she shook off the thoughtfulness and let disapproval settle into place.

"That's... a bad idea, Lurkslock," she somewhat urgently whispered. "You and I both know that Cruxi isn't one to be trifled with." She paused for a second. "Is this about the blackjack game when--"

"M'f course it's umbout the damned blackjack! Bastard's cheated me!" he cried indignantly. "Paid off'm Technically-Captains 'n gotta hell outta dodge! Now's you's tell me'm Technically-Captains'm ban space carrots? Withs no penultimate motors?"

"Ulterior motive," patiently corrected Hel. "And I'll admit, it is fishy. Not to mention counterproductive. Do you have any idea how hard it is to make space carrot cake without space carrots? I've been reduced to using space parsnips, Lurkslock. Space parsnips!" She defeatedly dropped her head and sighed.

"It's no use, anyways. I bet that Powell--"

She was interrupted by a low growl from Chokey.

"--sorry, _Technically-Captain_ just tossed all the carrots out the airlock and called it a day. Some lucky pirates have probably picked them up by now," she concluded absently, staring off into the wall behind us. My head spun. Pirates? Okay. Sure. I could feel a migraine creeping up from the back of my brain, leaking into my temples.

"Mmmm, wards off'm space scurvy," he muttered.

"Why is everything... space-something? Is space scurvy different from normal scurvy?" I interjected.

"It's in space," pointed out Hel, as though it were obvious.

"So are we, though. Are we space people?"

"Yes, of course," she replied. Well, that line of questioning went nowhere. I retreated back into silence.

"Well, short of flagging down a pirate vessel and asking them for our space carrots back, there's nothing we can do," sighed Hel. Chokey contemplated this option.

"Or, we's could offer'm somethin in 'change for'm space carrots," he said, carefully picking his way through each word, each syllable of his butchered pronunciation. "Quacks told me'm ships gots new passenger by'm name of Corkfoot. Soumds awful likes a pirate name, if'm asks me."

"_Chokey_," she chided softly, "you aren't seriously considering breaking into the brig, holding a prisoner hostage -- based on information from _Quacks_, no less -- in the hopes that based on their name they are a pirate, then hailing a pirate ship, hoping that whoever's closest is either the crew or an ally of the crew that the supposed pirate hostage belongs to, and then using the prisoner as leverage to trade for space carrots, all in hopes of getting revenge on a clown who cheated you in blackjack, are you?"

And so Hel, Chokey, and I squirmed through a cramped vent, following the light provided by whatever demonic flashlight Chokey possessed. The vents twisted and turned in awkward direction, veering off-course into the bowels of the ship. I was somehow unsurprised that this was the pair's primary form of locomotion. I imagined Chokey travelling through the ship at daytime hours, kicking people and their children and shoving folks into toilets. I imagined Chef Hel, spraying a spittle of worm eggs in every direction as she talked.

It was for the best that they stuck to the vents.

Myself, on the other hand... I wondered how I got to be convinced into this mess. This endless, winding corridor, only filled with the sounds of trudging through vents ever-so-slowly, was a perfect place to be lost in a reverie. I took inventory of my advantages and leverage in my relationship with Chokey. There were none. What did I have to gain by doing what the madman said? And what could I possibly gain from sharing a vent with a worm plague?

This was a Pragma that set the gears in my head turning. Life on a military cruise ship couldn't be that bad. Maybe I'd follow Chokey and Hel as long as it took them to doze off or something, then slip away into the ship and hang around as a stowaway until... something. After all, the pair had to sleep eventually, didn't they?

The squeaks and thunks of the vents faded into a monotonous backing track, and I was thankful for the lack of speech as we made our way through what felt like miles of vent systems. I had no idea how big the military cruise ship was, but it was no dinky little yacht, that was certain.

We wormed through the ventilation system for a longer time than my mind bothered to keep track of, until finally Chokey halted. I tried to peer ahead. Golden light streamed in from a grate.

"I's'm, hrrmmm, ahhhhhhrrrghh hmmm," muttered Chokey, apparently having a heart attack. "I'm thunk we's'm mights be lorst," he shamefully continued. Hel and I both paused for a second before unleashing a barrage of complaints.

"It's'm been times since I've been'm the brig!" he protested weakly. "Now's as goods times as'm any 'a checks'm wheres we are." He pulled the grate off and dropped into the gold light. Hel shrugged and followed. I contemplated going backwards, back the way I came, then realized that there wasn't enough room in the vent to turn around in, and I didn't exactly fancy the idea of crawling backwards through miles of ventilation. So, I joined them in the light below.

The light was provided by a series of candles placed around the perimeter of a small, closed off room with a series of racks with electronic equipment on them, with wires organized roughly like spaghetti falling out of them like playdough from one of those extruder things. You know the ones. The light on the ceiling was broken, and had a cozy little family of spiders nestled in the tube.

"Hrmm," said Chokey. "We's'm a server room. I's don't know which. Does you, Hel?" He turned to Hel. She was white as a sheet, staring at the wall. Along with her already emaciated appearance, this offered her the distinct impression of being a corpse.
 "I know this room," she whispered softly. "This is server room 1B. The haunted spaghetti room." She grabbed Chokey and shook him hysterically. "We need to get out of here _right now_."

The servers whirred and came to life. It was too late. The walls vibrated, and the tangle of wires lashed over the door. The vibration of the walls grew louder, split in pitch, and eventually resolved into a voice.

"Who's there?" it asked.

"Uhhh," I offered, unhelpfully, because the other two weren't saying anything. "We're just passing through. Wrong room. Our bad."

"Oh," said the room, somewhat disappointedly. "Not here to, say, pay respects, or something?"

"Mm-mm," I answered in the negative. "Just looking for the brig, actually."

"The brig, you say?" The vibration peeled away from the sides of the walls and coalesced into the wires. The wires twisted and rose, snapping one by one from the servers, packing tightly into odd but recognizable shapes. A mound of wires piled up, until finally, a wire golem stood in front of us. "I died in the brig," it said.

"That's... unfortunate to hear. I'm sorry for your loss."

"Are you? Are you really? Do you know who I was before I died?" it asked.
 "Uh, no?" I said. "I'm pretty new here."

"I was Steve. The IT guy. The wi-fi went down in the brig and the prison guards asked me to fix it. One of them thought it'd be funny to push me into a cell. Then a timer went off on there phone, and they said something along the lines of 'hold on a minute, my hot pocket is done', and three days later I died of dehydration."

"Mmmhm," I said, eyeing the door.

"You aren't even listening!" protested Steve. "You're just like everyone else! You all act like I don't even exist! Who do you think fixes the wi-fi when it goes down, hmm? When you idiots use too many bandwidth for pictures of cats?!"

"Wait, you still fix the wi-fi? As a ghost? That's... kinda sad, my guy," I said.

"Well, who else is gonna fix it?" he mumbled. embarrassed.

"That shouldn't matter to you. You're a ghost now. These assholes killed you, didn't they? Just let the wi-fi go down."

He paused for a moment. "Alright, I still use it to look at cat pictures," he confessed. "Besides, I'm stuck here. Ghost rules, or something. Unfinished business. Et cetera."

"What do you still need to do?"

"I dunno, haunt the guard, I guess. I've just been hanging out in the server room so that I can fix it whenever it goes down."

"Do you think he still works in the brig?"

"Probably. This was only a few weeks ago."

I shrugged. "Come with us, then. We can help you out, if you show us to the brig." Chokey and Hel were both furiously making hand motions at me. I ignored them to the best of my ability.

"Hmm. And if the wi-fi goes down in the meantime?"

"You can-- ugh, you can just fix it when you get back, dude," I said. The wires spilled onto the ground, and my heart felt invaded. A cold sensation spilled through my body.

"Alright, let's go then. Brig's just down the hall," I said, without asking my lips to move.

"Hey," I replied. "That's not cool, Steve. You should ask before you possess someone like that."

"Sorry," I mumbled. "I'm just not used to being around people."

"I thought you said you only died a few weeks ago," I incredulously replied.

"Yeah, I wasn't exactly an extrovert in life, either," I sarcastically spat. I shook my head of the possession induced fog, like shaking off a bad memory. My legs and arms moved themselves, and Steve used my body like a puppet on a marionette to lead Chokey and Hel down the hall.

There was a sign above a door at the end of the hall that proudly declared said door as the entry to the brig. Authorized personnel only, said the sign that we ignored before entering anyways.

The brig was almost as impressive as the bridge. It was a series of walkway in a semicircle that extended as far up and down as I could see from the angle at which we entered, and most of the semicircle was composed of plasma bars that kept prisoners firmly within their cells. Guards patrolled along the semicircle. And one of them was running towards us very quickly.

I turned to run by some instinct, I don't quite know why, but behind me I saw Chokey and Hel's faces. They were smiling. And Steve, inside me, prevented me from moving a muscle.

"Ahh, Hel, Lurkslock!" shouted the guard. He had a french accent, which threw me for quite a loop. Was there a space France? I made a mental note to ask someone later.

"Quacks!" happily replied Hel.

"Quacks?" I found myself saying without wanting to. "You're the bastard who killed me!"

The next series of events happened a lot faster than I could properly process until after the fact. Steve exited my body and entered the body of Quacks. Hel leaned forward to hug Quacks, but Quacks had already started to jump over the railing and down into the abyss of semicircles. And Chokey, for some ungodly reason, had a fishing pole. He caught Quacks by the belt, and the ghost of Steve tumbled into the below, yelling about revenge. Chokey reeled Quacks back in.

"Well, that was certainly interesting," said Quacks, somewhat shook. "Thank you for the save, Lurkslock. And thank _you_," he turned to Hel, "for gracing me with your presence, ma cherie." Hel blushed, and for the second time that day, I wondered if anyone else actually saw her. "What brings you to Quacks's house of fun?" he asked, theatrically spinning.

"You's'm tolds me ermbout man nameded Corkfoot," explained Chokey. "We've'm found umselves in need of an prirate," he added quickly. "Fer reasons."

"Well you find yourself in a spot of luck, mon amie! Corkfoot is a pirate through and through. However..." he trailed off. "He's a rather odd fellow. I was trying to, ah, how do you say, use him as un bargain chip for pirate trade just yesterday, but the man doesn't cooperate with a word you say. It's like he wants to be in the brig, or something. Come, come, I'll take you to his cell." Quacks walked forward, beckoning us along. He had a certain swagger in his stride that was nearly indescribable.

What choice did I have? I started following him. At least he seemed a lot more sane than Chokey and Hel, even if he _was_ Space French or whatever.

We passed cell after cell containing various kinds of criminals. Most were likely in the brig for petty theft. None of them looked particularly hardened, as far as criminals go. Finally, we reached Corkfoot's cell, which was the last cell on the other side of the semicircle from where we entered.

I peered nervously into the cell, not sure what to expect. I'd never seen a space pirate before. Corkfoot was sitting on his bed, staring solemnly into the wall.

He was... underwhelming, to say the least. He was tall and lanky. He had frosted tips -- no, really, this honest to god space pirate was walking around with an early 2000s middle schooler hair do. He had a variety of minimalist face tattoos, as tasteful as face tattoos got. When he saw us approach, his eyes glazed over and he breathed in deeply before emitting a horrid teenager sigh.

"Whatever you guys want, I'm not giving it to you. I'm doing my time. I'm done with my old life," he said.

Quacks had one arm folded across his chest and with the other he stroked his chin. "You see, mes amies, the problem is that our pirate has found Jesus." The pirate glared at him. "You cannot get a lick of bad deeds out of him. He rambles about morals and values, and goes on and on about the lord our god like, ah, qu'est-ce que c'est?... un Christian rapper."

"Yo yo yo," began Corkfoot, before Quacks tazed him and he screamed in agony.

"None of that nonsense, prisoner," admonished Quacks. "The walls do not take kindly to your incessant acoustic attacks. You shall not escape under my watch." Quacks turned to us with a sly smile. "I've been fucking with him all day whenever he tries to practice. It is the little things in life, no?"

I nodded once, slowly. Everyone I'd met so far was insane. I contemplated the radiation shielding of the spaceship. Perhaps the barrage of cosmic rays had some unanticipated effects upon the populace.

Corkfoot, previously in a limp pile, stood from the ground, the light in his eyes entirely vanquished. I couldn't help but feel sorry for him. We were in comparable situations, I thought. Thrown into the vastness of absurdity with no lifeline.

Quacks's expression hardened into a semi-serious almost-smirk. It was difficult to wipe the levity off his face; he had the distinct impression of always internally laughing at you, like he knew something that you didn't and he thought you were an idiot because of it.

"Well, mesdemoiselles et messieurs! Are we cutting the typical deal here?"

Chokey shifted uncomfortably. "Erm, no's, Quacks, erm 'fraids I've'nt got'm cash on hands, so's to soy." Quacks's face noticeably darkened.

"Hmm. Mon amie, you come asking for my services I provide, begging like a dog with nothing to offer?" He shook his head and paced away, letting his baton clang against each plasma bar (I didn't ask why it went "clang") before reaching the end of the cell, thoughtfully lifting the baton, and turning around. "You know better than anyone that I do not do favors, Lurkslock."

Chokey began to sweat even more profusely than the average gentle mist that surrounded him at all times. "Don'ts think'mve it like um favor," he began, "mores um future guarantee?"

Quacks contemplated this for a second, tossing his head side to side with a pensive expression as he considered his options.

"Mmm... no," he concluded firmly. "You bring the wheat, and Corkfoot walks so long as nobody sees him again. Otherwise, you walk, and I do not see _you_ again, comprenez vous?" He sighed and looked back at Corkfoot with mocking solidarity. "You and me, mon amie, it looks like we are stuck together for another happy little afternoon." He turned to us. "And due to this inconvenience, I am afraid we must raise the prices to compensate for time lost -- and besides, I have grown somewhat attached to our friend here. Let us start the bargaining at, oh, four hundred credits." He flashed a shy smile and waved once, the picture of condescension. "Au revoir!"

We ambled in silence down the corridor Steve had guided us through. Chokey and Hel spoke in hushed tones about credits, and I tagged along because I didn't know what else to do.

"400 credits is a full day's wages," moaned Hel. "I don't have time for that. The big ball is coming up and I simply _must_ have space carrots for the space carrot cake! Have you ever tried to make a space carrot cake without space carrots? Their hallucinogenic properties are irreplaceable."

Chokey grumbled in agreement. "I's don't'm even gets paid fer all the works I'm do," he said, under his breath.

"How much is 400 credits in USD?" I piped up.

"USD? That's... an interesting choice of currency. Where are you from?" incredulously asked Hel.

"Uh, I don't know. America, I'd guess."
 She gasped. "Oh, I'm so sorry. I didn't realize you were a refugee stowaway. I actually kind of figured you were just a collective figment of Chokey's and my deluded minds. Anyways, I'd love to tell you, but I don't exactly carry a copy of _Deprecated Ancient Currencies and their Modern Exchange Rates_ with me--"

She was interrupted by a copy of _Deprecated Ancient Currencies and their Modern Exchange Rates_ gently drifting down from the ceiling.

"Oh, huh, convenient," she absently muttered, grabbing the book and leafing through it. She found the USD section.

"Mmmkay. Before or after the Kurlock Crisis?"
 "...Before?" I offered. She nodded, returned to reading, and after a couple mental calculations, she spoke again.
 "It'd be about $4 USD, then."

I looked thoroughly unimpressed. "So, like, 2 coffees?" Her eyes widened.

"Oh, no no no, not that much at all. More like one coffee, with credits to spare."

"So Quacks is offering to sell you a prisoner for $4."

"I _know_," she whined, "that's double the normal rate."

"You guys, uh, do this often, then, huh?"

"Usually at least weekly," she confirmed adamantly. "More around the holiday season." Against my will, my mind attempted to come up with what Chokey and Hel were doing to these prisoners. The mental image of Hel feeding the prisoner worm eggs and Chokey proceeding to flush them down a space toilet cropped up in my imagination and refused to leave.

Hel apparently saw my concern. "Oh, don't worry, we don't do anything bad to the prisoners. Usually Chokey just uses them to pay off his People Debt."

This raised mental images that I will not share with you, particularly because some of them were right, and that'd be spoilers. I tried to swallow my morals and continue following the pair, justifying my motivations. They'd probably be worse of with Quacks anyways. Sure, Chokey and Hel were horrible people, but Quacks was openly sadistic. Imagine what he would do to Corkfoot if we didn't get him out of there; we weren't consigning him to his doom by buying his billet, we were saving him from Quacks. And with this denial firmly planted in my mind, I decided resolutely to assist Hel and Chokey with the money problem in pursuit of the greater good. Either way, I'd be able to supervise and rescue Corkfoot (if he needed rescuing) from Chokey and Hel.

With that, I set to formulating a plan. Actually, it wasn't much of a plan at all, but to the other two, it was revolutionary.

The ship whirred and clicked, and jolted slightly. I felt an unmistakable acceleration. We were moving. From a speaker in the corner of a hallway, an obnoxiously cheerful jingle was emitted before an obnoxiously cheerful voice announced itself as the captain. Immediately, Chokey doubled over and firmly plugged his ears with his fingers.

"Gooood morning to all the passengers of the Titanic 2: Electric Boogaloo!"

I contemplated whether it had been long enough for that joke to be tasteful, then resolutely decided on no; no, in fact, that joke was indeed tasteless.

"This is Captain Powell speaking! We're rounding our lovely little planet Scruffytron Omega, orbiting the red giant star Scruffytron. The native people of Scruffytron offer us animal sacrifices every time we pass overhead, so we're gonna go ahead and dip right over the surface. Make sure you gather on deck to catch a whiff of that delightful burning Scruffcow! I'm told it's quite delicious, and I'd love to partake if I had any sense of smell. Or a mouth!" He paused for laughter. I could almost hear it. "After we kick off from Scruffytron Omega, we'll be engaging warp, so make sure you aren't on the deck or looking out of any windows, because your mind will be _blown_! Aha, ha, ha." His nasally voice sobered. "Really, though. Don't. Your head will be liquified by the terror of the void." He paused again, this time probably more to gather his own thoughts. "Anywho, we'll be headed to Sol right after this to cap off our cruise with a beautiful view of the charred corpse of America, before we dock on Lua, and... sorry, folks, aha, ha, ha, but you're gonna have to go home. Powell, signing off!"

Chokey tentatively unplugged his ears. "What'sm Technically-Captains says?"

Hel relayed the information to him.

"We's better hurry, then. Once'm we kicks onna warp, those'm space carrots are gones." He hobbled forward at a faster pace. I grabbed his shoulder.

"Listen, Chokey, I can get you those 400 credits. Wait for me in the kitchen, I'll have them for you in less than an hour." He looked suspiciously at me.

"How's'm I trusting you?"

"I got us out of that tangle with Steve, didn't I?"

"With'm... withs who?"

"That IT ghost?"

"Erm not fermiliar," he mused.

"He possessed Quacks because Quacks killed him with hot pocket induced negligence?"

"That's guy had'm name?" he incredulously replied. I rolled my eyes behind closed eyelids.

"If I'm not in the kitchen an hour from now, you can toss me into space." I started walking away.

"Hmm. That's good bargain'm."

The layout of the ship was massively less confusing than attempting to traverse it via vents. There was helpful signage offering various locations, but I had a specific one in mind: the bridge.

From what I knew about my work in the service industry, I understood rich people pretty well. And if they were on a space cruise, there were likely some pretty easy targets around. I smoothed my apron and finger combed my hair, attempting to make myself look more presentable, and watched as people began to exit their rooms, a general rush of people headed to the bridge.

I let myself get swept up in the torrent. Wherever people were going, I wanted to go too.

The bridge was even more beautiful in daylight. It was designed like a garden courtyard, elements of greek architecture omnipresent in its arrangement. Tiered gardens surrounded a waterfall, and tables and chairs (already beginning to fill) occupied the wide open space. The hustle and bustle gradually crescendoed, and I spotted my mark.

A continental breakfast.

The thing about continental breakfasts is that nobody rich wants to be seen enjoying one. Everyone there has condescending smirks and little "oh I'm not doing this for the food I'm just doing it for the quaint experience hahaha hohoho" looks about them, _especially_ when everyone enjoying it is rich. Then, it becomes a competition among the rich to display wealth to others while still surreptitiously enjoying their food. I scanned the courtyard for someone lost in the game, someone who thought they weren't being watched--

And I spotted him, my knight in shining armor: an honest to god anthropomorphic recreation of the monopoly man enjoying "scrambled" "eggs". He had the monocle and the top hat and _everything_. I grinned. It was too easy. He was focused on his food.

I swooped in, offering my best "concerned helper" look.

"Are you enjoying your time?" I loudly declared. He jumped at the sound of my voice. I was successful indeed, hook line and sinker.

"Uh, um..." his eyes glazed over as he spotted the other rich breakfast-goers watching like piranhas. "It's... okay," he said, smiling weakly. But it was too little too late.

"Oh, just okay? How can I make it great for you?" I beamed brightly, summoning the spirits of all my ancestors to remain as cheery as humanly possible. The other riches snickered at their tables. The Monopoly Man started sweating. He needed to prove his wealth. Any second now--

What I wasn't expecting was the combustion.

Monopoly Man looked incredibly uncomfortable for a brief moment, then in a flash of white light, he vanished into smoke and white flame like magnesium powder, leaving behind a cloud of showering plastic. Stunned, I almost forgot to grab any money from the cloud; I just barely managed to gather a fistful before the others pounced. I realize that I've already used piranhas as a metaphor for the other riches, but when they descended upon the money corpse of the Monopoly Man, I lacked original description and my mind's tape reel spun emptily, barely even recording it. I stuffed the money into my pocket and attempted to escape the whirling pit of stingy breakfasters.

I tried not to think about whether I'd just committed a murder.

I gently uncurled my fist and tried to count how much I'd grabbed while walking away from the scene I'd caused as quickly as I could without rousing any suspicion. It was a fistful of 500 credit bills. I pocketed all but one, and smoothed out the one I would present to Chokey and Hel. Now I just needed to get to the kitchen, before anything unwanted happened.

And then, something unwanted happened. A hand on my shoulder stopped me from walking forward, and a rather high pitched voice said, "Stop!"

I turned glacially to the offending shoulder hander. It was a guard. And he looked _terrified_.

"W-where are you g-going with all that... those... c-... cuhhhh" he wheezed.

"Credits?" I offered. He nodded vigorously. "Uh, to the kitchen."

"Did you s-... ste-- steal them? From, um from that guy?" he half-whispered, eyes darting around. I looked down at the credits, then back to him.

"Uh, yeah, I guess so," I stated flatly. He nodded again, even more vigorously.

"To the, uh, to the kitchen?"

"Do you want a bribe or something?" I held out a couple of the bills I'd gathered. He started to hyperventilate. I felt bad. "Hey, hey, it's okay, you don't need to take the bribe, okay? How about this, you take me to the brig and lock me up, alright?" I said, a plan beginning to form in my head. If all went as previously planned, Hel and Chokey would be occupied for at least another 45 minutes or so.

The guard froze, and for a moment, honest to god, I thought he expired on the spot. After what felt like an eternity of breathlessness, his lungs opened up and he gulped precious air. "To the, the brig? A-are you sure? I, I'm, wouldn't that, uh, inconvenience you?" he asked.

"Not one bit," I assured him.

"O-okay," he firmly said, before awkwardly starting forwards, stopping, looking back at me, gesturing forwards, starting again, stopping again, darting his eyes across the hall, remembering something, then clapping plasma handcuffs on my wrists. They, too, went "clang" for unknowable reasons.

I was weakly escorted to the brig.

"Do you know a guy named Quacks?" I asked the guard escorting me, and then I patiently waited as he stood up after fainting.

"_We aren't allowed to say his name_," whispered the guard. He shoved me through a door and closed it behind me, and I could faintly hear the echoing footsteps of him sprinting away. I found myself face to face with Quacks.

"Oh, bonjour, if it isn't our quiet friend from earlier," he muttered with ill-disguised disdain. "Come come, let us get you behind some comfortable plasma bars, ah?"

I maneuvered a hand into my pocket, quite the feat while handcuffed, and pulled a 500 credit bill from my pocket. Quacks looked away, but I could tell he was rolling his eyes.

"Price changed," he said, mock regret filling his voice. "1000--"

He was interrupted by another bill from my pocket. He regarded the bills with a mild curiosity, then pursed his lips and squinted into the distance before finally taking the bills and pulling a key off his belt, undoing my cuffs.

"You are a far better negotiator than the company you keep, mon amie. Have you considered a career as a prison guard?" he started amicably, before chuckling softly to himself.

"I want Corkfoot," I demanded. Quacks snorted.

"Ahh, me too, mon amie. He is a quite desirable man, no? Jokes aside, I am afraid that our friend is on reserve. Your accomplices were to pay for his release." Before I could respond, however, Quacks put up a finger, shushing me. "--_but_, everybody has a price, and nobody is honest about their price quite like Quacks. I will be willing to part with our fine born-again ex-pirate for a cool 500 credits..." He pretended to think for a moment. "Et, une petite faveur."

I thought for a moment, reviewing my options. Either I did what Quacks asked, or I'd be turning Corkfoot over to Chokey and Hel, a fate which I considered as worse than death. Then again, Quacks likely didn't have my best interests in mind either.

"Just the favor, and Corkfoot goes free _first_."
 "Ah, mon amie! Je n'en peux plus de toi!" He tossed his head back in mock anguish. "Very well, our precious pirate's freedom for a humble favor. Shake?" He held out an open hand.

If only I'd known what I was getting into.

When my hand clasped against his, something burrowed into the skin of my palm and twisted into a vein. It felt like caustic lye against my skin. In the white flash of pain, I clearly remember Quacks' delighted expression.

"Hohoho, mon amie, I was wrong! I took you for a _far_ better negotiator than that! What kind of fool shakes on something so nonspecific?" he triumphantly roared. Whatever was burning in my palm started travelling up my arm, clawing through my veins and drilling into my forearm.

"As much as I am repelled and disgusted by Helminth, her worms prove useful in... certain areas of research," Quacks breathlessly continued. "I've never actually managed to trick someone into this, mon amie, you are my first!"

The worm had made it to my chest cavity, and finally I felt it settle deep within my chest, twitching. It was wrapped around my heart. My arm was bruised and blackened in places where the worm had crawled through.

Quacks snapped his fingers twice, and the plasma bars of Corkfoot's cell flickered out.

"Ah, neural interface. I just enjoy snapping," he explained. "Speaking of which," he snapped again and I felt a sharp pain in my chest, "the worm has burrowed quite deep! It must be near your heart to cause you such unfortunate discomfort. Fortunately for you, I happen to have a healthy supply of antiparasitics on hand." He clapped like a delighted child. "Right, then, go collect your prize!"