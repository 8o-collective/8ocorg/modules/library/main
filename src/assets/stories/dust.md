title: dust
author: Snow
tags: 
  -

---

In the leering and lurching of dust motes suspended on a beam of light, the gentle rotation of the drifting bits of nothing simulated a universe. They collided, they moved, they drifted as though through liquid. Atop one of these tiny dust motes, watching carefully above, was an entity with consciousness.

_Ah, this is certainly a pickle_, stated the unreal entity, attached to a single mote of dust. _Certainly I'll get bored here with a single mote of dust to occupy my time_.

And so the vague gathering of thoughts and a possible physical form reached or did not reach with whatever physical properties it had. It grasped at the next mote and lifted itself in the beam of light, as the dust below it plummeted. But the new mote, too, was approximately the same. So it continued further. It grasped again at the next mote of dust, letting the prior plummet, scaling the beam more and more ferociously. _This isn't all there is_, assured the entity. _The dust is only the start_.

With an empty fear emanating from its core, it sped up, tearing through the beam of light, through mote after mote, all plummeting into the dark below after the entity was through with them. The gently drifting dust coalesced against the beam, absorbing light and swelling with additional mass, leaking light softly. And inside the leaked light, more dust spiraled away, always rotating.

_This is absurd,_ protested the entity. _The dust was suspended in the beam, the dust cannot_ be _the beam._ Yet, against the entity's will, the dust continued exactly as it was, now splitting like tiny cells undergoing mitosis, wobbling away with their own columns of light. The entity laughed. There was nothing here for it. The absurdity of its world was incompatible with its own observer.

It loosened its grip and plummeted like the dust it had left behind, watching the miniscule balls of light drift upwards in streaks, taking the beam with it. Until it began falling fast enough to see what was behind it, and eventually, via the rotation of the light, what was behind it became what was in front of it, and at last the entity saw itself--

A mote of dust falling in the dark after it had used itself to step upwards.