title: a proper title would impose expectations on the reader that im not quite comfortable with
author: Proxy
tags:
  - awful-title
  - features-baked-goods

---

You are waiting in line. You are waiting in line at the bakery. You have been waiting in line for a while now. As it seems you will continue to do so for quite a while.  
You are wondering why so many people are in line at the bakery. Why are you in line at the bakery. Why are you here. You wanted to buy something?  
The line moves forward.  
You wanted to buy something. That must have been it. Have been? It still is. You still want to buy something. You want to buy... You'll figure it out. You have the time. The line is moving at a snails pace. You will be waiting in line for a while longer. You have the time.  
The line moves forward.  
You really should figure out what you want to buy. You don't really want to buy anything. So you should figure out what you need to buy. You are waiting in line after all. What do you want to buy? What are you going to buy. That sounds better.  
The line moves forward.  
You glance around. The line continues behind yourself. That was not very surprising. It ends somewhere. You don't really care.  
You still don't know what you need to buy. You are waiting in line at the bakery. So it is probably something you can buy at the bakery. You didn't come all this way for nothing. You didn't spend all this time for nothing.  
The line moves forward.   
How much time do you have left? You glance around. The line behind you is still there. That's reassuring. The line in front of you is still there. It's smaller than before. You should still have time. You have enough time.  
You need to decide.  
You need to make a decision.  
What are you doing here. You want to buy—.. you need to buy...   
The line moves forward.  
Maybe you should get inspired. There are people buying things in front of you.  
You take a good look at the person in front of you. It's a hat, the back of a head, a mantle and some slacks. Presumably boots. A man, you decide. What would he buy?  
The line moves forward.  
Someone clears their throat behind you. You turn around and see them staring at you. You forgot to move.   
You move forward.  
So what would he buy. What can you buy at a bakery? You really should know the answer to this.   
  
bread?  
Bread.  
You could buy bread. Do you need to buy bread? Does he need to buy bread? You could ask him. You are not going to ask him.   
The line moves forward.  
You are running out of time. The line in front of you has shrunk considerably. You glance around. The line behind you has gotten longer.  
Do you want to buy bread? What kind of person buys bread.  
The kind of person that waits in line at the bakery? Too simple. You are obviously the kind of person to wait in line at the bakery, but you are not sure if you are the kind of person to need— to want to buy bread.  
The line moves forward.  
You can count the people in front of you now. You could before, you just didn't. You count the people in front of you. 2.  
Worrying.  
You have yet to figure out if you are the type of person to buy bread.  
What kind of person buys bread. At the bakery preferably.  
A good person? He will buy bread. He seems like a good person? Sure.  
What kind of person buys bread. A strong person. A just person. Someone who defends the innocent. Generous. Liked. Adored. A Napoleon. An Alexander. They loved bread. You love bread. You need bread. You buy bread. You will buy bread. You've always bought bread. Bread is what makes you you.   
The line moves forward.  
You are READY. He with the hat is the only thing between you and the bread. He wants bread but so do you. You WILL get the bread. You are steadfast in your decision. The bread will COMPLETE you. You are devoted to the bread. The bread is devoted to you. You are made for each other. You were MEANT to wait in line, in this bakery, to buy the bread. It is your purpose. You cannot WAIT for th-  
"A cinnamon roll, please"  
  
  
what?  
  
the world crumbles around you. you try to orient yourself but you can't. you are spinning in empty space. gravity does not work. you are nauseous.  
  
a cinnamon roll?  
you must have misheard.  
he must have misspoken.  
he wanted to buy bread.  
he Wants to buy bread.  
he HAS to buy bread.  
Why is he not buying bread.  
He promised.  
He promised to you.  
  
you feel like you've been stabbed in the stomach.  
a cinnamon roll. A Cinnamon roll.  
What kind of person buys a cinnamon roll. a weak person. an evil person. a person with no face, no morals. a proxy for something real. a lie where something should be.  
a whimper escapes your lips.  
He turns around.  
  
he  
He  
He who BETRAYED you  
He who MOCKS you  
He who laughs at you and all that is good.  
  
You look at him.  
You LOOK at him.  
  
this is no He.  
this is no man.  
this is a beast.  
glowing holes in the middle of it's face. sharp fangs inside a mouth that grins at you.  
Your eyes dart around. You are trying to catch details of a.. thing. Of the Thing. The Thing in front of you.  
You can't.  
The Thing is fuzzy around the edges. Every time you try to make out where the Thing ends and the void begins your eyes force you to look at the holes in the things face.   
It knows what it's doing.   
It was toying with you.  
It was never your ally.  
It was never your friend.  
It was never going to buy bread.  
  
You see the horns that lift up the beasts hat. They seem concrete enough to focus on.  
You can do this. You can regain your footing.  
You can make it pay.  
You will make it pay.  
  
The Thing notices?  
It turns around.   
It gargles something you cannot understand.  
It. leaves?  
  
you win  
You won.  
The Thing is gone.  
You scared it off with your resolve.  
The world around you returns.  
  
The line in front of you is gone.  
You quickly check behind yourself. The line behind you is not gone.  
You move forward.  
You are prepared.  
You know what to do.  
You know who you are.  
  
  
  
  
  
but maybe?  
maybe you could?  
you know who you are, right?  
why should the outside matter?  
you don't need to perform your identity all the time, right?  
you know who you are, so why not take a day off?  
everyone knows you usually buy bread.  
you are safe, the You is safe.  
why not try something different for once?  
just once.  
  
Just once.  
