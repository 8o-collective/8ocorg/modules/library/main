title: Spider's
author: Snow
tags: 
  - spiders
  - annoying

---

Spider's. If it wasn't, it'd have some difficulty being described in ink. Solipsism dictates think means is, but spider's without thinking. Spider am, therefore, without thinking. And spider's beyond any annoying solipsistic viewpoint.

Spider's pragmatic. Pragmatism has had its faults in the past, but the direct route is at least wrong faster. Without much thought for the flies caught in its web, it will devour the insects for the sake of preservation.

Spider's and will continue to be, propelled by some innate need to is/am beyond sentience's ascription to purpose.

Spider's, but that's beyond the point. I killed 45 people this year.

Is this excessive? This seems excessive. 45 is a lot of people. It's so big, that even though it looks better in words (forty five has some gravity about it's appearance), digits are a courteous shorthand. Forty-five. Forty five. Four ten three plus two one.

I'm a doctor as much as anyone else has any right to be a doctor -- holding a screaming scalpel while trying to smash skin together fast enough to keep people alive. This presumption of keeping people alive is the error in mainstream medicine. Some call me a narcissistic idiopathic psychopathic murderer with a savior complex convinced with my moral mission of saving people.

Truth be told, Med School is just very expensive.

I tend to lack funds. It's become a default state of being for me. Being a med student, I do have access to certain compounds and facilities that may more easily (if illegally) procure currency, but they say to do what you know, and I know surgery. And murder.

Murgery.

The first wasn't too special, but people have some infatuation with firsts anyways, so I will oblige the easily excitable among the audience. Med School is a prestigious institution. The town that surrounded it was full of hospitals, nearly two every block. The competitive nature of capitalism in such a hospital-drenched climate led to some extremely low prices and extremely questionable injuries treated with extremely experimentally ineffective treatments. "Budget cuts" became synonymous with "placebo effect" as prescribed by doctors, and even beyond the hospital graveyard of the outskirts of the city (we call the abandoned blocks of once-hospitals "The Docks") venture capitalists threw money at new ones constructed weekly if not biweekly.

"The injury rate is rising," protests the suit, money dribbling from his jowls as he speaks, loose change coughed up and scattering on the table. "People clearly want hospitals."

And that's about why I found myself jamming a scalpel into Mr. Jones' spine.

The first man on the moon was lauded by critics. Absolutely fantastic performance from Sangrev Duskanski, the Soviet who landed on the moon 4 years before the Americans did. The only difference between the first man on the moon and the "first" man on the moon is that the "first" man on the moon lived. Dead astronauts is bad press. So is dead stockbrokers.

And so The Docks expanded. More hospitals, higher med school costs, more murders, more bodies, more money for spiders. Caught in the cobwebs, I was forced to cut my way out. And so I did. I was freed. People didn't talk about the labyrinth of hospitals, people didn't talk about the murders, people didn't talk about Sangrev Duskanski.

Sangrev made it back to Earth. In charcoal chunks over Siberia. DNA testing concludes that the chunk on my mantlepiece is indeed 100% astronauts. I collect charcoal. People charcoal is rare unless you make it yourself, and what's the fun in that?

I was born in The Docks, and I will likely die on the fringe of The Docks. Like Icarus, I so nearly reached the sun before my wax wings melted, and like a fly I was caught in the web.

You may be judging me, before you go home to your cozy cottage in the country having travelled forty five (45) miles to the nearest bookstore, perusing this novel and leaving the store post hence post disgust post postage, mailed home in a nailed coffin for your happy thoughts. I don't blame you. As this narrative is retrospective, you're learning what my mental state is like after I'm caught by an endless swarm of spiders, gently biting enough for a grip on my flesh as I screamed and grabbed desperately forward, almost to the gates of Eden before being dragged back into The Docks.

I lodged a scalpel between C3 and C4, having missed the crux of C1 and C2 (atlas and axis) due to my nerves. My nerves left him with a nerve deficiency -- unnervingly enervated at my nerve, Jones's nervous tic tacked nerves beyond my scalpel, and a twitch made his death more painful. That shows him for having Tourette's manifested in physical tics. That shows hand for shaking during a routine operation. That shows spiders that The Docks can't save themselves.

If I'd started smarter, I could have gone on longer.

Hospitals demand a stream of ill. If they couldn't sustain a healthy intake of unhealthy, they'd wind up expanding The Docks, winding inwards but untouched by the glorious barrier around Med School. And so some hospitals began to engage in smart business practices. Outsourcing ambulances? Sure! Like Uber, but your wife is choking on a sharp brooch and drowning in the bathtub. But the most unquestioned questionable business practice was a victim cost.

Hospitals needed numbers. Spider's unforgiving, and so is Wall Street. The numbers meant everything on the edge of the docks. Victim cost was a reward to the victim for choosing the hospital, in the form of a discount to their treatment. An absolute, $4000 discount to victims of grievous bodily harm. Sometimes, there's nothing a hospital can do to save someone. Sometimes, those $4000 dollars is a greater discount than the $0 it costs to treat a dead person. Sometimes, the one who brings in a victim can take ownership of the victim cost if they promise to cover the costs of the victim's care.

Bring in a corpse, the numbers go up. They pawn it to Med School as cadavers. They pocket the majority of the profit, but the corpse-bearer is compensated.

"I'm so sorry, Mr…"

"It doesn't really matter. He's dead?"

"We used a compress to try to stop the bleeding. We performed surgery to reattach the spinal cord. We're keeping him on life support but--"

"Life support?! How the hell much does that cost?"

"$140 an hour."
 "Pull the plug right now. It's what he would have wanted."

She barked into a walkie-talkie. The lights flickered. Someone somewhere died. I hoped it was Jones.

"Alright, the cost of admission, plus the life support, the compress, the surgery…"

"Spit it out."

"$1700. Will you cover the costs?"

I checked my bracelet. I'd have to go into debt for a minute, which would be another $500 overdraft fee. Shit.

"I will cover the costs." My bracelet blinked happily. She bustled away, and after a minute of waiting, she returned with a grimace.

"As you may be aware, Southsouthsouthwestwestern Child Adult Mercy Savior Private Hospital offers a victim cost discount of $4000. You've been reimbursed for you patronage. Thank you very much and have a nice day." My bracelet chimed cheerfully and the number on it blinked up faster than it ever had. Even with the fees, the sloppy execution, everything… I made $2200 in a single afternoon by stabbing a man nobody would miss in the neck.

My second quarry was Mrs. Styx. She was a cleaner in the darkest part of The Docks where I lived, Southsouthsouthwestwestern Quarter formally, "Seraph Exit" colloquially.

"Quarry" is an unfair word for a two-way hunt. She'd been hired as a part of a team to clear a hospital on the edge of Seraph Exit: my home, the First Mercy Hospital (ironically not even the first "First Mercy Hospital").

They were woefully unprepared for an army of squatters who weren't keen on being sold to Med School as corpses.

I grabbed Mrs. Styx by the mouth as she screamed and ran down the hall. Her assault rifle had been stolen, and a large gash separated the skin of her forearm. As my mother taught me, I estimated the depth and danger the gash posed as she attempted to scream and fight me off while I dragged her into an operating room. A deep laceration. It'd require suturing.

I barred the door shut and tied her hands and gagged her with bandages. I sutured her wound as she struggled, which was actually incredibly difficult. The administration of topical analgesic did little to alleviate her struggle, so my guess was that her strange behavior was psychosomatic. The suture kit and topical analgesic ran me $50 at the monolith-like machine that leered over us within the room.

I'd never been fond of the way the squatters tore them apart. It was messy and a waste. Mrs. Styx was more valuable to me alive until the slaughter. When she calmed down, I ungagged her.

"What do you want?"

"Money."

She spat at me. I reminded myself to sterilize the floor later.

"You pigs. You live here in filth in The Docks, contributing nothing but corpses."

"What's your name?"

She shifted uncomfortably. "Melanie."

"Last name. The hospital only needs a last name."

"The hospi--... Oh, god." She doubled over and began to sob.
 "Last name. Stop crying or you'll reopen your wound." I didn't need practice suturing. There was always plenty of first-aid work in The Docks, especially Seraph Exit.

"S-Styx."

I smiled gravely and replaced her gag. I tied more bandages over her eyes and started searching for a gurney as she began screaming again.

The story starts kinder. Before Jones, before Styx. Before forty three more after them.

Like most children, I was born in a hospital -- First Mercy Hospital. I was dragged into the world kicking and screaming, but I was also lucky.