title: JAMES
author: Snow
tags: 
  -

---

"Listen, James, I can do what I want with my money," drunkenly mumbled James. "Stop tryna… tryna stop me from using this here chip money for more chip," he demanded.

The room was barely lit. A single sparse light bulb dangling by a wire grimly cast a harsh white light off the cracked concrete walls. The wire was screwed to the wood ceiling, pink insulation peeking through the rafters. The dirty grey floor was covered in the ghosts of smashed beer bottles, glass long swept. Four folding chairs were arranged around a card table, and atop that card table was a poker game. In those chairs was James.

"Don't not don't do that, Jems," James replied. "Is bad news," he wheezed further. He stared at the other players. They had money for days. James would pry that money from their hands or his name wasn't James. He wouldn't let the name-gods take James away from him. That was his only characterization besides his drunkenness, his willingness to be in such a seedy location, and his seeming ability to sit in multiple chairs at once.

James was bad at poker even when he wasn't drunk, but paired with alcohol he was impossibly bad at keeping track of the cards and rules. The other players waited eagerly for him to play his cards.

He grinned deviously at the pair he'd been dealt. James raised an eyebrow at James' expression. Surely James was just Jamesing them.

But he wasn't.

"Read em and weep, boys," James said, splayed his hand out on the table. An 11 and a Walmart gift card. Royal Marketplace. The other players started cackling, and James squeezed his eyes shut.

The dessicated bodies in the other chairs never got a chance to play their hands.

James was satisfied with his victory against James, James, and James, but something felt wrong to him. Since when did James give up a game so easily? It wasn't like James to admit victory so quickly against James. Now that he thought of it, what was James even doing there in the first place?

"If you're James," He waved his hand around the pile of dust to his left. "And _I'm_ James," He patted his chest twice, too slowly. "Then who…" He chucked a four of hearts at the dust to his right. "...Is James?"

"I'm James," Said the pile of dust directly ahead of him.

James squinted tiredly in James' direction and pointed back at Left James. "But… He's James!"

Right James shook his head. "No, he's not James. James won the poker game."

"I won the ponker game," James slurred. "So I'm James?"

Yes, he was James. Right James, Left James, and Forwards James were all piles of dust.

"_This,_" Professor Henry rubbed his temples, staring at his subject through the glass next to his favorite student. "Is why you don't mess with my experiments."

Dr. Nelson had been Professor Henry's student for years, and with his help, he had recently received his doctorate in abnormal psych. And maybe, yes, it was a stupid thing to interfere with his teacher's work, but he knew that he could take the experiment to the next level. "You have to admit, the results are fascinating," He laughed. "I gotta go home, I'll be back early tomorrow. I'll see you later."

"Yes, yes, go home to your wife," the Professor waved him off. "Oh and James?"

"Yes?"

"What do you want me to do about… 'James,' over here?"

Dr. James Nelson chuckled to himself. "Just let him keep talking, he'll wear himself out. The other three did."

"The other three weren't like this one."

"He'll wear himself out," James repeated. "Trust me; I am James, after all."
 The professor let a smile linger on his face as James exited, then it gradually wiped away as there was nobody left to lie to in the room. He looked back at the glass. James tapped the table with the gift card, listening carefully.

"Tap, tap," he said. This was horribly wrong. LCR wasn't invented for this. He would have to chat with James -- the _real_ James -- about his ethical obligation.

"Tap, tap," reminded James. He stood from the card table and listened. The concrete in the room leaned in to listen. Tap, tap. The concrete didn't answer. He called again, with no response.

"Tap, tap?" tried James again. He found it. The reason. The room was dead. Acoustically failing. James struggled to remember which wall it was. He stood from the table, and dragged the card against the wall. Scratching, scratching. The sound echoed off the walls until James reached the next one. Scratching, scratching. And then the next wall.

Silence. No texture. This wall wasn't concrete. James tapped the card against the wall and smiled. Professor Henry watched, fascinated.

"Neat trick, huh, Professor?" he whispered. Shivers travelled down the professor's spine, and he instinctively reached to the reassuring kill-switch. James' grin grew wider.

"Excitation of the 5-HT receptors, right? Disrupting diffusion of serotonin. Causing… among other things… psychosis." The card in his hand dissolved into the familiar ashes. "I knew I was doing this to myself, you know. I could tell you all my secrets, everything I remember."

James tapped against the glass with his card, again. Tap, tap, tap. Faster, faster, faster.

"Where am I? I deserve to know," he whispered. The professor eyed the intercom, but instead held a flashing green button on the side of the LCR. James' eyes grew wide.

"So you're keeping me?" he giggled.