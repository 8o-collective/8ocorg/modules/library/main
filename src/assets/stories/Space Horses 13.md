title: Space Horses 13
author: Snow
tags: 
  - srs!!!
  - marketable-franchise

---

Crime Stanley was sitting in his office, which was small and damp and full of dust. The dust itself was damp too. Crime Stanley didn't know how it got that way.

He was crammed into his too-small chair that he had stolen from a kindergarten, because Crime Stanley was a criminal who stole many things, like tiny little chairs. It didn't help his size situation that he was a larger man — in dimensions, and in behaviors. His briefcase was a temporary desk, and on it was some paper that he pretended to write on, knowing well that anyone who would enter his office would point and laugh at him if he wasn't writing or working or doing something busy. But really, Crime Stanley's business these days was waiting.

He itched his disproportionately small nose, tucked underneath his disproportionately small eyes and above his disproportionately small mouth. Crime Stanley was almost immediately recognizable by his tiny face, which is rough, especially in the business of wanting to do crime. Most police would immediately recognize him, so when he went out and about, he wore a mask that was of his own face, but bigger. Unfortunately, the copy-shop misprinted the mask, so his faux face was disproportionately large. He hid his fake giant face under a large fedora. He liked to dress like he was a private eye, but instead of searching for criminals, he searched for crime. To do, that is.

A mousy woman with darty eyes and an old dress she'd bought at a second hand store opened the door timidly and stepped in, closing the door behind her.

"Stanley," she said, urgency in her voice.

"Franklin!" responded Crime Stanley, way too loudly.

"It's Benjamin," replied the woman. "My name is Benjamin, Stanley. We've talked about this. I go by Benji."
 "Benjamin Franklin!" delightedly continued Crime Stanley.

"Ok," continued Benji, even though things were not ok. "The ship is docking at Port Irsgranf in two days, and someone didn't name it by whacking their keyboard until something pronounceable came out."

"Oh no!" said Crime Stanley, rising to his feet. It took him a hot second, because there was a lot of him to rise, because Crime Stanley was a very tall man. "Franklin, I would do anything to protect you from the space police!" And before Benji could clarify that the space police weren't after her, Crime Stanley began an unskippable cutscene where he walked to the door and opened it by twisting on the doorknob. During this cutscene, it is revealed that Crime Stanley did not live off of the money he made from crime, but off of a large crime inheritance from his father, the majority of which he acquired by getting his sister written out of the will. This is gonna be a big twist later, so try not to remember that.

The door indignantly opened, annoyed that Crime Stanley didn't just ask. Not that the Door Subsystem of the ship AI wasn't used to rudeness from its passengers. Tourists, mostly. Tourists who wanted to go to space but couldn't afford their own horses.

Crime Stanley stepped out of his perpetually damp dust office, and the camera panned slightly to the right, revealing that the prior exchange occurred in a broom closet. Crime Stanley said that being a janitor was his cover when he wasn't doing crimes, but he rarely did crimes because he was so busy pretending to be a janitor. He flipped his too big face onto his too small face, and began scanning the bridge for crime to do.

The cruisehorse was nice. The SS Neighbor, but when you said it, you had to say "neighhhh", like you were imitating a horse. It was in the ship handbook, which Crime Stanley had stolen so that he would better blend in as a janitor. The bridge was mostly full of oil barons and monopoly moguls. Some of them were even tycoon magnates. They had money brimming their pockets, and anyone with sufficiently sticky fingers would find it as a crime doing paradise. But Crime Stanley was no ordinary criminal. He wouldn't settle for petty theft — no, anything less than scheming was beneath him.

"Stamley!" gruffly shouted a manager with a square head and gorilla arms that he insisted were legal biomodifications. Stamley was Crime Stanley's janitor not-crime name. It was how he avoided making a name for himself, but a lot of the time people thought he was saying "Stanley" and he had to correct them. Crime Stanley stood at rapt attention and saluted.
 "Yes, Mr. Chic-Fil-A?" responded Crime Stanley to the man who had sold his last name as advertising space to a multibillion militia that had gotten its start as an Earthling fast food chain.

"I need you to mop a lot of floors! After all, this ship is gonna be _crawling_, absolutely _infested_ with _oil barons_! There will be so many rich folks on this ship soon! All of them are here for the big horse race, the grand prix! I heard Miles Davis (no relation) was going to be racing in this here super big huge horse race! You gotta lick these floors clean with a wet mop, hear me?" spat Mr. Chic-Fil-A, as Crime Stanley furiously mopped up his spit.

"Of course, sir!" dutifully answered Crime Stanley, and then Mr. Chic-Fil-A kicked him straight in the noggin and said, "_Fuck_ people who work for minimum wage!" Crime Stanley fell backwards into a puddle of half-mopped manager spit and nearby moguls erupted into cacophonous laughter and assaulted Crime Stanley with a barrage of pointing. Benji pushed through the mocking crowd.

Crime Stanley wiped away a tear from his face as Mr. Chic-Fil-A stomped away, and Benji put a reassuring hand on his shoulder.

"It's ok," soothed Benji. "He's just jealous of how good at mopping you are."

"He doesn't need to mop at all!" wailed Crime Stanley. "He just sold his name as a corporate sponsorship for a position on the managerial staff!"

"Yeah, well I bet the captain kicks _his_ noggin twice as hard!" encouraged Benji. She thought for a second and bit her bottom lip, watching the devastated Crime Stanley sobbing on the ground, providing entertainment at the behest of the megarich.

"Besides, now you tell me that we're landing in Port Irsgranf in only two days? That's eight months on the Earth calendar! How will I scrounge up eight million spacedollars by then? That's a whole million spacedollars every month for all eight of them! Nobody can crime that fast! The shady criminal syndicate to whom I owe this vast sum will have my gosh-darned skin!"

Benji only shook her head in response. "We'll figure something out, Stanley. I promise."

Crime Stanley stood and started mopping again, and the inquisitive richfolks settled down to their soups and salads. He stared across the food court and looked out the massive window. The glass was tinted darkly as the Sun loomed starboard. Stanley stared into its orange writhing majesty and thought, _man, I'm gonna own that sun someday_. And he was incredibly, horrifically wrong. Not even touching the geopolitics or semantics of ownership, or how he'd have no possible way to enforce "ownership" over something as vast as a star, it was a well known fact that the United Space Galactic Superfederation already owned the star, and it was full of commies. There was no way to own the Sun unless the red-blooded Mumerican Army, most of whom were stationed aboard the SS Neighbor, overthrew the USGSF.

Crime Stanley began to hatch a plan that involved ownership of the sun, overthrow of communist rule, and eight million spacedollars owed to the Syndicate, owed because of promises his father made that he couldn't keep. It was a stupid and terrible plan, but it incidentally led him to the Stables. So instead of outlining this horrible and irrelevant plan (I think that Crime Stanley meant to commandeer several mecha-horses and drive them into the Sun until the space commies surrendered), we can just use it as an explanation for why Crime Stanley found himself walking towards the stables of the SS Neighbor, steel in his eyes and grit in his teeth and milk in his coffee and barista hot on his tail who was saying things like "hey, you need to pay for that coffee" and "where did you even get that glass of milk".

The SS Neighbor was huge. Relative to a human, I guess. So, compared to most Class III SpaceHorses, it wasn't that big. It was mostly just a pleasure ship. But it could fit a whole lot of people, because despite Mumerican protest, the USGSF had converted humanity to a mostly post-scarcity society and mega-structures are like, 90% of post-scarcity. In order to distill this concept down into your dumb tiny little human brain, I need to compare the size of the SS Neighbor to something you are familiar with. Size of a big ol' city. Not super big, but dang, you could fit like a couple hundred thousand onboard. And there probably were a couple hundred thousand onboard, too, but soon there would be millions because of the big horse race coming up, the grand horse prix, in which Miles Davis (no relation), legendary communist horse racer, was gonna race and be expected to win. This would seal the USGSF's cultural hegemony in the galaxy, and once and for all silence the last vestiges of morale of the Mumericans, and capitalism would die with a sorrowful whimper.

The Stables were quite far from the Rich Bridge, of which Crime Stanley was assigned to be janitor of, so Crime Stanley hailed a horse taxi and clambered inside, beckoning Benji to join him. But before Benji could step in, he realized that there was already another in the taxi.

"Oh! Uh, sorry Benjamin Franklin. Catch the next horse to the Stables, ok? I'll meet you there. Love you!" called Crime Stanley, and then he blushed because he didn't mean to say "love you" but also wasn't really sure if he actually meant to say it, because it made him happy to hear those words come out of his mouth, but then he became even more super embarrassed and flustered because he didn't want Benji to think he was a big dweeb.
 "Ahhh, geez," muttered Crime Stanley to himself, "now she'll think I'm a big dweeb." He was a deep shade of red. He turned to his co-horse-habitant.

She was an elegant lady, clearly of the upper upper crust. She had a real long pipe in her hand — so long that Crime Stanley couldn't actually see the end of it. She had one leg crossed over the other and was wearing a long sequined purple dress. She had sunglasses on top of a disproportionately tiny face.

"Headed to the stables, hmm?" asked the woman, holding out a purple-gloved hand for a handshake in the back of the cramped horse taxi. "What a coincidence, that's where I'm headed too. Name's Arson Julie."