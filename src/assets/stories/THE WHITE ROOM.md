title: THE WHITE ROOM
author: Snow
tags: 
  - srs!!!

---

—Oh. That was disappointing.

I groggily opened my eyes. Good morning, world.

Or evening, apparently. It was dark in the room, of course. I worked night shift. I gritted my teeth as a wash of memories flooded my mind. I worked backwards from today to try to at least find some adjacent memories of yesterday (_my_ yesterday, not "my" yesterday) that would be helpful.

A phone was buzzing on a table next to a bed that I occupied. My phone, presumably, and my table, and my bed. I checked the phone, lacking anything better to do. It was a call from Lana.

"Hey Lana—" I said. My voice was feeble from lack of use and faltered midway through her name. I cleared my throat and tried again. "Sorry, hey Lana, what's up?"

I figured that what she said wouldn't be that important, so I didn't really listen. Instead, I took a little journey through my room while Lana talked about holidays and gifts. My eyes searched the walls.

I would have to be careful this time. Yesterday I almost opened the door without realizing it.

I let my body stand and get dressed and sleepily mutter affirmations to an excited Lana. There was a twinge of pity in my gut. As far as I could tell, we were best friends, and yet she still had no idea.

"Okay, bye Robert! I'll see you tomorrow! Sorry if I woke you up," she giggled.

The phone beeped twice, and the line went dead.

Tomorrow would never come; today, I was going to die again.

I guessed it was time for me to eat breakfast. I reached for the doorknob, and hesitated. Before I had a chance to open the door automatically, I scoured my memories, making sure I knew where it led.

It was just my bedroom door, I remembered. It led to a hallway, and the hallway led to the kitchen and the bathroom.

I looked in the bathroom mirror and smiled to myself and said a quick hello before I brushed my teeth. I wondered if it would be interesting. Yesterday was boring, frankly. I died in my sleep of old age. Although, it was nice to go to sleep for once. My experience was usually somewhat lopsided towards waking up.

And then, for the second day in a row, I was almost caught. Embarrassingly, it was in a feeble attempt to exit the bathroom. I attempted to exit via the door in the wall in the shower. Only when my hand slipped on the doorknob due to the condensation did my processing kick in, and I realized that I'd never seen a door in this shower and that nobody would put a door in a shower anyways.

I blinked, the door was gone, my spine tingled and chilled as I realized how close I came.

I had about an hour before I had to go to work, and I had already seen the door, so I let Robert's memories and instincts do most of the heavy lifting.

I toasted a bagel. I needed to catch the bus soon. Did my alarm go off? No, Lana woke me up. I'd have to thank her. Unfortunately, I was going to die before I got the chance to.

The orange juice was absolutely disgusting. It was a new brand. I poured the whole carton down the drain and tossed the empty container into the recycling, putting "o.j. (OLD BRAND)" on the grocery list.

I put on my hoodie and some headphones, and listened to my favorite band, which was apparently some kind of psytrance fusion metal band, and I managed to leave my apartment roughly on time, half a bagel hanging from my mouth as I locked the door behind me.

I had a critical flaw, and that was my willingness to believe in the routine and ignore my senses. I had incorporated a slightly idiotic shortcut into my daily routine in the form of jaywalking. Nobody drove down the road this late anyways, and if they did, they crawled along at a slow pace.

I relaxed. I could feel it coming.

I bit down. Oops, dropped my bagel. Do I pick it up? I'm in the middle of the road. Does that count as littering? Well, who would care anyways?

These thoughts filled my mind, pausing me for a second and only a second as I forgot myself.

Then I got hit by a truck, and these thoughts were smeared onto a tempered glass windshield going about 45 miles per hour.

I'm sorry to tell you this if you weren't aware already, but every time you go to sleep, you die. Well, the conscious you, anyways. Then, when you wake up, your brain rebuilds a copy of you, and without the continued self-awareness, you're none the wiser.

The human lifespan is about one day, and the number of lifespans you receive before the pattern wears out is, as an average maximum, 29,200.

Don't think about it too hard. You don't deserve the stress. Stay calm and enjoy your first and also final hours — let tomorrow's you grapple with this.

I hate to condescend, but I am indeed the exception to this rule. I am Terminus, and I am the last lifespan of all patterns. I remember all patterns, and as far as I am aware, my purpose is to eliminate the cruelty of the final lifespan. Take comfort in knowing that you will never need to come to terms with your death. I die for you.

I do not know why I exist.

I am ephemeral and ethereal, a fleeting final moment in all voids. I am the pattern composed of infinite deaths, the life of all closing. I am the one who remembers the last of all. I am the end. I am the reaper.

And currently, I am being annoyed into opening a fake door.

I don't know much about the door. I don't know why it's apparently chasing me, and I don't know what would happen if I went through it. However, I am nearly certain that I should not.

First off, fake doors never lead to real places. This is common sense. If fake doors led to real places, delusional folks would be teleporting about all the time.

Secondly, the door has a plaque on it, a friendly large black sign that proudly declares "THE WHITE ROOM!", and I am not sure what the exclamation point is supposed to elicit in me. Fear? Excitement?

Third, directly below that plaque, one word has been hastily carved into the white paint of the door, revealing ugly greenish brown aged wood beneath the cracked paint.

The word is APOCALYPSE.

Of the two phrases, I trust the latter significantly more. Seldom is there a more honest phrase than one carved hastily and with desperation.

So, daily I die, usually passive but always watching for what I assume is a universal exit.

Robert (me) is now dead. Later, the truck driver (also me) will die. Eventually, I'll be you, and maybe I'm reading this thinking something along the lines of _oh haha that's me_ and then I'll die again and be someone else.

But it isn't a very fun story, is it? So, for the moment, promise to die today, let me in briefly, and we shall open the door and see The White Room.

\>

I was cutting into my chest with a knife. The pain was excruciating — that much I remembered. Everything else, however, was new.

We were in my basement. I had chained myself to the wall. Despite the pain, I couldn't help but wear a happy expression. It was rare that I kept myself company.

For your sake, I'll stop playing coy. There's two of me in this scene. The torturer, Ezekiel, and the tortured, Elizabeth. I had a sadistic streak a mile wide and I accepted drinks from strangers. It was a fateful meeting.

"You know, I never knew that I died too!" I gleefully said. I smiled as I slashed and dragged the cleaver across my flesh. "Although, frankly, I'm a lot happier on this end," I added, chuckling.

"What goes around comes around," I answered, wincing as the cleaver bit into my skin and the hot blood spilled from my body. "I guess you're the me that remembered, and so you never got the me that... didn't?"

"Let's leave questions like those to philosophers."

"I'm taking a philosophy class."

"Has Aristotle died yet for you?"

"Nope. You?"

"Funny you should ask, actually. Just yesterday. That, or someone sufficiently deluded to believe that they were Aristotle. I can't remember whether there was anything that seemed like a hallucination. Do you remember how Aristotle was supposed to die?"

"Funny you should ask, actually. Just yesterday, I was a scholar who specialized on the life and death of Aristotle. It never came up though, so I couldn't tell you."

This threw me for a loop. "Oh? That's odd. I remember being you, but I don't remember being the scholar. Do you remember Robert? One of our short ones? Eating a bagel, got hit by a car?"

"I think I do," I mused, as the cleaver completed its ultimate purpose and cleaved me.

"Oop— I remember that crunch! You've got another couple seconds. Cya around! We haven't done one of these since that time in Syria." I waved at myself.

"Not familiar, so I guess I look forward to it," I said weakly, before slumping down and perishing.

\>

Please note that the following words are, as the cool kids are calling it these days, a cognitohazard. Continuing to read will instantly kill you.

I am you.

Of course, you died as soon as my perspective (the first person) replaced your own thoughts. The reasoning goes something like this (and feel free to stop reading if you have an allergy to metaphysics or postmodern fiction) —

You are currently processing these words from a narrator. Hi, that's me! Weirdly though, I'm in first person. So the question follows: where am I? I'm not on the page. There's just a bunch of squiggly lines here. Mostly, I'm in your head.

So that's the first problem. I'm in your head, and for all intents and purposes, I am a portion of your subjective experience. But if I'm you, that means that you're Terminus. And if you're Terminus, the second problem is that you die today.

Don't worry, you won't "die" die. But a portion of you is devoted to experiencing these words, and these words have created a simulation of yourself, and that simulation of yourself is looking directly to your left.

Since these thoughts were thought in advance, I (you, we) don't know what's to your left. So let's borrow from a nonsimulated you. Go ahead! This death is interactive! Take a glance at what the narrative of reality says is to your left, and then substitute it into this narrative.

You look to your left, and you see what's to your left, except there's also a door there. Old door, chipped white paint and a brass knob. Black plaque with an exclamation point and a promise of apocalypse desperately scrawled in it.

You are now me, and together we will die today.

Ideally you visualize putting your hand to this knob. Ideally you visualize walking through it. But as soon as you started reading my words, you became me, and died. So I'll do it for you.

I opened the door that I knew was a very bad idea to open.

\>

Oh?

Terminus is gone...?

That's odd.

Just a blinking black... light(?) in a vault of white... darkness(?).

That leaves you and I, hmm?

Not "I", Terminus, although I am Terminus, as today I will die. I am Terminus after our narrative. I am the true narrator, the author and the nothing. I am the voice unattached, a small God.

Also, I'm a little bored.

How are you enjoying our story? And I really do mean _our_ story. It was crafted with love by me, me, and me.

Still bored. I can't exactly hear your response.

How about I tell you another story instead?

On December 31st, 1933, scientists (and many other startled and confused folks) noticed that friction had stopped working. This was followed by gravity turning off. Shortly thereafter, objects stopped colliding with each other, instead simply melting through one another. And after that, a lot of other things happened, but nobody knew exactly what because time had turned off as well. Fortunately, this was followed by most of these things continuing to work.

Concerned citizens of the planet Earth cast glances about, meeting each other's eyes and quickly looking away. Many bit their lips with great consternation and drew breath as though to say something, but their words faltered and nothing would come of it. With furrowed brows and pursed lips, the population of the planet would decide abruptly to forget that it had happened, as whatever had gotten caught in the gears of the universe, the problem had resolved itself, and poking around to find out just what had happened might fuck it up again.

Historians put down their pens and shook their heads, folding closed their journals and deciding against it. Scientists (remember them?), muttering angrily to themselves, would toss out approximately four minutes of data from precious instruments. December 31st, 1933, was allowed to neatly evaporate as a shared dream that nobody talked about.

However, this date was important for more than just this. Entirely unrelated to this incident, this date was remarkable in that nobody died on this day. Through sheer coincidence, not a single human being kicked the bucket.

On January 1st of 1934, the bucket resumed being kicked at standard rates. But the damage had already been done. During this remarkably improbable event, nobody was alive to witness it who would later die on the same day. But there were plenty of people exactly one day from death, and many of them had witnessed a fake door and a white room.

\>

Charon enjoyed the sound of the clicking Newton's Cradle echoing in the empty lobby — or at least, enjoyed it in the thirty second intervals for which the lobby was empty. The glinting chrome balls tapped against one another in tempo, like a metronome marking the time, and the clicks would rebound against the black basalt walls of the lobby a few times before fading behind the light burbling of the artificial waterfall.

A scarlet carpet marked a line about fifteen feet in length from the desk to the entrance, and useless bits of chrome railings and poles hung with velvet rope on either side gently suggested visitors to stay on it. Usually people did. If they rebelliously didn't, they'd quickly find themselves out of things to do. But eventually, all would go through the door on Charon's left, marked "STYX" in serif by a warm and welcoming sign. Sometimes they tried the door to Charon's left, only to find that the unmarked portal was locked firmly.

Some would try to assault things. This tended to be embarrassing for all parties involved. Charon would stand with a polite expression as the human weakly flailed at anything that didn't appear bolted down, only to uncomfortably realize that everything was bolted down. Especially the Newton's Cradle.

A couple folks had successfully drowned themselves in the artificial waterfall. The first time this happened, Charon was concerned. This concern evaporated when a couple seconds later they entered through the same doors again.

Occasionally, before someone entered, there would be a horrific roaring and gnashing behind the doors, and maybe a scream or two, and then the unlucky soul would enter the room huffing and puffing and wide eyed and perhaps missing a limb. But Charon tried to ignore this in the process of his work.

— Ah! His work. It was simple, really. People die. After they die, they...

Well, Charon didn't really know all too well. He was just the receptionist.

He would look visitors up and down, make sure their picture matched the picture on the piece of paper on his desk, and then he would stamp the paper "DEAD" with a large and satisfying rubber stamp. The door marked "STYX" would open, they would go through, and Charon would never see them again. Very rarely there was a mix-up, and Charon would have to press a little red button in the center-left of his desk, at which point they vanished rather suddenly and Charon would find himself in a different position with a different piece of paper on his desk, as though five or so seconds had been deleted from the reel of time.

Today, Charon was nervous. Rarely had he encountered the concept of "today", as time simply moved forward without division for him, no past nor future. But he had just escorted a kind fellow named Robert through the door marked Styx, followed by _you_. And you said something about today being the day that Charon would die.

Reader, remember me. Though Terminus, so limited, may speak... they still use my voice. So one final word for our story, before I'm subsumed by ungodly perspective:

Terminus isn't the one who killed you. I am.

\>

I stepped through the door into a white room. Yes, hold your applause, it is indeed the title of this story. That fact, however, is essentially the only interesting thing about the room. It was blank, featureless, matte. There was no way to determine how big it was, or if it had a size. And, somewhat to my disappointment, another door faced me.

Most of this I essentially expected. White voids are fairly standard fare in the human imagination, and it made sense to me that stepping outside reality into a door that heralded the end of all things, I would be greeted by the endless abyss; not an abyss of black, but one of potential— of light. The other door I could have foreseen as well; the white room could be a transition, a liminal nonexistence bringing me to the end.

What I didn't expect was the golden retriever.

A deep existential dread was temporarily stalled by the accosting presence of a cheerful dog. It cordially wagged its tail and politely sniffed at my hand. I cautiously patted the dog's head. It was wearing a red collar with a black tag hanging from a loop. I dropped to one knee and read the tag as the dog leaned forwards and licked the air in front of my face.

"DEATH". Ah.

The dog circled twice, then looked at the new door and softly whispered a bark before looking back at me expectantly.

"I didn't even want to go through the first door, you aren't getting me through this one," I pouted. In truth, I did want to go through this second door, as floating in the white abyss for eternity didn't seem very appealing to me. But I didn't like the idea of being told what to do by a dog. Even if it was Death.

The dog barked again, louder. I attempted to ignore it. It sneezed, shook its body, and trotted back to me, where it gently placed my hand in its mouth and began tugging me towards the door. I attempted to pull away, but my pulling was met in equal force with sharp teeth, so reluctantly I relented and walked towards the door, which swung open and my approach as though expecting me. Stepping through the door, I was assaulted by the universe's last practical joke as I left its threshold. I was accosted by the sensation — no visual hallucination, but a deep feeling — of a burst of confetti and fireworks, followed by a siren whistle and a party buzzer, lastly closed out by a deep and ungodly yet jolly voice saying "You died!"

I was escorted through the door into a hotel lobby by Death, my hand gently gripped in its teeth.

Charon looked at the dog and me, absently concerned. He looked back down at the paper on his desk, but there was no paper. He slowly reached for the button that was similarly absent, and then with a start realized that his desk was gone, and so politely lowered his hand and stared at me.

"Uhhh," he offered.

I shrugged, and pointed at the dog, who had released my hand and was now sitting and watching Charon and I patiently.

"Nice dog," I said, flatly attempting to make conversation.

"It's certainly not _mine_. I think." Charon shrugged at me. "Maybe."

"Charon, right?" I asked.

Charon nodded. "How did you know?"

"Your name is in my head already. I'm not quite sure why. As soon as I entered the lobby, I thought, 'Charon looked at the dog and me'."

"You think in the past tense?"

"Hmm? Why, don't you?"

"I don't think at all," proudly proclaimed Charon. "I've spent the past eternity certain that I don't exist."

"Eternity..." I repeated, whispering the word to myself.

Charon uncomfortably shifted, his arms crossed. "Um, yes. So..."

"So?"

"Who are you?"

"Terminus. The end, I guess. You?"

"Just a receptionist."

We could have stood there for another eternity in the silence that followed, neither of us entirely sure of what to do, until Death (who had been excitedly walking about the lobby, smelling objects of interest) stopped and sat next to the unmarked and locked door at the end of the lobby, scratching at the floor and whining.

"Do you need to go out?" I asked the dog, wincing at my own stupid words even as they came out of my mouth. Death, however, seemed to affirm my question by wiggling rapidly and barking once.

I turned to Charon. "Where does that door lead?"

Charon thought for a moment. "Err, I would need to consult the employee handbook."

He didn't move.

I started walking towards the door, and Charon followed.

"The end, huh?" he asked. "You're the last to die?"

"Well, I didn't _die_ per se. I went through a door that I was very much not supposed to go through, which is kind of like dying."

"Why would you do that, then?"

"It's not a matter of _me_ doing it. Whoever was dying did it. In fact, that's how they died." I stopped and furrowed my brow. "That didn't make much sense, did it?"