title: THE CLAY, THE GOD, AND THE COLORS
author: Snow
tags: 
  - srs!!!

---

Bloodshot eyes watch from the crucible of a melting god. As it slipped into a space or a non-space that was unfamiliar, it tallied the missteps and cruelties it had either wrought or suffered. Either way was the same to it -- if it had suffered any cruelty, it had been wrought by itself. Orange glow sputtered across cold stone walls, and from the shadows cast by the crucible from the reflected light on the walls, the floor dropped out into eternity, a starless sky below a pit of midnight, lacking any depth but so reeking of dark it couldn't have been shallow or a shadow, it was a portal. The bloodshot eyes looked at themselves, and saw only eyes, no god. And the colors that precipitated from the the dark, the clay that pooled in the crucible, was meaningless because the god was absent.

In a trembling right or left hand, a precarious set of scales leaned right, or left. The eyes had long been scratched away from the palms, leaving bloodied folds of cloth draped around the middle of the hands. And at the middle of these hands, where the trembling right or left met, a third hand was pulled from the scales with a quiet folded ring and little finger; it never had any eyes at all. But, even blind, it was not _blinded_. So, right or left knew still the colors of the clay and the thoughts of the god, while the divine justice had no need for a blindfold and no need for forgetfulness. It felt for the scales, and finding them, pulled to balance them. And all throughout this, the blinded hands and divine justice never once saw the mirror -- at once the divinity was removed, the confrontation of the god's biases was facilitated by smooth silver.

A right or left hand pawed at the glass, feeling for an impression of its colors, finding nothing. Divine justice listened to the glass for a sound of the colors' ghost, finding nothing. The blindfolds, soaked red, drank the blood from right or left and only the bitter taste of iron was there to provoke red's sister.

So the bloodshot eyes blinked open, pulled from the crucible to judge themselves. Fixed upon the black pupils in the silver, the eyes saw the monotonous truth, or lack of one. They saw a scattered reflection of cruelty, doubled for every infliction via cold empathy and tripled for any experience via recursive analysis. They saw a machine that ate at cruelty and spat lies in the eyes right or left, a machine that grabbed at the scales of divine justice and greedily filled them with gold and wine. The eyes saw in themselves the laughing god, recklessly feeding on shadows of colors churned from a clay machine.

And the eyes, without regard for the god or for divine justice, without regard for the colors or the clay, without regard for the blind or the blinded, looked to the scales and saw that the bowls were polished mirrors. The eyes flickered shut, and the scales burned away.

The scales burned away, and divine justice closed into a fist, and the blindfold of right or left untied and fell loose, revealing smudged ink, and the clay recognized the story as a fiction.

In the crucible, the colors mingled with the clay until the two were indiscernible.

And the god was left in the abyss below, finally left to dream of colors unbounded by its clay.