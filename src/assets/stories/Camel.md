title: Camel
author: Snow
tags: 
  - mentions-mediterranean

---

The labyrinthian, complex mind of a camel is a scientific marvel. Following the Great Sun Flare Incident of '96 (which inexplicably bestowed all sentient organisms on Earth with plot-convenient telepathy), animal cognitive science flourished. We unlocked the secrets dormant within the shrew's surprisingly profane mind, revealed the poignant Good Boy philosophy upheld by many dogs, and illuminated the empty recesses of where the minds should be in those who believe that hot dogs are not a form of sandwich. But my research into the humble camel has left me academically destitute, shunned by any reliable peer-reviewed journal, and cursed to walk the streets of Cairo -- Kentucky, not Egypt -- looking for answers.

My first camel rally was attended three weeks ago at the behest of my then-girlfriend, who left me a week later citing "camel madness". I don't blame her. Glimpse into the mind of the camel and sometimes the camel glimpses back. We watched the prideful animals stroll around the track, heads held high in an easy trot. The crowd cheered, vendors sold novelty camel-shaped corn-dogs, the air was filled with the fresh and exhilarating scent of camel. And like the lotus eaters of Greek mythology, once I had tasted camel, my ambitions evaporated and were replaced with the hedonistic desire -- no, the _need_ to languish in camel presence. After the race, as my then-girlfriend attempted to peel me away from the beasts and walk back to my then-car, I looked back into her eyes and pleaded with her the commune with a camel, just once. Reluctantly, she yielded.

This would prove to be a fatal mistake.