title: AAAAAAAAAAAA
author: Snow
tags: 
  - srs!!!
  - contains-sword
  - main-character-sighs

---

Coaxed into existence from the dark recesses of a blank white page, Robert, or... whatever his name was -- sprung into existence and reached for the sword he kept by his bed. Why did he keep a sword by his bed? Dragons. Metaphysical dragons, to be precise. Surely there was one lurking somewhere out there. Waiting. Watching. Steven(?) knew this dragon awaited him outside the narrative.

He contemplated stabbing at the words themselves as they formed, but his sword was just too short. So he settled for leaping out of bed and snorting a line of coke he had carefully set up the night before. Or would have set up the night before, if he had existed. Whatever. It was there now, and _now_ now it's not, because he snorted it. Feeling ready to fell a dragon, David sprinted into his shower, vaulting his weirdly long carpet, swinging desperately for the door handle, and skidding into the shower. But someone was already present there.

"Davis! I told you I was taking a shower," said the newcomer who was already present, and his name was something along the lines of Methusafalethes. I'll call him Meth.

Meth stepped out from the shower, holding a dagger behind his back. He had planned to kill Phil on sight, but now couldn't manage. He didn't have the guts. Just as mother always told him. He lowered the poison he was holding in his _other_ hand behind his _other_ back, and in his third hand, he sprung a non-euclidean bear trap. Damn. The murder wasn't going well so far.

Herbert raised his sword upon noticing all these hands behind backs.

"Meth?!" screeched the man whose name I can't decide on.

"Gladly," hilariously misunderstood the man named Meth.

"Ah, no, you see, I was referring to your name. I'm already quite high on cocaine," explained Ronald. He sheathed his sword, letting his guard down for even a second.

"Aha, you let your guard down for even a second!" screeched Meth. He swooped his poison bear dagger forward and caught Philbert on the cheek. He reeled back. Sam did, that is. Meth cackled. An angel of death squeezed itself into the bathroom.

"Uh, says here, someone, uh, kicked the bucket," said the angel. He looked down at Daniel. He was clenching his sword between his teeth.

"You think me capable of being die?" answered Daniel. He stabbed forward at the angel clipping its wings. "Wait, that didn't make sense!" he frantically continued, cutting the angel's wings into ribbons.

"What? Dude, uh, please stop," said the angel.

Ryan, still naked, sprung from the room, also dead, also very high on cocaine. He'd show Meth. And that death angel! He crashed through his window and tumbled four stories onto the street below.

A poor young hot-dog seller, from a long lineage of similar tubed meat pawners, was shilling his tubed meat to passersby.

"Better than cold death," he meekly proclaimed. And then he was buried under Bartholomew, screeching and waving his sword around.

"Think you capable of many dog sale?" managed Walter. He bit his bottom lip. That didn't make any sense either, did it? Damn. He would have to be more careful. Now, where was this metaphysical dragon? Still outside the boundaries of the universe? Good. Stan had it surrounded. The angel floated out from the hole in the window, followed by the would-be assassin Meth. The hot-dog seller struggled to his feet and clenched his hot dogs, spurting the meat juices. No man would insult his dog sale, miniscule though it might be.

The police wisely evacuated the scene.

"By my long family lineage, traceable from Italy -- you know, it's actually quite interesting, I took one of those DNA test things and thought I was Scottish but it turns out I'm mostly Italian -- I curse thee for... Uhh." The hot dog salesman looked around. Where was he? Why was he doing this? This story was stupid. He flashed out of existence.

"What?! But how!" gasped Quincy. "Of course! The dragon consumed him from outside our universe! Probably!" Meth sheathed his poison bear dagger. He had indeed witnessed the young man vanishing from the universe.

"By god, Adam, I thought you were insane, but you might just be right," smiled Meth, smoothly caressing the fourth assassination weapon he kept at his side: a flintlock pistol. But it was too late for him.

The angel looked down at Fred, who had started clawing up the fringes of his angel robe... thing.

"Uh, what are you, uh, doing?" asked the angel. Terrence grabbed at the angel's curled hair as though they were the reins of a pony. Surprisingly, it worked essentially to the same effect. The angel turned in the direction of the pull.

"TO ITALY, MY STEED," shouted Rudy. The angel jetted forward against his will. Oh well. Better than being in heaven. I mean, really, clouds for breakfast, lunch, and dinner. At least he managed to surreptitiously sneak a hot dog while nobody was looking.

They whipped through the sky at mach speeds. See, our friend Bill didn't really know where Italy was, but he knew he wasn't there. So he figured he'd fly forwards until he doubted the idea that he wasn't in Italy, and at that point, he was probably in Italy. So it was mere moments before he touched down in Nebraska.

"Is this Italy?" he asked the angel.

"I don't--"

"GOOD! Be free!"

"What? No, uh, I still need to, uh--"

"GO NOW!" He wiped away tears generated by parting from his steed. He waved his sword vaguely at the angel. "GO OR I WILL CONSUME YOUR HEART!"

The angel fucked off. He had better things to do than deal with this shit. Honestly. If he made it back to heaven, he was quitting.

Meth approached Anthony, panting. It turns out the angel had flown a circle, and they were in Nebraska all along.

"Ah, my less trusty steed," said Jonathan, before mounting Meth.

"What? No--" said Meth, but it was far too late for him.

"Incredible that you ran all the way to Italy. Now, we must discover the location of the Original Hot Dog: Terminus Nullus. It's said that he betrayed his family and spat in the faces of the gods, daring to seek shelter from the hellfire and brine." Peter surveyed the landscape of Nebraska. Surely there would be something here worth doing or seeing. But no. All of Nebraska was worthless. A single farmer approached the pair.

"Hey howdy," said the single farmer. "Saw y'all fly in on an angel. You folk God 'n shit?"

"Ah, no, we're just here to slay a dragon. A metadragon."

"Mmm, yeah," agreed the single farmer, without really agreeing. Damn tourists always here for the metadragon. "You fellas want some corn?"

A high-pitched whining filled the universe to the brim, shaking all of creation to the core and horrendously grinding the gears of the celestial spheres. All existence halted for a brief moment, and then, in a single camera flash, a Token Female Character was created to balance the primarily male cast.

"I Am Female," said the Token Female Character. The others gasped at her presence. Surely she wouldn't be. Consider the possibilities: a woman? Her? She? Existing?

Meth choked and falter in his step, unable to take in air due to the incredibly efficient leg headlock that Luther had him in.

"I Am Female," protested the Token Female Character. The others agreed. Always the voice of reason, and platonically Part of the Crew. That was Token Female Character for you. The single farmer wandered away, chewing on corn.

"Well, there's three of us now. That should be enough for most tropes." Out of breath, the single farmer reappeared in front of them.

"Three of you, you say? Y'all got a well balanced character dynamic n what?"

"Why do you talk with a southern accent? We're in Italy."

"Well, thanks for askin' 'bout my lineage, friendly yet geographically confused stranger. I hail from Texas, but really my family's from Scotland--"

"I see. How does a southern Scott end up in Italy?"

"Via aeroplane, I presume."

Meth fainted, the flintlock pistol gripped in his hand. He hadn't managed to liberate himself. Gerald picked the gun off the ground. Ahhh, a boon from heaven. The angel still had his back, probably. Probably.

Kendrick analyzed his crew. Token Female Character, Meth, and a single farmer. He was ready to overthrow the pope. Yes, he decided, the hot dog was no longer enough. The catholics had had it good far too long. Heh. "Had had". English, right? But then he noticed us probing at his thoughts, and he angrily waved his sword at us until we left.

"I'll SLAY you, dragon! Just wait until I kill this pope, then you're _fucked_."

He started walking through the corn fields to what could only be the vatican. It was big and red and shaped like a barn, and probably was in fact a barn and not the Vatican. But he didn't ask questions.

But, in his endless march of solemn sorrow, Meth passed away.

"God, I feel so... empty without him." Images of Meth playing in his piles of cocaine flashed through his mind. That one time he had killed him. Good times. Better times. The times that he needed.

"Single farmer, do you have a watch?" asked Jerry.

"I Am Female," said Token Female Character. The others erupted into laughter. Witty and relatable, as always.

"Why yes, sir, I do happen to have a watch," replied a single farmer. He pulled a demonic timepiece from his chest pocket. It dripped blood and whispered in old tongues forsaken to all save the most base and vile. "It was m'grandpappys. Grandpappy always did hate humanity."

"I'm about to attempt to borrow time from the time bank. Don't tell them I'm not giving it back." And Finn vanished into the watch.

"Hi, welcome to timebank. How can we help?"

"I need time."

"How much?"

"Like, a lifespan."

"Okay, are you gonna give it back?"

"Yeah, totally. Hah. Like I wouldn't. Ask anybody."

"Anybody isn't available right now. Can I ask you?"

"Yes."

"And?"

"Yes."

"Wonderful. Sign somewhere. Eventually."

He popped back into existence, annihilating a single farmer's misanthropic grandpappy's demon watch. He wheezed with power as the time dust left his lungs. He shoved more time into Meth. Meth stood up and promptly shot himself.

"Hi, welcome to timebank. How can we help, mustachioed stranger who I have never seen?"

"I need time," replied Augustus, in an impeccable corn-based disguise.

"How much?"

"Fuck it," said Augustus, and then he stole all the time in the register.

When he returned, the reanimated Meth had enough lifespan to be essentially immortal.

"Don't be so careless this time buddy," laughed Ezra.

"Why won't you let me die?" hoarsely croaked Meth.

"We have popes to kill. Don't be so down." They marched towards the barn. It had escaped its foundations, and now was rotating in the air. It inflated as they approached it, seemingly stretching against the space that attempted to confine it. Sections of the walls tore away and spun into the nothingness surrounding, sloughing an ethereal red aura. The dome atop the barn crackled with energy. The clouds wrapped around it, gently touching it before spinning away into the void again. The barn opened its eyes.

"HELLO," said the barn.

"Hello, Vatican. Spit out your pope, please."

The barn chuckled, cool and remote, an immense chuckle that filled space.

"FRIEND, IF I HAD A POPE, WHY WOULD I GIVE IT TO YOU?"

"Ugh-- Listen, I need to slay this dragon, right? But to slay it, I need to pop out of existence like this one hot dog salesman I met, and I'm pretty sure he got that power from his Italian heritage, but the pope is really bumming me out."

The barn remained silent for a moment. "YOU'RE JUST FUCKING INSANE, HUH?"

"Oh, absolutely," replied Meth.

"Yup, he's talking to a barn, after all," added a single farmer.

"I Am Female," said the Token Female Character. A sitcom laugh track emanated from the ground around them and filled the air like buzzing locusts before dissipating. Classic Token Female Character. Really breaking down those gender stereotypes by rigidly confining to most of them.

And so the barn acquiesced. In a moment, the flickering fire emitted from its base froze in air like shadows of dark, then at once twisted into each other and twined together in the form of a pope, made from the flames that surrounded it. A gentle fog seeped from the earth and settled gently around the pope.

The pope opened its eyes.

"I heard you intended to kill me, child." The pope stood. It edged closer and closer to Larry, observing him with its cold eyes. "God looks down upon swine like you."

"How dare you call me swine, pope? You are the perpetrator of evils in this world."

"In the pages you have been alive, you have accused a dragon into existence, abolished an angel of its faith, crushed a worker's spirit, and nigh enslaved someone who once called you a friend. You stole time to keep them alive when they struggled away from you. And you dare to call me faithless?" The pope's skin bubbled and cracked, and light poured from the breaks; a sickly orange light dimly seeped from the ashen skin, then frothed forth and spewed onto the ground, lighting the night with pope-juice.

"I seek only you irritate you, apparently rather choleric of temper; for a pope." Devin unmasked himself. The legends were true. He was Xanthus, Popeslayer. His sword glowed and shimmered, screaming against the air with will to once more taste pope-juice.

The pope chuckled, then cackled and reared its head back, howling with laughter that quickly tormented into growls, growls that metamorphosed into guttural screeched as the pope's bones cracked and slid against each other, rearranging in their body. The rib cage split open, exposing a heart that had long been dust. Skeletal wings unfurled from the pope's back.

As the two waged war with the clashing lights of a dying cosmos, Meth attempted to light his crackpipe. But, you see, it was rather damp outside with pope-juice.

"Spare a light?" he asked the barn.

"PARDON? THESE TWO ANCIENT WARRIORS ARE QUITE LOUD," replied the barn.

"Spare a light?" Meth said, a little louder.

"AH, YES," and the gates of heaven opened and rays of unfeeling and unorganized light spilled from the blinded above, lighting Meth's crackpipe.

"Thanks," said Meth, and the severed head of the pope-beast plopped next to him. He regarded it cheerfully, raising his crackpipe to it.

"Nice one, pope pal," he wheezed.

Malcom entered the scene, witnessing Xanthus slaying the pope at last.

"Hey, woah, took a bathroom break. Who's the new guy?"

"Xanthus, Pope Slayer," gestured Meth towards the severed head. "Killed a pope."

"Neat."

Xanthus landed on the ground, sword buried into the ground. He slowly lifted his face and locked eyes with Malcom.

"Your form was adequate," he grated, before vanishing in a shower of sparks and dust that smelled vaguely of italian seasoning.

"Well, adventure's over. Pope got killed."

"THIS IS MERELY THE BEGINNING," rumbled the barn. The earth cracked and shattered around them, sending them tumbling into the stars. They fell without direction, tumbling in the nebulous clouds of color. Token Female Character was vaporized. And they tumbled finally downwards, given direction by the gravity of an object.

This object happened to be Jupiter.

They hit something hard and metal. A floor of an airship. An angel leered over them.

"Back, beasts, you won't be the first angel I've killed today," mumbled Marcus.

"State your name and rank, Wingless," whispered the angel.

"No idea, and quite delicious, thank you." Wilbur stood shakily. Where was Meth? Had he made it, or had the author finally gotten tired of him and written him out of existence? No, Meth was playing cards with the barn. They had become close.

The angel was buried under approximately six feet of a single farmer, who landed straight up, rigid and on his feet, with a stalk of wheat hanging from his mouth.

"Yyyup," answered the single farmer.

"This is no home to dragons, farmer," urged Bernard. "Only more damned angels." He sheathed his sword and gauged his surroundings. Yes, they were around them. If only he weren't blind. "Farmer, what do you see with your functional eyes?"

"Well, I'm not right quite sure, pal. All I've got here is a dear pal o' good ol' southern--"

"I don't even care. Shut up. I hate you so much," said Emmanuel. He slung his likely racism over his shoulder and trudged towards the nearest bathroom, because where there were bathrooms, there was probably water, and Earth was mostly water, so if he took a bath, he was mostly on Earth already.

Alas, angels did not shit. Bruce was trudging straight upwards into the sky. A single farmer looked on semi-interestedly.

"Well, that there's just not right."

"Shut your stupid fucking face. God damn."

Another angel, this one likely higher ranked (as betrayed by their larger wingspan and aggressive mating strategy via flashy plumage), marched towards the pair.

"Oy, Wingless! You can't fly without wings," he called.

"I'm not flying, asshole, I'm trudging. Can't you read?"

"Trudging upward is the same as flying!"

"I'm trudging _bathroom_ward. Don't you folk have one? No? Cya later then, bucko." It was becoming difficult to carry on a conversation with Richard, as he had ascended quite far.

"AHA! There! It said you had ascended!"

"Ascending via trudgery isn't flight. You can trudge up stairs as a way to ascend them, can't you?" called William. The grounded angel scrunched up his face, then, thinking quickly, utilized a single farmer's straw hat to construct a functional toilet. Nathan began trudging towards the newly fashioned technically bathroom.

"NOOOOOOOOO!" he cried. His plans had been foiled. The angel clutched at his heart through his chest, only to be surprised to find that it wasn't being.

"Hold on, now, you're supposed to be dead!" spat the angel, pulling him closer. It observed his skin. Pale, slick with sweat and blood. Yes, probably dead. Not quite there yet, but certainly supposed to be headed that direction.

But the clouds of Jupiter writhed below them. An alarm sounded on the deck, and the angel snapped his stare away from him. It dropped his heart, then rushed to the edge of the observation deck, leaning over the railing.

It then pulled away from the railing, stumbling backwards, missing the top half of its body. What was left of it spurted blood.

"LEVIATHAN!" shouted another angel. "TO ARMS!" It gripped a massive spear and took flight, accompanied by scores of other angel holding similar weapons. A rumbling shriek emitted from the cloud cover below them, and the Leviathan tugged at the fog, revealing the scaled surface of its ugly length. The angels flew towards it, only reaching so far before dissolving into red mist. The horrid screams of the angels mixed with the Leviathan's levity, almost like lilting laughter.

_Hold on now,_ thought Eric. _Does a Leviathan count as a dragon?_ Strewn bits of angelic gore slammed into the ground next to him, but he paid it no mind. He had greater questions to contemplate. For one, Leviathans were known for inhabiting the ocean, a general distinguishment from the typical dragon. But on Jupiter, the oceans were simply more sky. An angel orphan began crying hysterically, clutching their dead parent's body close to their heart. Ahh, but a Leviathan was a sea serpent -- a snake, of sorts. Then again, dragons were classically serpents as well, particularly dragons of Chinese folklore.

"Hey, could you fuckers die quieter? I'm contemplating whether I care about this crisis or not," he shouted. It was mildly effective at calming the tempo of the battle, and the angels attempted to die quietly, as angels are nothing if not creatures of politeness. When they lodged the Greatspear of Athora in an artery of the twisted creature, it bellowed softly like a newborn kitten, as Leviathans, too, tended to be polite.

Yeah, no, Leviathans were completely different. But with the pope dead, he was back to square one in terms of escaping the narrative in order to do battle with the beast outside the bounds of the universe, in the hallowed halls that crossed the hollow walls separating reality from fiction.

Ahh, perhaps if he committed enough ungodly atrocities, God would simply kick him out of existence, and then he could kill the dragon and evaporate from memory _there_. But what was atrocious enough to make God shy from you, and attempt to shut you away from its creation?

He looked back down at the Leviathan.

Maybe he could fuck it.

He reached down towards it. Wait, could Leviathans consent? This was important. He looked up and down the length of its scales. But within the depths of the beast, something rumbled, and the flesh rippled like troubled water. The scales glinted glass before shattering and falling into a void where there once was a creature.

That was plenty. If he could enter the void, he wouldn't have to... _enter_ the Leviathan.

_STUDIO APPLAUSE, WHISTLES_

_AN EXCITABLE MAN NEAR THE BACK SCREAMS "WOOO JOKES"_

From the burbling black of the infinite and ephemeral nothing writhed a glint of color, though, and as Kurtis tumbled through the tumultuous Jovan atmosphere, gripping the hilt of his sword, he caught sight of what had caused the tear in the universe.

The ribbon squirmed from the gap where the Leviathan's middle was, and the beast bellowed from the wound, crashing into the sea of clouds again, falling in the immense way that only planets and moons can fall. Gripping jaws of static gnashed from the rip in space, and with the screech of a dead dial-up modem, thrusted into the air, slamming into Craig and sending him spinning away. He caught sight of a man riding the back of the deadwyrm.

"Much of my supposed personality is based on anecdotes from unreliable sources," screeched fucking Mozart from the back of the wyrm. His eyes glinted and unleashed a wave of irradiating, high energy sex. "In fact, this projected personality is about as accurate as any historical document would suggest, albeit it's more meta than would be expected of the average person. Our best supposition for my personality would be--" and he paused for breath as he stood atop the wyrm's head and gripped a knife in his teeth, comically useless wig flapping in the wind "--merely an average personality for someone else of the time period, or perhaps another artist." The wyrm's skin gristled with slick oil and vaporized into the air, melting the angel's wings as they touched it and sending them screaming, plummeting into the void of the Leviathan.

Now Roland had a dilemma on his hands. He grimaced and wiped it off. Back to contemplating his options. He could either let the angels die horrible, screaming deaths, or he could miss perhaps his only opportunity to escape the narrative and do (probably fucking awesome) battle with the dragon that he was pretty sure laid outside there.

So he did what he knew he had to do. He craned his neck backwards to look at the sky full of falling angels, shouted "CYA LATER DICKWEEDS," and dived headfirst into the frozen nothingness.

Accompanying the dark nothing, instead of nothing, was a soft jazz melody played on a vibraphone. It was tauntingly warm, and was played over a speaker that had certainly seen better days.

"Well, that doesn't seem right," said Arthur to the nothingness. A carpet ravelled together underneath him, and he found his face scrunched against it with a loud knock against the hardwood beneath the luxurious rug. Accompanying the jazz on the speaker now was the crackle of a fireplace, and the warmth washed over him along with the soft orange glow casting light on the inside of a rather cozy room with a faded green armchair occupied by none other than Meth, smoking a pipe and reading a book. He was wearing a suit jacket with patches on the elbows. He noticed Liam lying on the ground, shut the book abruptly, and sighed deeply.

"Why are you here? I was just getting comfortable."

"Why am _I_ here? Why are _you_ here? I jumped into the wound-maw of a Leviathan amidst the boiling corpses of falling angels!"

"Hm, yes," replied Meth, standing and setting the book gently on a table next to the chair with the spine facing Terry. The title was illegible. Meth walked towards the black windows and stared into the void that surrounded the rather cozy room. "In the void is all possibility not within the narrative. You left me on Earth, and so canonically I still exist somewhere down there, but since I remain unwritten, I happily don't exist in the void." Meth set down his pipe next to his book. "And now you come here, to the words unwritten, and you force them onto paper."

"What do you mean by that?"

"I mean that we are written now, and so we are no longer in the void."

The air was cool and sweet in the grass meadow, and the light of the stars above them were plenty to see by. No fireplace, no books, no void. Damien was lost, for a moment, but then understood. You can't kill something that doesn't exist without bringing it into existence first.

"So how will I manage to kill the dragon now?" he asked Meth. He turned to look at his companion, then kept turning, only presently realizing that he was gone. He felt empty in his chest. He'd lost everything. Even his sword had tumbled away from him, in the void.

Then, a metallic clink, followed by silence, then a tap. Clink, silence, tap. Evan attempted to locate the source of the sound, before realizing it was directly beneath him. He looked down.

A woman was lying there, flipping a coin.

"Oh, hey," she said.

"Hello," he replied, unsure.

She flipped the coin again and checked it.

"Hm, tails again."

"What are you flipping for?"

"When I get heads, I'm going to shoot myself."

"Oh."

"I've gotten tails four hundred thousand, eight hundred and ninety eight times in a row now," she replied, scratching the side of her head with the barrel of a pistol. "Give or take."

"What's your name?" asked the one least qualified to ask that question. He sat down on the grass next to her.

"I never really thought about a name. I always assumed I'd get heads sooner. I guess you can call me..." She stopped flipping the coin for a second, and watched the starlight bouncing off of it. "Coin. What's your name?"

"I've got too many to list. Just think of a different random average American male name every time you talk to me."

"What, like, Daniel?"

"No, I think we already used Daniel."

"We?"

"Whatever or whoever is putting this on a page, and whatever or whoever is reading it, processing it, and bringing it to life with their imagination."

"Oh."

They remained silent, then, only letting the cool breeze and the flipping coin distract them. The silver bit of metal twisted in the air, then fell back into her outstretched hand.

"Tails again," she muttered. She turned to look at Cameron. "What brings you out here?"

"Oh, it's stupid, really. I don't think you'd be interested."

She half-smiled and sat up, clutching her knees to her chest. "I think that any story you can tell me that results in you appearing in the middle of an empty field would be at least a little interesting."

He chuckled quietly, at first genuine, but then sober and bitter, recounting the events of the day, or however long it had been since this story had started. So he told her. And she laughed, much to his chagrin, at all the parts that are funny to you and I, but serious to him. But the annoyance was ephemeral and quickly evaporated, because when you have someone to listen to you, you can't complain about their engagement.

"...and now I'm here. Middle of nowhere, and no dead dragon, no sword, no friend."

"Not quite the middle of nowhere, actually," remarked Coin. She pointed into the distance. "There's a major road right there that leads to a fairly large city, and there's a little nook built around that road where I live."

"Oh? What's it called?"

"Meat Town," she grimaced. "Unfortunate name, I know." She stood and smiled, stretching, and she flipped the coin again, looking at it before offering her hand to Drake. "Come on, I can show you around."

He met her hand, and she led him across the field and past a road to a small town outside a large city. It wasn't terribly interesting, as far as small towns went. Seeing as how most of its traffic was focused on going to the large city, it was populated mostly by eight separate gas stations vying for superiority over a single stretch of roadway.

And a pet store.

Doug's Non-Malicious Pets was a shambling mess of a pet store that was sued for false advertising. The animals within seemed to either be waiting for death or actively pursuing it.

"Dragons, sir?" asked the cashier, a teen who wasn't getting paid nearly enough to deal with the pair who had just entered the pet store.

"Yes, do you have any dragons I can slay here?" impatiently repeated Logan. The cashier narrowed their eyes, and their lips grew tight over their face.

"We have... uh, lizards."

"Eh, yeah, sure."

And so Coin and Tate stared at the lizard on the pavement in the alleyway. It was a slow, ugly thing -- but in the cute way of ugly. Its bulging eyes tracked the shadows on the buildings adjacent, and Quin brandished a stick.

"Prepare to meet your death, Santiago!" he yelled at the small reptile. He braced himself and lifted the club with both hands, but faltered. He looked at Coin. "I don't know, this doesn't feel right."

"Awww, come on, it'll help. You might not be able to kill a _meta_dragon, but at least you can kill this small, nonthreatening dragon."

"Yeah, but why did you have to name it Santiago?"

"Realism?" offered Coin. She flipped her namesake. Tails, of course.

"It's a defenseless animal."

Coin pondered for a moment. "I can help with that."

Before long, Santiago was gripping a makeshift toothpick sword cobbled together with hot glue.

"What you've done here is just made him more cute," explained Trevor.

"I know, right? Look at him, he's so adorable," squealed Coin. The pair watched the lizard explore his surroundings with his sword.

"Ah! Pardon me chaps, would you happen to know where the nearest wallet and phone are?" asked a delightfully English mugger with a hat nearly two stories tall. He waved a revolver at them.

"Oh, yes sir, they happen to be in my pockets," answered Coin. "Wait a second-- are you one of those delightful English muggers?"

"Why yes, I am!"

"Delightful English muggers?" asked Frank.

"Yes, they're quite a problem in Meat Town," muttered Coin.

"Hip hip now, lass, hand over the wallet and--"

But like a streak of green lightning, Santiago skittered up the side of the delightful English mugger and jammed his toothpick sword in a neck artery. The delightful English mugger dropped, and Santiago turned glacially to the other two, covered in slick blood.

Coin had a delighted surprised smile. "Good lizard!" she told Santiago. She pointed at him and turned to Michael. "Did you see that? He's such a good boy! Now we _have_ to keep him."

"A little bloodthirsty for a lizard, don't you think?"

"I see no problems with a lizard with healthy bloodlust."

They turned back to Santiago, who was cleaning the blood off of himself.

"You're right."

Coin flipped the coin. She didn't have to look to see the result. She turned to Mike and half-smiled. "Hey, it was fun," she said.

Ralph returned the smile, weakly. "That's it, huh?"

She nodded, gave a little mock salute, then shot herself in the face.

Blood exploded from the back of her head, then froze in the air, any state but liquid. It suspended itself like powdered paint on the air, fixed to the universe by pins, blossoming from the back of her now empty mind. It gleamed like dark stained glass, like ruby, tumultuous waves of writhing crimson against the bright light cast from an unknown source. Light brimmed in the alley, bouncing from the walls of adjacent building and the white concrete below, catching the suspended blood in effervescent diffused light. She tumbled upwards, body separated from blood, blood that reached forward as though seeking. Where it touched, it left red shadow, painted splatters against concrete.

The surfaces contacted by red dropped away, shadows sinking into the solids and making them abyss. The smooth grinding of stone eased her body into the ground, and the world swallowed her, offering her the dignity of a smooth exit with no traces.

Left behind was a splatter of void. Her absence ate away at presence, until the alley was gone, the concrete was gone, the blood and shadow was replaced by the same dim white light eagerly gobbling all features and reflecting from once-red to black. The light illuminated the infinite depths where the fiction bent to leave her, loosely wrapped in words, sleeping in the heart of the nothing.

And, into the gaping void was violently hurled a green lightning bolt.

"No, Santiago!--"

But it was too late. The lizard, with vengeance in his eyes and toothpick gripped between his teeth, followed Coin into the nothing.

Jack reached out in an attempt to pull Santiago from the narrative maw. This was a mistake, in the sense that it had unintended consequence, but truthfully no possible consequence was intended when he reached for the lizard. Overbalancing, he spilled into the void, pulled by one gravity into another. He hinged across the ledge, and with a ripple across the water, neatly entered a plane perpendicular but coexistent.

Immersed in the cool nothing, a spotlight fell on our infinitely named main character, the world behind now tilted on its side below a floor of glass. He was illuminated against the endless plane of dark.

A limping figure, with human proportions but improper existence, greeted his spotlight. Its body was covered with a stretched green fabric, the same green shade regardless of light and regardless of folds or shadow. It was yoked with a pale wooden staff.

It spoke in words that resolved only seconds after touching the mind of the infinite. In garbled tones, backwards and laced with static, patched together via concatenated random chance chosen only for its usefulness, it said:

"We walk. Time is now our enemy."

It took Him moments to realize what the green figure was an approximation of. Santiago, too, had been butchered by the void. He followed. He didn't have any choice, because the strings of my words make him dance. They plunged into the dark.

"So it begins," quietly said Santiago, bits of his speech looping back into itself. In the dark outside the spotlight, something glinted dark red light, and flickered alive with a weak glow. It was a clock. A spotlight came on with a _clunk_ and illuminated the body of the clock. It was a huge, many-legged beast. Each leg terminated in a clock hand, some tiny, others the size of tree trunks. Ragged fabric drooped at the crook of each leg, strips of torn blackness that dragged on the ground.

For a moment, it almost seemed confused. Santiago and Main Character held their breath, somewhat by instinct. It scuttled forward by a couple feet, the noise of clock-hands tapping against the ground like deafening machinery, accompanied by the sounds of pistons firing, steam escaping. The clockbeast tilted its face slightly, probing at the dark. As it took a couple tentative steps forward, Santiago readied his toothpick.

Then it accelerated into a trot, then a gallop, the screeching machinery accompanying an immense voice created from the same twisted patchwork of dial-up tones and dead sitcoms as Santiago's. "Yᴏᴜʀ ᴛɪᴍᴇ ʟᴏᴀɴ ʜᴀs ʙᴇᴇɴ ʀᴇᴠᴏᴋᴇᴅ. Yᴏᴜʀ ᴇxᴘᴇᴄᴛᴇᴅ ɪɴᴛᴇʀᴇsᴛ ᴀᴍᴏᴜɴᴛs ᴛᴏ--" and then it punctuated that statement with a visceral screech like metal scraping against metal.

"Your sword," said Santiago.

"Lost to the void," He replied.

"No, look." Santiago outstretched a green hand towards the clockbeast. Following his gesture, He saw the sword, buried hilt-deep into the Timebank Debt Collector. From the wound festered purple tendrils, both grasping at and shying away from the sword.

"I see." He looked back to Santiago, who was once again a lizard with a toothpick glued to its claws. He processed this for a second. "Well, shi--"

The clockbeast slammed into Him, unhinged its face into 12 sections and revealed the twisted continuum of time beneath, like a writhing purple snake. It had Him pinned beneath its hand-legs. The hands of the face clock clockface whirred and spat energy.

"Tʜᴇ Nᴀᴍᴇʟᴇss sʜᴀʟʟ ʙᴇ ʙʀᴀɴᴅᴇᴅ ꜰᴏʀ ɴᴀʀʀᴀᴛɪᴠᴇ sᴇᴄᴜʀɪᴛʏ," continued the clockbeast. Seared into His skull by the passage of time was a pulsating "001". "Tʜᴇ ᴀɴᴛɪᴠᴏɪᴅ ʜᴀs ɴᴇɢʟᴇᴄᴛᴇᴅ ᴛᴏ ɴᴀᴍᴇ ʏᴏᴜ, ᴀɴᴅ ʏᴏᴜʀ ᴄʀɪᴍᴇs ʜᴀᴠᴇ ɴᴏ ᴀᴛᴛᴀᴄʜᴍᴇɴᴛ ᴛᴏ ʀᴇᴀʟɪᴛʏ ᴏʀ ɴᴏɴʀᴇᴀʟɪᴛʏ. Sʟɪɢʜᴛɪɴɢ ᴛʜᴇ ᴛɪᴍᴇʙᴀɴᴋ ᴡᴀs ᴛʜᴇ ʟᴀsᴛ ᴍɪsᴛᴀᴋᴇ ᴏꜰ ᴛʜᴇ ᴄᴏɴᴛɪɴᴜᴏᴜs ᴄᴏʀᴘᴏʀᴇᴀʟ ɴᴏɴᴀᴛᴛᴀᴄʜᴇᴅ. Yᴏᴜʀ ɴᴀᴍᴇ ɪs 001." There was a moment as the text shifted to accommodate his name. 001. Before time could wither him away, however, a small lizard crawled into the open maw of time, looking back with an expression only describable as understanding. The lizard was shredded across the continuum, and the clockbeast was disoriented by the pull in every direction. 001 took the opportunity to unpin himself, grasp desperately at the clockface, find a handhold, and vault atop the grotesque machinery. He pulled at the sword buried in the beast, and the purple tentacles unfolded, letting the steel slide cooly from the wound.

Then, with a fell swoop, the sword slashed through the clockbeast's neck, sealing the opening to the continuum, then continued, severing his tie to the void, and he flipped upwards back into real space, exhausted, spitting the dry nothing from his lungs. He was named now.

"No, fuck that," gasped 001, slowly standing, clasping his hands together, flexing, and through sheer force of will, vaporizing his name into a fine red mist that floated gently away from him. Take that, narrative cohesion.

He rattled his bones and unjangled his limbs, which had, until this point, been almost entirely jangle. 001. What a stupid fucking name, thought James, settling on James because everyone was making such a big deal about it.

Briefly forgetting how to walk, James tumble flopped forward in an undefined setting, since I haven't defined it yet. Imagine. Just imagine. James, limp like a noodle, back composed of overcooked spaghetti, loosely limping his noodly limbs, lumbering in a blank white page accompanied by an ungodly silence.

_Yeah_, thought James, the only noise to interrupt his thoughts formed by the slapping and meaty crunching of his bones and meat contorting in a never-ending hell summersault.

_Yeah._

Gaze now at James in profile view, crawling and rotating forward at ungodly speeds through a white abyss, the camera tracking his movement. The camera cuts views to a different section of white abyss; James conspicuously absent. In the left audio channel, the fruits of weeks of a foley artist's labor gradually raises in volume, eventually entering the right audio channel as well in a cacophonous orchestra of slaps and clicks and crunches.

Entering the left side of the frame, James tumbles through the screen at an ungodly speed, body stretching in a rough loop, a subtle grace to a speed unnerving. In fractions of a second, James reaches the middle of the frame, and a camera shutter noise is added in post. A CGI reconstruction (good enough to fool the eye for the moments it is shown) is suspended on the screen for some moments in a moving still frame, capturing the form of James in motion. Another camera shutter noise, and we see James from another angle, slowly zooming in on his determined face, left foot's sole jammed into his right cheek. A last camera shutter noise, a long shot panning out into the white abyss, revealing an immense dust cloud kicked up in James' wake.

_Yeah._

Once more, the camera changes angle, recentering on James in a frozen pose, before releasing him from his time prison. He is onscreen for another couple of frames, and the masterwork of sound design fades away once more into the right ear.

Cinematic masterpiece.

A foreign word barged into James' mind uninvited, and this word was "bowling". The uninvited word brought uninvited tears into his eyes. Oh God. Oh God, no.

Slats of fake foam wood bubbled under his feet, replacing white abyss and clouds of dust. A scent of dead churro and sweat brought by heat and not exertion drifted gently down from above. Shoes galloped from all sides and tossed themselves gleefully into glass displays. Balls. Heavy balls. Heavy balls, with holes, between rails. A convoluted mechanical mechanism that nobody is quite confident in, dedicated to the retrieval of heavy balls with holes made of a material that nobody is quite sure of.

"Mmmhm, don't quite think the author of this here section is certain on the logistics of bowling alleys," quoth the bartender behind the bowling bar, wearing a classy vest and bow tie and polishing a bowling ball with a classy rag shaped like a bow tie. James sighed and shook his head, sitting on the stool at the bowling counter across the bowltender.

"Heavy ball, please," mumbled James. "On the racks." The balltender nodded. He reached up to a high shelf of a mirrored cabinet and retrieved a small commemorative trophy, then stepped out from behind the counter and handed it to James, revealing that his lower half was a pig's body. Like a centaur, but confused.

"You're going to be really disappointed when you read the joke that my horrific physical malformation is the setup for," said the boartender, and then he winced, bit his lower lip with his eyes squeezed shut, and continued in a low, strained whisper. "Yep, there it is."

"Ah, don't whine," said another occupant of the bowl bar. She was sitting next to James this whole time, but was unwritten because she didn't seem relevant. Then she had to go and say something. Ugh.

"I see you are wearing a suit of armor," observed James, for nobody's benefit but the reader.

"Cut that shit out," she responded. "The reader doesn't have to know that."

James sheepishly hung his head and nibbled on his small commemorative trophy.

The armored woman flipped the visor on her helmet, revealing a tiny face — well, okay, so the head is normal-sized, right? but all the features are tiny — and she struggled a bowling ball towards her face with one hand, wavering and shaking as she guided it to her itty bitty face, veering off course as she struggled to maintain control, committing aerial maneuver atrocities and getting kicked out of flight school before heroically sacrificing a finger to finally bring the ball to her little tiny mouth, whereupon she sipped a liquid from the bowl holes in the ball. She finished off the cocktail and dismissively tossed the ball behind her. It rolled down the alley, and a chorus of gasps filtered through unused PA speakers.

Strike! A vast army of sports reporters apparated and viciously tore at one another to be the first to record it, before vanishing in a curl of smoke.

"Name's Madam Lancelot. Or Lady Lancelot. What's the... the female equivalent of Sir?" she distractedly asked the aether. The aether shrugged, and so she pulled out her phone and typed a query into a search engine. "Uhhh," she started, despite not having the answer. She faded into silence.

"She's been after my goddamn CPU all day," added the bratender, distorting a joke originally intended to subvert a boring setup but becoming such a common punchline that it became stale itself. He pointed at a cup on the shelf. It was a Big Slurp cup from the anti-gas station in Delaware that steals gas from you and sells cursed artifacts instead of snacks. There was a pentagram splayed on the side, and letters not quite _on_ the cup, but spatially or temporally adjacent to the cup, that proclaimed "GODDAMN".

"The Unholy Grail!" interjected Ladylot in a harsh stage whisper, her eyes momentarily flickering to black.

"Leave my goddamn cup alone! You stole my block of rare solid swiss from my cheese larder just yesterday, and made a goddamn sandwich with it!" shouted the brietender.

"The Un-holey Grilled!" she responded in an identical whisper.

"The day before that, she stole my phone and started texting one of my best friends online! I was consoling her through an existential crisis!" proclaimed the brotender, agonizingly wracked with pain every time his title was changed to reflect a facet of something he just said.

"The Unwhole E-Grill!" frothed Lancelot. She flipped the visor back down, concealing her miniscule visage, and carefully began clambering onto the counter. She placed a single gauntleted hand atop the counter and attempting to hook her foot on the edge of the bar. Oop— missed the mark on that one — there we go, that's one foot — she's begun hopping on the other foot, that's good form. Inspector General Morrison held a magnifying glass up to the planted, hopping foot, rapidly taking measurements, and offered a thumbs up. The microhops were clear to increase in magnitude. Another hop, there we are, she's grabbed the edge of the counter with her other hand, that's the way, good stuff, she's managed to catch hold of the far edge — now that ought to be a turning point — seems she's slid her chest over the bar, good placement of center of mass, still hopping, uh oh, lost a point of contact there; seems the hopping foot is no longer in hopping range, merely hanging suspended — there we are, she's withdrawn the former hopping foot, she's home free — struggling a bit on the knee tuck here, she's slipping, not gaining ground here, seems she's opted for a new strategy; she's levering he mass using arms and leg and managed, just barely, to tuck the former hopping joint onto the counter, and there we are, home free at last. She panted for a second as the exertion paid off.

Then, the shambling ones came.

"Cup! Cup! Cup!" chanted the masses of writhing flesh and dark, a lattice of muscle leaking from the universe and twining with the fingers of absence beyond it, pulling nonreality into reality with a heaving sob.

Lancelot's armor shuddered once and leaked pitch, a pool of darkness that emanated from joints and cracks in steel. In a voice similar to Lancelot's but not quite the same, what was once Lancelot began chanting too. Well, ok, so, she was already chanting "Cup, cup," etc., and had been this entire time, but this time her voice was different.

A weak "thank you" was released from a growing pile of flickering viscera as the poortender was consumed by a mindless yet singleminded void. James sighed and downed the last of his small commemorative trophy. So, this was it. Void finally caught up with him.

The tidal wave of abyss stretched space around it, impossibly massive and infinite in a finite space. It reached for the Big Slurp cup, but paused hesitantly.

"Jaaaames?" asked the void.

"Uh, hi," awkwardly stammered James.

"James!" exclaimed the void in unison.

"I don't like where this is going," warned James. His dubious expression melted in frustration as the tangled void corpse began chanting his name.

"Fuuuck. With the chanting," muttered James, already running away from the void corpse that roared behind him with noiseless static. He reached for his belt to unsheathe his sword, but his sword had fallen into a plot hole opened by him not having his sword when I explicitly wrote that he exited the adjacent dimension with his sword mere pages before this. So now he doesn't have his sword, and it's for canonically consistent reasons, and the plot hole has been closed.

Consistency.

James tripped on a twig, or something. He looked back into the maw of doom with a shrug. "Had a good run," he admitted. "Little slow in some parts, but we can clean that up, I think. I'm still looking for a sub-18 page. I don't know if it's theoretically possible? Cuz the community took the best of a lot of runner's splits—"

A bit of reality unwound itself from the void corpse, unzipping with an accelerated zipper that split into branches across the non-surface, peeling back the skin of the void corpse and unfolding into book pages that ignited and froze in time as their space was occupied by more unzipped pages. And from the undulating pages of an unzipped book on fire walked Meth, holding a sword and crackpipe.

"Ugggh," moaned Meth. "They're out of the flavor I like."

"My sword!" ecstatically cried James.

"Mmm, no. My sword now," said Meth. He strolled behind the counter, carefully stepping out of the puddle of non-puddle left by a flaming carcass of an adjacent universe. He swiped the cup — you know, the Unholy one? — from the shelf, holding the crackpipe in his mouth as he jammed it into the drink machine. A constant red slurry spewed into the cup.

"What? You can't take my sword!" cried James, less ecstatically.

"Right. I couldn't, if it were your sword. Because when I take your sword, it's my sword." Before allowing time for a response, he descended a staircase through the floor, noclipping through faux wood and sliding in an opposite direction from his movement.

"Hey! Stop escaping!" decried James.

"I'm not escaping, idiot, I'm sliding in an opposite direction from my movement. Can't you read? Thanks for all the time in the time register, loser. I'm going to the moon."

And so, staircase moonwalking to a moon walk left Meth, sipping Red because they were out of Blue.