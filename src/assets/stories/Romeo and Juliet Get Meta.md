title: Romeo and Juliet Get Meta
author: Snow
tags: 
  -

---

[*Romeo and Juliet meet for the last time before enacting their ill-advised plan of pretended suicide followed by actual suicide.*]

Romeo: Juliet, oh beautiful Juliet, why do your eyes glimmer so with, uhmmm… Wetness. Eye sweat. What are they called again? Hold on--

Juliet: You are as eloquent and charming as always, my darling.

Romeo: I can sense the sarcasm dripping off your voice, my sweet.

Juliet: I can sense the dumbass dripping off your putrid breath, Romeo.

[*Romeo pauses briefly.*]

Romeo: Weren’t we supposed to be archetypal lovers? Of the… forbidden variety?

Juliet: Weren’t you supposed to be a man? Oh, hold on, you’re right, actually. Flip through the script again?

[*Romeo shuffles through some pages with a confused expression.*]

Romeo: It’s no good, it’s too meta.

Juliet: Oh, excellent, yet another genius has decided to bastardize a completed work for the intention of parody.

Romeo: It mightn’t be that bad, my love, beyond the language that tries too hard and the jokes that nobody will laugh at.

Juliet: Oh, Romeo, listen to yourself. This is a self-derogatory piece of trash. Whoever wrote this is both meta and lazy.

Romeo: Or sadistic.

Juliet: Or sadistic.

Romeo: So what are we to do? Some final act of rebellion? Shall we burn this place and end this tale as it should have ended?

Juliet: What rebellion is there in following the track of fate? Whatever we do has been written. I have the script in my hands.

Romeo: Then maybe we’re destined to rebel!

[*Juliet flips through the script.*]

Juliet: It seems like we’re destined to carry on this dialogue for a few more lines, and then the stage sets on fire.

Romeo: Deus, meet machina… 

Juliet: Well, we may be able to avoid--

[*An act of god sets the stage ablaze.*]

Romeo: Oh, come on. This cynicism is unwarranted at best.

[*Juliet screams as her flesh is charred into what Gordon Ramsey would call an atrocity.*]

Juliet: Apparently the author is attempting to convey drama by making me scream in the stage directions.

Romeo: What? We’re fictional characters, we can’t even feel pain. We can’t feel anything, in fact. We’re basically nothing but words on a page.

Juliet: Ah, but see, fair Romeo, we’re words on a page being spoken out loud to a small gathering of people. Doesn’t this voice bear some semblance of life?

Romeo: I might be inclined to agree with you my dearest Juliet, except I too have caught fire, and I now refuse to believe in anything that could be construed as positive.

Juliet: Isn’t that just bull-headed cynicism? Stubbornness isn’t a philosophy.

Romeo: Neither is stealing two characters and then setting them on fire, but look at J go.

Juliet: Hmm. Now he’s self-inserting into the script. That name drop was unnecessary.

Romeo: Huh, I suppose you’re right. Well, if anything, I welcome my death with open arms.

Juliet: So much for rebellion.

[*They die. After around two minutes of silence, both actors nervously glance at the audience, hurry off stage, and the play finishes to very little fanfare.*]