title: plotfloor dogchild arbiter king void ribbons
author: Snow
tags: 
  - awful-title
  - cowardly-police

---

"Nobody move, there's plot on the floor!" screeched a dog from the back of the pet store. This dog had the semi-unique ability of speaking English while also being a dog. The floor he was talking about was coated in a thick black substance that oozed from a theater mask and smelled like narrative devices explicitly designed to propel a story forwards.

The owner of the pet shop sprinted towards the dog, hand outstretched to silence the pooch, and slipped on the plot. He spiralled into a wall, and his flesh began bubbling and melting into itself. The plane of existence his skin inhabited stretched and needled outwards without stretching the skin itself, leaving him alive but screaming.

"Goddamnit, don't touch the plot!" said the dog, again. But as he said it, he realized something. He was a talking dog. Surely he was some manner of plot device. After all…

What kind of monster wouldn't have a talking dog in their story?

The dog grinned, its teeth extending beyond its jaw before turning back. Spirals of bone burst from its paw, and it levered into the air, escaping the cage. The bone spires sprawled about in the pet shop, and the dog pierced the skin of the owner, putting him out of his misery.

"This was always going to be the end, my love," whispered the dog, withdrawing bone from flesh. The police entered the pet store, then exited. They really didn't want to deal with this either.

A small child entered the scene as though written in suddenly by a rushed author. It looked around. Surely this place was not food. But still, the child began to consume, as the dog turned over the shelves and waved the pet store owner's corpse around like a flag on a bone pole. The small child spotted the dog after eating the other half of store, leaving a gaping white void in reality that ceased to be written.

The child rushed the dog, mouth still full of the shreds of existence. The dog, alarmed by the voracious appetite of the child, clicked horrifically and mutated further, its legs sprawling from inside. The new appendages extended from the mouth and rushed forward to greet the child, who was now attempting to gobble up the flank of the dog. Before the dog's rear could be converted to baby food, however, the plot juice on the floor rumbled, hungrily. There was not nearly enough plot to satisfy the Theater Mask.

The ground bubbled upwards and a king burst into flames atop a mountain of dog food and discount paper shredders. He addressed the child and dog directly with a disappointed stare, then melted back into nonexistence.

"Behold, the Arbiter," whispered the dog, now half consumed by the ever-ravenous child. "He brings our prophecy, and so our fate must unwind."

The pet shop detonated into ribbons of color, extending into an eternity of blackness. A soft color effused from the strings of existence, and the dog and child were hurled into the void. This was a proper battlefield.

Both spontaneously combusted as I ran out of time to write them, leaving behind a ravaged Earth, thirsty for revenge and the sweet sweet taste of Coca-ColaTM.

Do you think this clock cares about your feelings? I sure don't. Look at this satanic clock. Red with the blood of its enemies. This clock has killed 13 people. It's just so horribly out of time that the hands move in a blur, decapitating passersby, thirstily grabbing for blood from its horrid little clock recess on its clock wall. This clock is here to kill.

Long ago, in Argentina, this clock was the driving force behind a Nazi military coup, and now South America, to this day, is still suffering under a horribly secret anti-semitic regime, which has concealed itself like a bullfrog waiting to strike out at the baseball game, because bullfrogs aren't very good at baseball. They keep trying to swallow the ball. Don't swallow the ball, bull. Bulls blow. Anyways, the clock. It looked like your average pinpoint accurate clock, except it was neither average nor pinpoint accurate (or perhaps it was, depending on the size of the offending pin). Along its edge was a constant string of razor blades, rusted red with the blood of those it has cruelly disemboweled. Its hands were handsier than most clock, handsing anyone who got too close. That clock has bloodlust in its heart.

Then a fox with a pistol shot it.

You think that fox cares about your feelings? It has a pistol in its mouth. That fox has killed 26 people, twice as many as the deceased clock.