title: Conversational
author: Snow
tags: 
  - mentions-mediterranean

---

Their arms hovered there, suspended in that fraction of infinity like gleaming gold stars adrift in the void. The noise of the party seemed to dim as the stage was set for the pair, and slowly the world around then crawled to a stop.

Marybelle. Age 27. Blond, light makeup, average height, average build. She held a red solo cup in her left hand, tucked close to her body and blending nearly perfectly with her red dress. She reached for the handle of a ladle in a punchbowl.

Persephone. Age 26. Brunette, minimal makeup, tall. Not quite beautiful, but certainly good-looking. She wore a comparable red dress, perhaps from the same store. With her left hand, she reached to the stack of cups adjacent to the bowl. With her right, she reached for the ladle.

All this information, absorbed by both parties in an instant. Their minds calculating every exact second. Every slice of time, every instant processed in equal measure, and the painful, silent second ticked by as minds revolved in their calculations.

"Oh, my goodness, Persephone? I hardly recognized you! Please, after you," began Marybelle. She had gained the advantage of the first strike, and she did not yield the opportunity. Her voice was laden with soothing friendliness.

"Oh, Marybelle! It's so wonderful to see you here!" She shifted posture and turned to the stack of cups. "I'm just grabbing a cup right now, so go on ahead." She watched Marybelle carefully, and took a cup off the top of the stack. But Marybelle wouldn't so easily be defeated.

"Oh, I still have a bit left," she lied. Marybelle contemplatively swirled her cup in her left hand, then raised it and pantomimed drinking. Behind the cup, she smirked. It was close, but she would emerge victorious. She lowered the cup, and saw Persephone, still smiling, take a step and reach for the ladle. Closer… Closer--

"Oh! I'm such a klutz!" sang Persephone. The cup that she retrieved had slipped from her hands. But Marybelle wouldn't be caught off-guard. She had seen Persephone attempt the same tactic at prom. Back when Marybelle still admired her. Back when they were the best of friends. The memories of Persephone flooded Marybelle's mind as the cup tumbled to the floor. Always in the spotlight. Always so easily navigating social situations.

The memories were bittersweet, sweet to create and bitter to remember. Marybelle knew what she had to do.

"Oh, don't worry about it," she giggled, and she leaned over to pick up the cup for Persephone, who looked on with interest. It was an unexpected move, certainly. But the gamble had paid off, as it was too late to respond. Marybelle had learned, then. Persephone needed to stop playing around.

"Oh, you don't have to! You're still so sweet, Belle. Here, let me get some punch for you." She finished speaking before Marybelle even had the chance to straighten her posture. Time slowed again, ticking to a stop. Marybelle cursed herself for her ignorance. She had forgotten Sun Tzu's famous words: "the supreme art of war is to subdue your enemy without fighting". She analyzed the situation. There was no way out… but maybe she didn't need one. Keeping Sun Tzu's words in the back of her mind, Marybelle feigned defeat and held out a cup.

"Oh, thank you, Persephone! You're too kind." She handed off the cup to Persephone and prayed she wouldn't notice. Persephone took the cup and turned to the punchbowl. Marybelle smirked as Persephone filled the cup. Once the cup was full, Persephone turned and offered it to her.

"Oh, I'm so sorry! I just realized, that wasn't my cup! That was yours! I don't want to be selfish," said Marybelle. The spider laid her web, and the fly was caught, struggling. No matter their past, all who sought war with her would find only defeat.

But then, Persephone did something unexpected. Something that she had told Marybelle about only once, years ago. A new strategy she had developed, almost unbeatable. There was a single chink in the armor. The words "I don't want to be selfish." They weren't matched up, they didn't slot neatly into the flow of the mood.

Persephone raised an eyebrow and tilted her head. The Jewel of the West Gambit. Feign confusion at an incongruous statement with only an eyebrow raise and a head-tilt. Innocuous enough to pass as simply mishearing, aggressive enough to undo your enemies defenses. An eyebrow raise and head-tilt could signal offense, anger, worry, confusion, even discomfort. There was no possible escape, as every emotional front was covered, and _any_ interpretation was a misinterpretation. Legend says the great Carthaginian general Hannibal overtook entire armies with the Jewel of the West Gambit.

Marybelle looked down and giggled.

"Just kidding, of course. Thank you so much, Persephone." She gingerly grabbed the cup full of punch, her smile only now truly empty. Persephone smirked and filled her own cup, then walked away from the punchbowl. As she passed Marybelle, she stopped and whispered in her ear.

"Better luck next time, Belle."

And she walked away, leaving Belle speechless, staring at the ground with a fake smile and a cup of punch.