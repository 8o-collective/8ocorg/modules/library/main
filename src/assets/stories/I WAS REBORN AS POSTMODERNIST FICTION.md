title: I WAS REBORN AS POSTMODERNIST FICTION???
author: Snow
tags: 
  - implies-afterlife
  - awful-title

---

The concept of isekai is hit by a truck and reborn as the genre of postmodernist fiction.

My name is Isekai. I live in the conceptual void as just your average genre. There's nothing special about me. I'm a little bit of fantasy, and a whole lot of wish fulfillment. I have no hair and none-colored eyes. I am average height, which is to say I don't have a height, because I am a genre of fiction.

Every day, I go to Genre High School, a framing device which allows the author to construct a backstory for a literal intangible concept. I'm not exactly popular. Not to say that I get bullied or anything by the brooding urban fantasy or the suave disturbingly-marketed-questionable-consent teen romance, but no female-coded intangible concepts even give me a second glance, a fact that fuels an undertone of emasculation throughout this work that points to larger problems with heteronormative society as a whole.

I feel like my life is on the same track as every other genre: get established in the zeitgeist after a few successful pack leaders blaze a trail before fading into market saturation and ultimately getting parodied in some dumbass postmodern fiction by an author who doesn't fully understand or have an intimate relationship with my genre in the first place.

Not to say there's anything wrong with that, but I've always felt like I wanted to achieve more with my life. I guess some people are born to be boring like me, and others are born to understand dramatic irony.

I can't help but wonder...

Will people even notice when I'm gone?

My alarm beeps and I pull my covers over my head, groaning. Time o'clock already??? That's the time when I have to wake up and advance the plot! I begrudgingly exit my deliciously warm abode and stumble to the door of my messy bedroom with various relatable posters of relatable nerd culture (unidentifiable due to copyright) plastered haphazardly in the walls, thus signifying that I am a relatable person to you in particular. I shower and brush my teeth, and I go downstairs, where my mom is waiting with breakfast. My mom's face is always conspicuously out of frame because she's too much of a non-character for the animation budget to be wasted on her design, even if this work is non-animated. My dad doesn't exist, apparently because keeping track of more than one parental figure is beyond the pay grade of this author. Oops, my mom vanished too. OK, I'm an orphan, then. At least I got breakfast out of it.

I exit my home with toast expertly hanging from my mouth by the corner, and despite the fact that I woke up to an alarm that I set every day, I was somehow late! Oh no! I being sprinting with toast in my mouth, flopping comically about. Oh no! The toast flew out of my mouth and blinded me as I was sprinting! Oh no! I'm headed straight for the freeway crossing in a rural area! I can't see! Oh no! The birds are squawking and tweeting loudly and I can't hear anything! My legs are machines, they cannot be stopped! I have too much momentum to stop myself from running into the freeway, and my arms are too preoccupied with flopping around uselessly to remove the toast from my face! Oh no!

The author's inkwell topples over and they stare deeply at the page with unimaginable existential pain, paralyzed by a crippling hatred for both themselves and what they are writing. Without looking, they absently reach for the fifth of Grey Goose they keep on their desk, ignoring the growing puddle of black ink encroaching on their work. Their fingers wrap around the neck of the bottle and they lift it, only to find it empty.

The author steps outside, flinching at the caustic sunlight even as their dark sunglasses block out most of it. Their head is pounding, and their mouth is dry. They step into their ancient, rusted truck and stare at the ceiling for a few moments before turning the keys in the ignition. The truck coughs and revs, engine turning without starting. The author swears at the steering wheel, gripping it tightly with a shaking left hand, and turns the keys again.

This time the truck roars to life. The author relaxes, letting out a shaky breath, and pulls out of the driveway.

It was a long trip to the nearest liquor store. The author lived in a secluded rural area, hidden away from the smiling masses who reminded them of their failures. The nearest town with a liquor store was forty miles out.

Pine trees raced by in the windows, and the author watched the shifting landscape, mind wandering through their writing. Their foot lowered on the gas pedal steadily.

The insomnia, the drinking, the endless white pages that needed to be filled with ink. Their mind darkened and eyes closed.

The collision woke the author.

They slammed the brakes far too late, and with a sickening scrape something fell from the hood of their car.

"No..."

"No, no, no..."

They clutch their head as they stare at the body on the ground. It's a genre, just a high schooler. Probably just on their way to school.

The author dials 911, then hesitates. A black fog curls around their vision, their fingers almost too jittery to press the proper number. They look back at the kid, then the bright blue sky encompassing all. They can't do this.

They look through the kid's pockets, fighting an urge to vomit, before finding their phone. They dial 911, drop the phone, step back into the truck, and drive home.

Isekai got hit by a truck! W-...

Wacky! Haha... hah.