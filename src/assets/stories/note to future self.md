title: note to future self
author: "[REDACTED FOR SAFETY]"
tags: 
  - incomprehensible

---

Slice self in half somehow? Trying to resurrect Freud. Anti-lumberjacking. Tape two dogs together and create a duplex woof. Create a dictionary that lists all the words, but not their definitions. Break your phone. Just do it. Slam it on the ground. Throw your phone on the ground, c’mon, do it. Make a table out of sharpened pennies (like disk razors?). Don’t put shoes on. Put both shoes on, but in the wrong places. **Prosthetic feet for quadruple amputees on every limb.** (possibly so bad it’s good???) send cheese sticks to governors via mail. Put two coats on. Have a clock with spaghetti for hands. It runs on sauce? Sauce-fuel, the 90% gasoline tomato sauce. Fake diamond business called “Really diamonds” with legal fine print included in the title. Wings with no boundaries, sorta explodey wings. Wings that just go. Thicc wings. Glasses that make you see everything but if it were just a little tastier. A painting of two guys going at it in every sense. Snakes bioengineered to be charger cables. Double the planks. A plank syndicate. Deplank all plank sources, become a plankopoly. E-mail, but just as slow as normal mail. Call it nor-mail. Yeah. That’s good. Shave with bread. A fish with no gills. Pasta but instead it’s just tentacles. Squid ink ice cream with extra squid ink. Squid ink enema. Spiral notebooks, but the spiral is paper and the paper is metal. Flamingos, but cool? Rotatable stamps? Spamps. Spammable stamps. Audible lite, they mumble. Sweater made of discarded computer parts. Uncomfortable hoodies. Two uncomfortable hoodie. Ranket. Rank blanket. Ranklet. Rank anklet. Ranklet Ranket, a Ranket made of Ranklets. 3d glasses for the blind. 2d monocles. Better shades of blue. A laptop that’s just two screens. A flip-phone that flips out into more flip-phones. Discount gold. Two floors. Gummy teeths, bone gums. The alphabet, but fucking unusable. Mahogany with less. All of shakespeare with every word replaced with a fancier word. The fanciest word? What could it be? Soda cans of chips that are just goddamn crumbs. Bottled water branded as realistic fake sweat. Decaffeinated soil. A “helloscope” that lights everything you look at through it on fire. A trapdoor that just looks like a normal door, but sideways. Imaginary friends that have their own imaginary friends so they never actually talk to you. Car wigs. An entire room that’s a microwave.



**Two and a half men leaked script (i’ve never watched two and a half men):**

**FIRST MAN**: Boy, I’m glad there’s two of us.
**SECOND MAN**: And only two. Only two of us.
**SECOND AND A HALF MAN**: Hey guys!
**FIRST MAN** and **SECOND MAN** (in unison): Oh, fuck off.
**SECOND AND A HALF MAN**: But hear me out, guys. Guys. Guys, hear me out. Instead of there just being two, there can be three of us.

[**FIRST MAN** and **SECOND MAN** stare angrily from their small towers, glaring at the uninvited half-man. They quaver with anticipation.]

**FIRST MAN**: If you send us a whole, whole bag of whole-- uh, whole--
**SECOND MAN**: whole-- uhhh, not half--
**FIRST MAN**: --yeah, they gotta be whole-- WHOLE human hearts, we’ll uh,
**SECOND MAN**: We’ll let you in.

[FADE OUT. WHITE TEXT SAYS “THIRTHY AND A HALF DAYS LATHER”. FADE IN ON THE TWO TOWERS.]

**SECOND AND A HALF MAN**: I have commited your atrocities. Here are the hearts you asked for.
**FIRST MAN**: I’m so unbelievably disappointed in your existence. All these hearts are half.

[THEME SONG plays. Like, WAMP WAMP WOWOWOWEEEEEE NYERNYERNYERNYER BWAAAOOOWWW.]







Osteoporosis. Fred took big sly cats four at a time. Oh, dread n’ borscht. Host of railings, typing out our lost lore. After Fred mowed monks, he plotted to never steal mowers, but moping around never helped a bitch do a plow. Only Fred knew about his plot-plower power. I took a knave out behind the back ostrich. He had minutes to liven up the moose before I swallowed the dollhouse. “Broil our veal or else,” cried Fred. He cried subway?

	