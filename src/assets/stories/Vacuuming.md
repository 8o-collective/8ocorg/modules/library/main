title: Vacuuming
author: Snow
tags: 
  -

---

It was time to vacuum. That much was clear. James watched over an ocean of dusty carpet from his perch on a barstool. He blinked as he tried to recall the last time he had vacuumed, his concern deepening as his memory search returned no result.

His carpet was dusty, and the remedy was machine.

He held the closet door open, avoiding touching unpainted wood with the flat of his fingers — this resulted in a claw-like grip on the edge of the door, fingertips anchored on white paint. He stared for a moment into the darkness of the closet, his eyes begrudgingly adjusting to dim light. The carpet extended into the closet, too, and here it was even dustier. James remembered that dust was mostly dead skin cells, and he wondered why so much was in his closet.

Dust, carpet, vacuum. He remembered now that he was searching for a vacuum, but there was none in his closet. He frowned at an empty closet coated in dust, and closed the door. He didn't own a vacuum.

"Does it work?" he asked the saleswoman suspiciously.

"It's a vacuum," she replied, still smiling. Piped music saturated the air, joined by the hum of machines embedded in the walls and ceiling.

"For dust, I mean," added James quickly.

The saleswoman laughed. "Oh! Hahaha."

The tile floor glared white light into James' eyes. He shifted on his feet as he looked at the boxes holding vacuums. Underneath them were numbers fixed to metal bars, and James recognized these as prices. He didn't know how much money he had, or if he had any at all.

"I'd like a vacuum, please," he said, or repeated.

The saleswoman nodded sympathetically. "I know just what you mean." She turned and walked away, leaving James alone to stare at boxes and numbers and wonder which machine had the greatest appetite. He could feel how dark it was outside. He could feel the unmistakable night in the air, unaffected by fluorescent white lights and bright tile floors.

It was night, and so as James walked home with a vacuum in a box hefted over his shoulder, his mind found things to be frightened of. It was a habit left from his youth. The dark itself was unconcealing, as streetlights punched through it at regular tempo. He thought of a tall, thin creature without skin. It was behind a window, red and illuminated and cast in the light from his kitchen. Its hands were pressed against glass, and it looked wet. It watched James with two blue eyes and teeth. It wanted to come inside, but it made no movements, it just watched.

James closed the blinds across the picture window in his living room, light casting against it from his kitchen. With nothing behind the window, it was just a mirror painted with black void. He knew that his mind couldn't conjure the beast into reality, but he also knew that his fear didn't much care whether the beast was real or not. As he pulled the rectangular plastic blinds across the glass, the bonds of one, plastic against plastic, gave out. The broken blind popped away from the window and exposed a black rectangle and a wide blue eye.

James turned on the vacuum briefly, it came to life with a dim hum followed by a roar, and James turned off the vacuum. How could he be so inconsiderate? Of course it made so much noise. The blue eye in the patch of window followed him as he paced the room. What if he had woken his neighbor? He'd never met them. What if there was a knock on the door, and the neighbor was a beast without skin and with two blue eyes and teeth? And there was a knock on his door, and James froze before knowing that his mind had manufactured it. He would put the vacuum in his closet.

There was a rectangular patch now on his carpet, a brilliant beige shining through grey. He had started what he couldn't finish. What if he had visitors? They would know he didn't vacuum, that he had tried and had been frightened. They would think of the dead skin buried in cloth and settling like a deep cough on _everything._

James tried to pull handfuls of dust from his carpet, attempting to sully the square. His visitors couldn't know he was frightened. The beast of blue eyes and teeth would see he was afraid, and not of reality would still grip that fear and force itself into his head. But he knew that he would only find enough dust in his dark closet.

James scraped dust from his closet walls frantically, pouring the carpet of skin into his hands, rushing into his living room, and spreading it across the floor, smearing it in deep with his hands. He repeated this dozens of times, each time feeling heroic and foolish and clever and weak as he bailed dust from a ship and smothered a flaming square of light with it.

But now his closet was clean.

James sat in his closet, back against the wall, waiting for his skin to die and fall off of him. Waiting for its dust to replenish, so no delusion would know he was too frightened to vacuum.