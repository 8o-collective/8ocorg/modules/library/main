title: Mirror
author: Snow
tags: 
  - delusions

---

Have you ever stared into a mirror for too long? Watched yourself become a stranger? Have you ever drove to school or to work, and then couldn't remember getting there?

That's how I got lost in the mall.

I was there for a shoe. At least one, maybe even a pair. The fog of dissociation settled over me wearily as the blurs of people, waves of faces spread in the mall. I clutched my money from within my jacket pocket tightly, as though worried it would leave me. I tried to adjust my grip, loosening my hold on the money, but my mind didn't have such nuanced control over my body.

I was walking, I think. Tumbling forward, leg to leg, dizzy in a way that shouldn't have affected my motion, much less effected it. I shifted my gaze to scan the horizon of the mall, gauging the wide open area, more faceless blurs streaming in and out of stores, detached from time in the same way I was. But one of these blurs remained still, drawing my gaze like a magnet. The little control I had over my eyes was relinquished to my body, and my sight transfixed there. It was me, I think.

I think. My eyes stayed glued and my shambling legs shambled in a different direction, pivoted by the odd attraction, and I was pulled to the body behind the glass. The glare outside the storefront that held the figure cast a reflection that I peered through, trying to stare through myself -- almost taken aback by the lack of recognition. That figure behind the glass resolved slowly in my mind's eye.

I don't know how long I stood there, but my body froze me out. Joints locked. Nothing moved, not even the faceless on my periphery. They were soft, frozen blurs. The reflection on the glass casted my face back on me, projecting my image in front of the plastic self. I watched myself, my eyes transfixed on mine. My mind screamed at my body to move. It refused, and so I sort of pouted at myself, pacing inside my head. I looked for details that would give myself away. My being was encompassed by this image -- this was what I looked like. Joints locked, steady. Slow blurs froze, eyes froze, watered without blinking. Plastic limbs fixed in the void. Joints locked, slow stayed steady. The glass melted beyond my vision, and with it, the reflection. I watched my reflection walk away, my being waltzing through the mall, shambling like I always shambled, leaving me behind, plastic joints locked, slow and steady. My reflection blurred as it left me on the mannequin, still watching. I lost myself in the ocean of blur, and before long, I was snapped to a mannequin in the mall, still not moving.

I woke up the next day with new shoes, memory lost in that mannequin.