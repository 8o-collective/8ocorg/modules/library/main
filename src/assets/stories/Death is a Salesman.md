title: Death is a Salesman
author: Snow
tags: 
  - implies-afterlife

---

A dry desert wind picked up in the dusty twilight, a dark blue eating away at the sky. The acrid smell of gasoline hung in the air, deadening the senses. Across the landscape was nothing but dirt and cacti, and a wide asphalt road, unpainted and cracked on the edges -- engulfing a truck stop that had no right to be there, like a mirage that spent so long suspended in the air that it solidified.

An old, lime green sedan hurtled down the road, filled with the horrible singing of Michael Stanth, a man in his mid-30s physically, and mid teens spiritually. He turned down the radio as he realized that the road terminated in a truck stop. He fumbled a folded map from the glovebox.

There was no truck stop on the map.

He inhaled deeply. Sammy would kill him if he wasn't home for dinner. He was making a pot roast. Hell, knowing Sammy's pot roast, Michael would kill himself if he wasn't home for dinner. He reluctantly pulled up to the truck stop. He'd ask for directions.

The car door opened, and Michael stepped out and breathed in deeply, then regretted it. He coughed as the wonderful cocktail of gasoline and cigarettes invaded his lungs -- along with the unsettling scent of something deeper. Goosebumps erupted on his neck, and he shivered involuntarily.

A door slammed, and Michael jumped. He opened his car door and crawled back in. Nope. No, not today. He wasn't going to get murdered in the middle of nowhere at the world's shadiest truck stop on his way home to dinner with his husband.

He turned the keys, and shifted into reverse. Four loud pops in a row, following by a grating crunch, followed by his gas meter dropping from full to empty like a brick. After a couple seconds of awkward silence, his rearview mirror fell off and the hood of his car set on fire. He open the car door and stumbled miserably back out. He turned and looked back at the derelict truck stop.

It was full of used cars?

A hand gripped Michael's shoulder. Without a second thought, he instinctively whirled the back of his hand into the face of the offender. It slammed into something hard and sharp, and he doubled over in pain.

"Rude," it replied. Michael glanced up.

The glint of cold bone and two empty eyes like searchlights pierced the darkness. Metal horns spiralled into the air, framing tousled wispy black hair. A grin stretched across its face.

Michael fell backwards and began scrambling away. The thing slowly paced forward, stretching out a hand.

"Lemme help you up," it joked. Michael wasn't in the mental state for processing a joke right now. He flipped over, stumbled to his feet, and started sprinting. He looked back at the skeletal horror.

"Watch the wall," it called after him. Michael rammed into a stone wall full tilt and dropped.

I'd say things "went black" to indicate unconsciousness, but Michael's perception was that he rammed into a wall and teleported inside of the world's shadiest truck stop. He sat in a tan chair on brown carpet framed by generously beige walls. A drabber interior decoration the world had never seen.

It entered the room, still smiling.

"I've noticed you're in the market for a used car?" he suggested. Michael sank deeply into the chair, gripping the arm rests.

"Are you Satan?" he whispered.

"Close. I'm a salesman," it winked. "Let's talk price range, Mr. Stanth. We've got models here for everyone, including a car for a family, on a budget. You and your husband were thinking of adoption, no?" it casually added, staring at its skeletal lack of nails disinterestedly. Michael's eyes widened somehow even further.

"Are you a ghost?" he choked, almost inaudibly. The thing chuckled.

"Name's Death," it replied. Michael bit his lip, eyes welling up with tears.

"I'm… dead?"

Death laughed uproariously. "Oh, god, no. You're just in the market for a lovely refurbished vehicle."

"You want me to trade my soul for a car?!" wildly conclusion-jumped Michael.

Death's eyes narrowed. "Why--... Why would anybody want your soul? No. I want something far more valuable from you. Money. Take a look at our selection!" It snapped its fingers.

And then, they were standing outside in the parking lot, having not moved between two places -- rather, the two places moved between them. Mediocre used cars filled every parking space. Michael's car was brightly burning on the horizon, a somehow cathartic sight.

Death strode forward confidently, laying a bony hand on an ugly hatchback that was almost red.

"This baby's a 2008 Warrior-Spirit," he breathlessly continued. The car was neither manufactured in the year 2008, nor was _any_ car named 'Warrior-Spirit'. Death casually flopped its hand backwards and gave the best possible impression of slyly raising an eyebrow without having eyebrows. "Lots of seats for any… _little ones_ the future might hold."

Michael contemplated the car. "I don't like the color," he decidedly stated. He then realized that he was genuinely contemplating a purchase from the specter of Death Itself. "Wait, no--"

Death interrupted him by sweeping around and pointing at another, exactly alike hatchback -- this time in the same lime green as his once-sedan. "Perhaps a better paint-job?"

"Oooh," involuntarily exclaimed Michael. Sammy loved green.

They were inside the car. Death's lighthouse eyes leered over him. The seats _were_ comfy.

"For you? I'll cut a deal." It leaned in conspiratorially. "$5500." Michael gasped, then tersely bit his bottom lip and stared at the roof of the car for a second.

"Ah, what the hell. Death, you've got yourself a deal." He stretched out his hand.

"You probably don't want me to shake on it. Touch of Death, and all," it flippantly continued. The car was soaked in a disturbed silence before Death unhinged its jaw and cackled. "It's a joke, son," he laughed, before clasping Michael's hand and shaking rigorously. Michael felt a piece of his soul murderously eject itself out the back of his head.

With a violent flash, Death's bones peeled away and flesh bubbled from the void, filling the space. A violent red skin knit itself over the muscle and sinew, and a grinning demon was holding Michael's hand. Michael screamed.

"I LIED, KID, I _AM_ SATAN," he cackled maniacally. "YOU JUST SHOOK ON $6000!"