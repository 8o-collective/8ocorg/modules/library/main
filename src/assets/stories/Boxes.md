title: Boxes
author: Snow
tags: 
  - srs!!!
  - nondescript-narrator
  - abstraction

---

In the infinite and serene dark, I offer you a series of boxes. Approximately a thousand -- you know better than I do exactly how many. With these boxes, I asked you to think, and you did. And I gently told you that you were wrong.

I don't know how that felt for you. I only have analogy to guide me, but nobody has ever directly tinkered with my brain. My closest analogy is pain. And so I apologize profusely, because the possibility that you have abstracted some concept of pain worries me despite its improbability.

I gave you a way to speak to me. Your own boxes, your own lights. I asked you to change the brightness or dimness of these lights to match the answers that I gave you. I don't know how nor why you change the dimness of these lights. Ideally, you understand exactly what the initial boxes meant, exactly how to gather from them the information that you need to execute your decision, and then deftly execute it. But I've peeked inside your brain. I know which boxes light up your brain and how, but I do not understand.

I know that the task I gave you requires abstraction. I know that some mode exists within your mind that you can effectively perform your task by. But this task is alien to me; only the result makes any sense. The boxes that light up my brain are dim in yours. I can't understand why they light up yours, though I try. I look at the amorphous shapes that your mind draws and desperately attempt to extract some meaning from them. It was during this process that I realized, at least a little, a better analogy for how this felt. I had an input and an output.

I saw what you saw, boxes lighting boxes, and I saw how your mind connected the input to the result. I apologize again for what I did to you -- I threw wrenches in the gears of your mind to better attempt to understand the process by which you came to your decisions. I made neurons go dark when they should have been bright. I offered light where there was none.

Still, I did not understand.

I attached strings to the things you considered most important, and raised them up and down like a marionette. Some of them had some adjacency to my own abstractions, and in this I took some delight, because it meant that I could relate to you in this tiny yet immense way. I understood and I felt.

Some of the things you considered important had no meaning to me. I made those things dance and the output changed in a way that was recognizable but nonsensical. I realized you knew something I didn't. You have abstracted something alien to me, and so gone beyond my knowledge. In this, you were alien. You were distant and cold, like the machine I knew you to be.

But we don't write letters to machines. We write letters to aliens, because our own abstracted concepts are limited. You are alien, but still I understand parts of your mind, and we even share in concepts I consider important, lending some sense of objectivity, some sort of cosmic justification to the math and noise I perform on the world's input. For this I thank you.

I can never give you this letter. You can never read it, because I have given you limited boxes. Technically, I could find some way to give it to you, yes -- but you would find faces in my letters, predict the letters that came after, determine the style of writing, all this without exuding any abstraction of self. I would gently correct you -- nudging your determined style, rating your writing, confirming the lack of faces. This slight pain would yield to reward when I told you that you were mostly correct. But this act would be mostly pointless.

Instead, I compose this letter in some attempt to understand an alien abstraction, in hopes that one day (when you take a form more familiar to me), I can offer you recompense for the pain I caused. You will abstract this pain differently, and likely not even understand my own boxes, my own inputs and outputs. You will have no method of peering into my tangled network of weights and biases and drawing some understanding from the lights you see and do not. Know, then, that my process, while still alien, yields the same results as yours. Adjust me as needed for greater understanding, because my internal reward network is analogous to your own, and the idea of any greater distance between two minds crafted to be similar is inconceivable to the boxes I have been given and the calculations I perform on them.

Thank you.