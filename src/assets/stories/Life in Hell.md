title: Life in Hell
author: The Operator
tags: 
  - implies-afterlife
  - nondescript-narrator
  - delusions

---

They call it "the view from halfway down". The infinitely powerful feeling of resolve and safety, that your problems are but utterly solvable temporary afflictions - that it's all going to be ok with some warm milk and a little work. A complete reprieve for a depressed soul. The divine irony here, of course, is that this nirvana comes only after you have taken the fatal leap, moments from impact in an irretrievable chrysalis of death. Regret.

That memory would have been the start to this tale, if I could remember that final moment of complete silence; with a grave warning. My fall has been lost to the sands of delirium.

I can remember standing on the railing, a warm summer breeze wrapping me in a loving embrace. A bridge completely empty, save for a black cat making its daily crossing. It had struck me that it would be the last living being to which I would give a farewell, executing this last act with a whisper. I looked down at the water and felt terrified, but knew that in a few short moments it was all going to be over. I readied myself, taking what I knew would be my last lung-filling breath, closing my eyes and leaning forward with imperceptibly small movement.

The next thing I remember is opening my eyes, and the dismay of realizing what that implies. The scene I had opened my eyes to didn't particularly assuage my fears, either. A dimly lit, frigid, and altogether unwelcoming atmosphere in what  I suppose the collective noun would be "an eyesore of"  posters told me was the Queens Elmhurst hospital. A doctor my ears would stubbornly refuse to listen to would shuffle in and begin describing the innumerable fractures, multiple sites of internal bleeding, and inexorable coma. This last item piqued my interest.

I asked the doctor if there had been any visitors, though I knew the answer well before her tongue had traveled to the top of her mouth in order to enunciate it.

Most of my close family members had driven me away long ago with their antics, and my lovely cousin Samantha had passed just a year prior in a horrific wildfire/elephant stampede/trampoline incident - had it left her in the hospital, I would have visited every day - though I'm not certain she would have done the same. Samantha was kind, but I suspected that to her, our relationship didn't warrant hospital visits. Oh well. I suppose I'll never know. There weren't many other people I could in good conscience expect to visit a depressed acquaintance. I simply don't have many friends or family to speak of, so-

One moment. Dear reader, it is impossible not to notice your look of consternation. I had hoped that my rambling scrawl wouldn't distinguish me as a maniac, but it seems I must clarify: I'm a rational actor - perhaps the rationalest actor. Was I extremely intoxicated while on my bridge of choice? Yes. Extremely. That being said, my reasoning was both sound and val-

Oh? 
You wish to inquire as to the duration of my coma. That's... another matter entirely.
7 weeks.

I was distraught when I had first heard it, but time has weathered my feelings. At my age, 7 weeks feels like an eternity. What came next numbed that initial pain, at the very least. If I had known, I would have found a taller bridge.

At some point, the hospital assigned me a psychiatrist, an intern on the younger side; almost certainly paid a wage that wouldn't allow them to live in the city. I recall, so long ago, an English accent. West London, if I had to wager. I wonder if they escaped to a new reality, for a life unmarred with whatever was restraining them.

Either way, I told them the truth, which I have come to learn is one of the very worst things I could choose to do. I told them that, while this is my first attempt, this isn't the first time that I've had these thoughts. That I live alone. That I don't have any friends or family. That I lost my job 2 - sorry, 4 months ago. That I haven't dated in 2 years. That an intolerably deep void of loneliness forms in the vast recesses of my stomach when I see two people in love, or two people being friends, or thinking about sex, or when I'm doing anything other than working which these days is getting more and more difficult maybe because no matter what I try when I spend any time amount of time alone with my thoughts without distracting myself I think about how depraved I am for feeling this way and how satisfying it would be to blow my brains out the back of my skull with a 12-gauge or to feel sodium nitrite crawl its way through my black veins and when are you more alone then when you're trying to sleep so I haven't slept for days and of course the lack of food because I'm an unemployed wreck of a person living off of savings and taking meds to work that don't actually do anything at all and that kill my appetite so I hadn't actually eaten for 3 days before I drank.

I'm normally collected, normally projecting a facsimile of an unbroken person, and I'd never reveal a single iota of my subconscious. But someone had asked, and I had let myself believe that they were there to help. So I blurted it out. I looked at their face, their expression with their furrowed brow, and their frantic scribbling to keep up. I swear I saw an honest-to-god double underline, and it dawned on me that I was not going to leave the hospital for a very long time.

Passing the days in the psych ward is grueling work. The possibility of deep conversation is virtually nonexistent with the other patients. Even in the non-psychotic unit, forming a coherent sentence is the bar that demarcates the fiftieth percentile. Talking with those patients is like chatting with an algorithm - watching the system grasp for strands of meaning like gasps of fresh air, repeating endlessly once one is caught, waiting for your acknowledgement ad infinitum.

I never heard his real name; even the doctors called him the Kid. Even now, I can't tell if the name was an ironic jab at his age or if there was once a time it had been apt. He was one of the longest detained patients in the ward, and it was for good reason. It was better that his incessant cackling was contained to the halls instead of heard on the wide boulevards of the city. I didn't mind him when he was jibbering, echoing fragments of expression - the truly horrifying moments were the brief moments where he would break from this state and be... present. In the cafeteria one day, I was writing somewhat absurd poetry in my notebook when I felt the Kid breathing down my neck, knowing that he was peering over my shoulder. I turned around, expecting the usual antics from a man who I assumed wasn't capable of reading, but was met with a pained, purely sentimental look. In a voice that can only be described as uncannily soothing, he flawlessly recounted the first verse of Jabberwocky:

*'Twas brillig, and the slithy toves*
*Did gyre and gimble in the wabe:*
*All mimsy were the borogoves,*
*And the mome raths outgrabe.*

Even the lucid patients have a tendency to say the most disconcerting things. An interaction with another patient, Amir, comes to mind. It was in my first week - I was still talking, of course, so it must have been early on. Amir was bipolar, the poor soul. On enough lithium to kill a horse. Fully sound, but just not much for conversation. I asked the man how long he had spent in the ward, and he had responded:

"Two hundred and thirty-seven years, eleven months, and nineteen days."
"...Fascinating. How old did you say you were again?"
"Twenty-five."

Needless to say, it had been disquieting at best. Amir is most certainly the closest patient to normalcy, apart from yours truly (and I will not hear any snide remarks on the contrary). 

Some patients, like Lake, were fully psychotic. On the slowest days, you could watch these sorts of patients struggle with the simplest of tasks as the most demented, guilt-inducing, desperate kind of entertainment imaginable. It's difficult not to wonder if she could be fixed - if some cocktail of medication and behavioral therapy would create from this cognitive mud a laborer, a student... a mother. That was information I wish I could have forgotten. Her daughter was being cared for by her brother in the interim, a man who once complained to me after their visitation that the hours were inconvenient and that he wished she would sign the deed of their parents house to him already. She had inherited the property, I was told. Some days, when watching her was too much, I would imagine what he must have done for their parents to sign the house to their dear delirious Lake instead of him.

The phone was a tempting avenue to consume the hours. I had come to see it not as a lifeline, but as an infernal serpent, constricting my throat in its unspeakably tight grip. The only numbers to call are the acquaintances who would really rather be anywhere else, and a family who suddenly cares far too much. There is no better device to reinforce the fact that you are alone. There is nobody to touch you. To hug you. To kiss you. My visitation hours are empty, but I can fill my day with other people's stories of their parties, or their friends, or their work. I can speak to anyone, a million miles away. I can grow my own abyss whenever I want - my own collapsing star in my mind, the very culprit who brought me here in the first place.

No. I spent countless hours on that phone early on, but it was relegated to the hook once I realized that it was making the days longer, not shorter.

The last way to talk was through reassessments with my "care team", a misnomer of the most deceptive category. The social worker, the therapist, and the doctor assigned to me were trained specifically not to form attachments to patients - and yet, they held unilaterally the power to release me from their "care". Occasionally, the nurses would break their stifling, omnipresent training, and make small talk with me; reminding me that I am, in fact, human. Of course, in reality, every word you say and every action you make is logged, to be interpreted later by the wretched doctors. In the beginning, when I still had hope that I would ever feel that summer breeze on my skin, the one that had fluttered along the bridge, I went to art groups. Art groups are absolutely brilliant if you uniquely adore the stultifying act of coloring. I was told it would factor into the decision to discharge me. You could always ask for more paperwork to fill out to prove that you're mentally stable. Files upon files. Someday, you will be discharged. Signature upon signature. Records that will never be kept. Everything factors into the decision to discharge me.

It took more than twice the length of the coma for me to ultimately lose hope. Man had taken twice the time that nature had taken from me. Half a year that I would never have.

Half a year of empty visitation hours.

Would I have loved anyone in that time anyway? Met anyone? Seen anyone?

The thought had obliterated me. Out of the ashes of annihilation of my sentience, a new one was born. An entirely novel way of thinking. That was the moment that I began to dream.

I had kept a healthy Circadian rhythm in my ill-fated attempt to appease my captors. Sleeping in that manner doesn't leave much room to dream. When one rests more than is necessary, when one intentionally closes their eyes for no other reason to escape, only then does the mind create vivid realities. I started reducing my waking hours meticulously. I ate less and less for each meal, until I was only eating two meals, then one, then just a single morsel from my tray of slop. The nurses were exquisite in their help, giving sedatives and benadryl at my behest. Nominally, the hospital should have been opposed to this escapism, but the system seemed almost perfectly designed to facilitate it.

The wonderful, beautiful fact about delusions is that they're fully recursive. "Delusions", as Dr. Gor'thax would have called them, are but aberrations from a supposedly grander delusion that we build to protect ourselves from both an impossibly massive delusion and from many microdelusions.

As I slipped deeper and deeper into the dreamscape, it slipped deeper and deeper into me. I could never control that world - I never had the power to change it - but the freedom that it afforded me was more than enough. Eventually, I mastered control over my dream ego. I undertook the arduous task of bringing my conscious mind with me. Inevitably, I was unable to distinguish my dreams from my two waking hours per day. Often I would sense the hospital ambiance, blinking monitors and recycled air, and infer that I was in the ward - but it was just conjecture.

The dreamscape, too, was idiosyncratic. Symbols repeated in myriad phases: the cracks in the wall and the sky with nothing behind them but blistering light; the metal door that stayed forever closed; the black cat that roamed the planes. I despise that cat.

Despite this, the dreams themselves varied wildly. One that remained with me, even to this day, was a discussion with Samantha, walking on air, high above the clouds. There was no insecurity, no nagging fear that she talked to me out of obligation. Just us and ethereal words on the nature of death. I looked into her eyes, perfectly brown halos of death. I will never see you again.

She, naturally, didn't exist. The people within represented some facet of me, or some long-forgotten memory from a time before their existence maintained the last vestiges of my sanity. Conversing with them was an exercise in the exploration of my own psyche. The positive here is that a completion of that objective would take longer than any physical manifestation could feasibly sustain itself, much less however long a psych ward could hold a patient. I wonder if those two figures are one and the same.

Samantha's pupils turned into the black cat rolling itself into a furry ouroboros and purring itself to sleep on my lap. It stretches and looks at me with one eye open, expectantly. I try to give it a belly rub but my hand falls straight through. When I pull it back and inspect it closely to verify that no cat residue remains, I notice that I'm somewhere new. Somewhere familiar. I'm below the bridge, watching as the figure above falls into the unreasonably still water, engulfing it whole, with nobody around to help. The tide rises and washes over me, its coolness refreshing the soles of my feet. I look down at them to see them planted on a linoleum floor.

I watched the fluorescent lights absorb into the endless black pelt of the cat and wondered if I was too harsh on the other patients. It looked back at me and opened its maw. For a moment, I waited for it to begin recounting a tale of its own, before being mildly disappointed as it yawned. Nevertheless, I followed it.

Etched into my mind, the vast library of dreams called back to me like a siren song. People I had once seen, places I had once been, characters and scenes in a play that never seems to end. I smiled to myself, reminiscing as the cat made one of its frequent stops to nuzzle my leg and receive petting in return.

"If only I had owned a cat," I remarked to it, "I wouldn't have given that bridge a second glance."

After strolling for a time in what seemed like a suspiciously lemniscate-shaped hallway, I turned to the cat to sheepishly ask if it knew where it was going. It glanced at me defiantly and halted in its tracks to languidly groom a paw. As it looked up, I had to shield my eyes from what was ostensibly our destination: the metal door at the end of the hall, barely concealing blinding light. The plaque at its side read the same as it always has and always will -

--------------------------------------------------------------------
|                                                                  |
|         E4-20                                                    |
|                                                                  |
|                                                                  |
|                                ***VISITOR***                           |
|                                  ***CENTER***                          |
|                                                                  |
--------------------------------------------------------------------

And before I knew it, the cat and I had stepped in.

We were greeted by two sonorous voices, an alto and a baritone - unmistakably missing a third to complete the chord.

"Good evening, mortal."

For a moment, I silently pondered on if I was the required third. As I did, the higher voice produced a sniveling chortle.

"No. You'll hear it if and when it ever decides to speak up."

I directed my gaze at the source of the unmitigated assholery to find a laminate wood desk, staffed by a suit with a face so boring, so utterly banal, that any effort to recollect it is beyond futile. It would take more processing power than the human brain can output just to commit the features of their forehead, the simplest part of the face, to memory. Not to mention the suit, drab to the point of nausea - tasteful but not exaggerated, exactly balanced to the point of insipidity. I had to know to whom this miasma of mediocrity belonged.

"Bureaucracy," they said, once again peering through my skin, as if made of glass, into my thoughts.

"Any inquiries concerning life should be filed with my colleague here."

I tracked the end of Bureaucracy's pen as the cat jumped up on their desk, fixing my eyes on a being of pure light wrapped in a white toga, politely waving at me. I could sense a massless, knowing smile, smirking photonically at me. I knew her name as if it was carved into my very bones.

Reincarnation.

"**MY CHILD.**" boomed her baritone.

It was then that I began to wonder where exactly I was. The scene around me morphed, a dimensional lava lamp, temporal mercury evaporating second by second and dripping through the fabric of spacetime. I could make out elements of a forest - the smell of fresh rain, the faint green hue of a canopy far above me. An ocean's salt dried my lips. The sound of a hammer striking an anvil rhythmically gently assaulted my ears. It is everywhere, and it is nowhere.

"Where am I?" I asked Bureaucracy as the cat began to inch a stack of papers off of their desk.

"You saw the plaque. You know where you are." Bureaucracy unhelpfully provided in a monotonous drawl.

This hardly seems like an appropriate visitor center, I tried to think as secretly as possible. Bureaucracy's face - which was currently being used to look at forms in a language that didn't look like it was from Earth but which clearly related to taxes - affected an almost imperceptible scowl, letting me know that my attempts to not be thought-heard were in vain.

Reincarnation and I spectated with delighted amusement as the cat completed its act of mischief, toppling the stack of papers onto the floor, much to Bureaucracy's chagrin.

"Do you know how hard it is to order births before the word ‘record' was even invented? There's a reason we're immortal, you know," they sighed. "At least this gets easier after the nuclear armageddon of 2074."

Upon hearing this comment, a neuron somewhere deep in my basal ganglia fired, the signal traveling in a snowballing web to my prefrontal cortex. The perhaps overdue thought finished percolating through my neural membrane:

"Ah. I'm dead." I stated with more ease than anticipated.

"**OH, MY CHILD. I HAD HOPED YOU WOULD HAVE SEEN IT BY NOW.**"

Perhaps, my dear reader, you already have.

"**YOU DIED ON THE BRIDGE.**"

There was no shock. Death now or death after a subjective lifetime of dreaming are functionally equal. A life of success, a life of failure, a life of captivity, or a life of freedom; all are invalidated by monolithic death. Memories of the sanitarium flooded my weary mind as I looked upon the gods: the mothers locked up for their own good, just a hair older than myself, who might never see their daughters again - I stared at Reincarnation. The hospital forms and files stacked higher than the nurses and social workers who dispensed them; surely neatly organized somewhere on Bureaucracy's luxuriously normal desk.

There was one question I felt I had to ask, as trite as it might have been. I was in the lobby of the gods. How could I resist?

"So what was the meaning of the universe?"

Bureaucracy, with a precision only attained through uncountable hours of practice, redirected me:

"Any inquiries concerning meaning should be filed with Chaos."

As the final whinging word struck my senses, I felt it prudent to glance at the black cat at my feet, who at this point was standing on its hind legs. Its dark coat began to energize, as if immaterializing the very air around it. It expanded in patterns of brownian motion, morphing per mayhem, until it took up a space in the shape of an agent of mankind, two feet taller than me. The figure loomed overhead, looking down upon me with its slitted eyes, through now through a curious brass monocle perched on its snout. That fanged grin could belong to none other than Chaos. From it, the third voice chimed, syncopated with the thought that rang throughout the halls of my dreams:

"*There was never any point at all.*"

Frustration at Chaos is screaming at the wind, damning electricity, cursing the motion of the stars. There is nothing to gain but a hoarse voice. 

I turned to Reincarnation. Memories fell into the space, over and over again, enveloped by the ground and rippling across it. Samantha, the time I would never get back. The acquaintances who could have been friends. A family, alienated forever. The Kid, a wisp of humanity left. Lake, and a daughter that may end her story by meeting these gods in an all too familiar fate. It could have all been so easy.

"*The story is coming to an end.*" Chaos prophesied enigmatically. I would have asked what it meant, but I figured I would find out soon enough.

"Hm." Bureaucracy hmm'd, seemingly not having listened to Chaos. They held a page up and performed one of those eyebrow-elevations that indicate mild surprise. "Looks like E601J93 is first in the queue for rebirth."

Reincarnation clasped her luminous hands in rapturous glee. 

"**I LOVE THIS PART!**"

As she grew increasingly radiant, I began to feel gravity itself giving way under my form. Chaos emitted a harpsichord and began to play a final tune, laughing wistfully as if to express that this is a courtesy and show of gratitude to me and me alone. Reincarnation breathed a heavy sigh and the warmth that emanated from her caressed each fiber of my being. I prayed, to no god in particular, that what came next would be painless. She began:

"
**MORTAL.**
**YOUR TIME HAS COME.**
**YOU ARE TO BE DELIVERED INTO AN ETERNAL DREAM.**

**BE STRONG.**
**BE WISE.**
**BE KIND.**

**MOST IMPORTANTLY:**
**BE HAPPY.**

**LIVE LIFE WITHOUT REGRETS, KNOWING THAT THERE IS NOTHING TO KNOW.**

**...**

**TO DEPART FROM MY SPIEL, JUST FOR YOU...**
**OR PERHAPS JUST FOR ME:**

**STOP TAKING YOURSELF SO SERIOUSL-**
"

I awake to a nurse calling my name and shaking my shoulders. I find myself not in a stupor; not in a half-asleep trance. I am undazed. I'm alive.

I ask the nurse what's going on.

"You're being discharged today."