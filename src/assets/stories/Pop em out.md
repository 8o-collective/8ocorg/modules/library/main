title: Pop em out!
author: Snow
tags: 
  - awful-title

---

Two men sat in a room, across a table from one another.

“Pop ‘em out real quick and we can take a look,” casually started one. The other tested his eyes, swivelling them around, darting them every which way.	“That doesn’t make sense,” replied the other. “I can’t pop out my eyes.”

“I think you could.”

“I mean, *could*, sure. Yeah. But I don’t think that’s... conducive to my health.”

The other paused for a moment. “Conducive...” he quietly repeated under his breath. He tapped the table absent-mindedly with his index finger, rapping a rambling rhythm. “I mean,” he breathlessly continued, “if you think about it, there’s not a whole lot that’s conducive.”

“Conducive to what?”

The first froze, index finger hovering over the table. He attempted to look anywhere else than wherever he was looking. It didn’t work very well. He breathed in deeply through his nose. “Listen, Jimmy-- can I call you Jimmy?--”

“That’s not even tangentially related to my name,” quietly replied the other.

“--Jimmy, we both know what conducive means.”

“Really.”

This line of questioning was unexpected, to say the least.

“Absolutely. We both know a whole lot of words, one of which being conducive.”

Silence settled on the room.

“I just want to hold your eyes,” he continued, probing carefully at the silence.

“Mhm,” replied Not-Jimmy. He nodded slowly, once. “Good talking to you, but I really need to...” He trailed off. The silence returned briefly as he contemplated finishing the sentence, but he gave up and stood. He turned around and grabbed at the door handle. There wasn’t one. Door handles imply the existence of doors. He looked around the room for one. Finding none, he settled back into his seat at the table.

“Tea?” innocently asked the one that wasn’t Not-Jimmy.

“Sure,” he tiredly replied, curious how tea would be managed with no kettle, no cups, and no tea. “What’s your name? The author is tired of finding weird ways to refer to you.”

“Fuck the author,” quietly spat some asshole who didn’t know English. He pantomimed pouring tea. He held the imaginary cup to Not-Jimmy. 

Not-Jimmy graciously took the cup and sipped the tea. He wasn’t sure what he expected, but as he brought the cup to his lips, he was greeted by the sensation of nothing happening. He relaxed his tea-hand and wiped his face with it in a frustrated gesture. The other looked horrified as the tea cup toppled to the floor, grew legs, and scampered directly into the concrete wall, ignoring the solidity of said concrete.

“A door!” he screeched, torpedoing himself headfirst into the wall, and crumpling to the ground.

Not-Jimmy got halfway through a spit-take before he remembered that the tea wasn’t real.

“Oh, it’s just gorgeous,” gasped Connie. The dress was startlingly red, and flowed dramatically across the ground, ruffles of fabric dancing delicately across the hardwood. Amanda grinned and spun for Connie.

Then she continued to spin.

The taps of her feet across the floor settled into an even looping rhythm. Connie smiled indulgently, an expression that slowly wiped off her face as she watched Amanda spin, her face frozen in an ecstatic grin.

Connie dropped into a sorrowful thousand yard stare as Amanda continued to tap her way in a circle, flourishing the dress. She started to count Amanda’s rotations, giving up after a minute or so.

Compelled by something she didn’t really understand, Connie rose to her feet and began to spin in tandem with Amanda. It wasn’t nearly as flashy -- she was wearing jeans and a t-shirt.

Two quick courtesy knocks on the door and the tailor excitedly entered the room, already spinning.

“So, how is it?” asked the tailor, her voice increasing and decreasing in loudness as her voice was projected into various corners. The question stalled in the air and died away, leaving the three spinning in place amidst the deafening racket of shoes and bare feet against the store’s floor.

Nearly a minute later, several police crashed through the windows and ceiling, shouting various variations of “FREEZE” and “GET DOWN”, their voices in tremolo and undirected -- as they, too, were seized by the need for spin. The sound of feet and shoes on the floor was now joined by boots crunching glass. Amanda’s dress acted as a sort of umbrella, keeping most of the shattered glass away from her bare feet, but some still made it through. A puddle of blood began to emanate from under the dress, carrying shards of glass with it. 

The room was beginning to become cramped.