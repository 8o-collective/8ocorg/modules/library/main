title: Baker tries to quit
author: Snow
tags: 
  - references-media-before-1998

---

Mr. Baker watched the ticking Newton’s Cradle. He was nestled in a high-backed leather chair, uncomfortably pulled into himself, elbows tucked close to his body, hands clasped in front of him on his lap. His eyes flicked between the Cradle and the door to his left, leering over him like an oaken monolith. After several seconds of discomfort, he alternated between pristine posture and a hopeless slouch, trying but failing to distract himself with the metronomic clacking of the cradle, and trying but failing to remember how a normal person sat. His legs crossed and uncrossed, somehow mismatched perfectly against his alternating upper body posture every time. 

He drew in a deep breath just as the door opened. His breath exploded into the silence, and he froze himself in his posture -- a grotesque casual slouch, torso rotated to the left with legs crossed and hand thoughtfully on his chin. He feigned a smile and beamed at the woman who appeared behind the open door: solemn, silent, and unnervingly tall. She watched him for a moment and stepped forward into the room, steeping in silence, before slowly closing the door behind her without breaking eye contact with Baker.

She marched behind the desk and sank into an even higher-backed leather chair, propped her elbows on the desk, and tented her hands in front of her. She had the same predatory, slightly bemused glare that everyone else in the god-forsaken building had given him. The ghost of a condescending smirk tied together the spitting image of a typical Kingsly staff member.

“Good evening, Baker,” she said, nonchalantly. She spoke with an edge of command, a slight rasp that matched her severe, hawkish features and hair just grey enough to demand respect.

“Good evening, Mrs… ahhh…?” he trailed off, waiting for her to provide a name. After a few seconds -- millenia in social time, really -- he realized that she would not.

“I understand you’re attempting to quit?” she interrupted the bleak silence. “At Kingsly, we acknowledge and highly regard employee loyalty. As an esteemed substitute teacher, you will be sorely missed not only by your colleagues, but also your students.”

Baker whimpered imperceptibly and attempted to somehow stare harder at the cradle, still happily clacking away. He knew what was coming, and he willed his outer consciousness to shut off, leaving behind only the barest social niceties required for conversational survival. He had gotten this far, a feat that (to his knowledge) nobody else had ever accomplished. He steeled himself for the incoming gaslighting and emotional manipulation. As long as he stayed disengaged, he stood a chance.

The woman withdrew a small tape recorder from a drawer in her desk and gingerly placed it on the table. It laid there expectantly, and Baker internally panicked. This was not a part of the typical social pallette, and the barest social niceties were no longer good enough to survive this conversation. He felt his emotions engage again as his brain attempted to comprehend the situation, and he slipped -- only briefly, but long enough to establish engagement. He looked up and attempted eye contact, then immediately realized it was a mistake.

She pressed “play” on the tape recorder, and Sarah McLachlan’s “Angel” began to play.

Oh.

“As I’m sure you know, you are a vital part of these kids’ lives,” she said, feigning heartbreak. The rasp disappeared from her voice and was replaced by quivering honey.

“Think of Steve.” She reached into the same drawer and produced a framed photograph of a child model crying. Baker’s heartstrings were relentlessly yanked, and despite himself, he felt the outer corners of his eyebrows drooping in dismay. He hadn’t considered Steve. The rational part of his brain scrambled for control as his mind was flooded with emotions. He searched for an exit route, desperately -- and saw a glinting neon paradise within his own mind: an escape via humor. 

It was subtle genius. He could play the whole thing off as a joke, and then deflect the awkwardness on to his opponent. It was a gamble, but she was currently the more emotionally vulnerable in the current conversation. If he could laugh at this situation, she would have no choice but to admit to her games and laugh too, or risk exposing further emotional weakness.

“Haha, yeah, wow. That’s… Steve. Won’t miss that little rascal!”

Moments passed. The Cradle clicked.

The woman observed Baker for a second, then tilted her head slightly and raised an eyebrow. Baker’s shifting gaze scattered about the room, and a bead of sweat appeared on his forehead.

More moments passed. Baker’s eyes widened as he realized his fatal mistake. He had failed to account for the third route. In his well-loved copy of “Resisting Emotional Manipulation For Dummies”, an entire chapter was devoted to detailing this trap, the Jewel of the West Gambit:

	“…when exposed to a sudden shift in emotional tone, appear to be only mildly confused by tilting your head slightly and raising an eyebrow. The following awkward silence, despite being manufactured by you, will appear to be the fault of your opponent. To break the awkward silence, the only choice is retraction of all arguments. The key here is in the ambiguous negativity of a raised eyebrow and tilted head. To double down on any arguments is to risk misinterpreting which negative emotion, and the beauty of the Jewel of the West Gambit is that any interpretation is a misinterpretation.” (Resisting Emotional Manipulation for Dummies, 184)

Baker looked down at the floor, gathered his breath, and then stood. He shuffled toward the door, defeated.

“See you tomorrow,” he mumbled.