title: A List of Obscure "Chucky B." Films
tags: 
  - enumeration
  - references-media-before-1998

---

**37 - It Was My Best Birthday Ever, Charlie Brown**  
36 - You're In the Superbowl, Charlie Brown  
35 - You're a Good Man, Charlie Brown  
34 - It's Flashbeagle, Charlie Brown?  
33 - It's the Pied Piper, Charlie Brown.  
32 - Lucy Must Be Traded, Charlie Brown  
31 - You're a Good Sport, Charlie Brown  


**30 - Snoopy, The Musical**  
29 - He's Your Dog, Charlie Brown!  
28 - What a Nightmare, Charlie Brown  
27 - It's the Girl in the Red Truck, Charlie Brown  
26 - Snoopy's Getting Married, Charlie Brown  
25 - Life's a Circus, Charlie Brown  
24 - It's a Mystery, Charlie Brown!  


**23 - Happiness is a Warm Blanket, Charlie Brown**  
22 - You're in Love, Charlie Brown  
21 - It's Your First Kiss, Charlie Brown  
20 - It Was a Short Summer, Charlie Brown  
19 - Someday You'll Find Her, Charlie Brown  
18 - What Have We Learned, Charlie Brown?  
17 - There's No Time For Love, Charlie Brown  

**16 - Snoopy's Reunion**  
15 - He’s Your Dog, Charlie Brown  
14 - It's Arbor Day, Charlie Brown.  
13 - It's Spring Training, Charlie Brown  
12 - She's a Good Skate, Charlie Brown  
11 - Is This Goodbye, Charlie Brown?  
10 - A Charlie Brown Celebration!  

**9 - You're the Greatest, Charlie Brown!**  
8 - Play It Again, Charlie Brown!  
7 - LAND, MONEY, AND POWER, Charlie Brown!  
6 - It's Magic, Charlie Brown!  
5 - Why, Charlie Brown, Why?!  


**4 - He's a Bully, Charlie Brown.**  
3 - It's an Adventure, Charlie Brown.  
2 - You're Not Elected, Charlie Brown.  
1 - There’s blood in your empty palms, Charlie Brown.  
0 - Charlie Brown's All Stars…  

You're a Good Man, Charlie Brown  