title: Logan and the Firehunk
author: Snow
tags: 
  -

---

"I'm gonna punt your head clean off your body."

"Mr. Leglord—"

"It's _Doctor_ Leglord. Respectfully. Your honor."

The judge filed through the papers on her bench, carefully peering through half-moon glasses.

"Uhh," she responded, "no."

Logan Leglord snapped his fingers and winked at the judge. "Almost gotcha."

"You didn't, really..." absently added the judge.

"I'm gonna punt your head clean off your body," repeated Logan emphatically after a brief pause.

"About that Mr. Leglord—"

"It's—"

"Shut the fuck up. Are you admitting that you decapitated Mr. Rumpers?"

"I did no such thing. But I could have." He gave his thighs a meaty slap.

"Mmmmhm." The judge stood from her seat, seemed to remember something, then sat back down and slammed the gavel.

"Uh, what was that?" asked Logan.

"Jail," succinctly put the judge.

"What?"

"Your sentence. Yeah, uh, jail."

"For how long?"

"Fuckin—... ugh. Forever. Jail forever."

The bailiff side-eyed the judge then darted his glance back to the judge. He looked back at Logan and shrugged.

"I mean, what she says goes, I guess," apologetically said the bailiff. He began approaching Logan.

"Stop!"

The bailiff froze in place. The judge scowled.

"Bailiff? Uh... jail?"

"Let's hear him out," softly whispered the bailiff.

"It's my turn," proudly declared Logan.

"Jail," said the judge again, urging the bailiff forward. "Jail! Jail! Jail!" she began chanting.

Logan stood, straightened his tie, tore off his pants from the front like a psychopath, took a deep lunge forward, brought his knee to his chest, spun on one foot, and let his thighs loose like a slingshot.

Plop went the bailiff's head, directly in front of the judge.

"Oh my god," the judge whispered, stunned.

"I know, right?" said Logan, grinning like a maniac at the judge.

"You punted his head clean off his body," she breathlessly continued.

"Yuh huh," nodded Logan. "Pretty dope, right? That's how I got my doctorate."

"I'm going to do a kickflip on your corpse when I kill you for money," answered the judge, producing a skateboard from within her robe and leaping onto it.

"Oh shit!" Logan slammed his foot into the ground and the shockwave launched a table into the air, which he kicked towards the judge, but she merely did a 360 ollie and smashed it into pieces with her sick moves. She was picking up speed.

Logan repeatedly kicked the ground, thereby gaining speed until he roundhoused a wall and punted the wall clean off the building.

"I'll kill you for sure," called the judge as Logan sprinted away from the courthouse. He looked behind him as he ran and watched the judge retreat into the distance and finally over the horizon. He cackled with joy and then abruptly ran into a huge hunk.

"Hey there babe, watch where you're going," flexed the hunk, nipples out and everything. He was wearing a fireman hat and holding a kitten. He looked down and gasped at Logan's blood covered thunder thighs.

The firehunk gently put down the kitten, spun around slowly in a full circle, then clapped twice. He shot a glance and raised an eyebrow at a nearby woman with a camera.

"Meredith. Look at his legs."

"Uh huh. Yeah. Mhm," she answered, pulling her fedora brim low over her eyes. "Oh yeah."

Firehunk turned back to look at Logan.