import React from "react";
import { Routes, Route } from "react-router-dom";

import App from "components/App.jsx";
import Story from "components/Story.jsx";

const routes = (
  <Routes>
    <Route exact path="/" element={<App />} />
    <Route path="/story/:filename" element={<Story />} />
  </Routes>
);

export { routes };
