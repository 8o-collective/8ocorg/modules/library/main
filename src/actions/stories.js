import { parse } from "yaml";

const extensionPattern = /\.[^/.]+$/;
const pathPattern = /^.*[\\/]/;
const getFilename = (full) =>
  full.replace(pathPattern, "").replace(extensionPattern, "");

export const parseStory = (story, defaults) => {
  const [rawHeader, content] = story.split(/---(.*)/s);

  const header = parse(rawHeader);
  header.author ||= defaults?.author;

  return {
    ...header,
    content,
  };
};

export const loadStory = (filename) => {
  const file = require(`assets/stories/${filename}.md`);
  return parseStory(file.default);
};

export const loadStories = (defaults) => {
  const context = require.context("assets/stories/", false, /\.md$/);
  const relevant = context.keys().filter((e) => e.startsWith("."));
  const raw = relevant.map(context);
  return raw.map((e, i) => ({
    ...parseStory(e.default, defaults),
    filename: getFilename(relevant[i]),
  }));
};
