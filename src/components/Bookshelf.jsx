import React, { useState, useEffect } from "react";

import {
  BookshelfContainer,
  BookshelfShelfContainer,
  BookshelfBookDetails,
  BookshelfBookContainer,
} from "assets/styles/Bookshelf.styles.jsx";

const MIN_BOOKS_PER_LINE = 8;
const MAX_BOOKS_PER_LINE = 14;
const BOOKSHELF_WIDTH = 400;
const BOOKSHELF_SHELF_HEIGHT = 300;

const BOOK_WIDTH_DEVIATION_FACTOR = 0.4;

const AVERAGE_BOOK_HEIGHT = BOOKSHELF_SHELF_HEIGHT * 0.7;
const BOOK_HEIGHT_DEVIATION = AVERAGE_BOOK_HEIGHT * 0.3;

const BOOK_MAGNIFICATION_FACTOR = 2;

const generate = (input, min, max) => {
  const random = (min, max) =>
    Math.floor(Math.random() * (max - min + 1)) + min;

  const inner = (arr) => {
    if (arr.length === 0) return;

    const minIndex = arr.length < min ? arr.length : min;
    const maxIndex = arr.length < max ? arr.length : max;

    const splitIndex = random(minIndex, maxIndex);

    const generatedArray = arr.slice(0, splitIndex);
    const remainingArray = arr.slice(splitIndex);

    if (remainingArray.length === 0) {
      return [generatedArray];
    } else {
      return [generatedArray].concat(inner(remainingArray));
    }
  };

  const result = inner(input);
  return result.every((item) => item.length >= min)
    ? result
    : generate(input, min, max);
};

// todo: seeded random with randy
const getRandomizedWidth = (booksPerLine, shelfWidth) =>
  shelfWidth / booksPerLine +
  (Math.random() * 2 - 1) *
    (shelfWidth / booksPerLine) *
    BOOK_WIDTH_DEVIATION_FACTOR;
const getRandomizedHeight = () =>
  AVERAGE_BOOK_HEIGHT + (Math.random() * 2 - 1) * BOOK_HEIGHT_DEVIATION;

const Bookshelf = ({ visibleStories }) => {
  const [chunkedStories, setChunkedStories] = useState([]);
  const [shelfWidth, setShelfWidth] = useState(BOOKSHELF_WIDTH);

  useEffect(() => {
    if (!visibleStories.length) return;

    const minBooksPerLine = Math.min(
      visibleStories.length / 2,
      MIN_BOOKS_PER_LINE
    );
    const width = BOOKSHELF_WIDTH / (MIN_BOOKS_PER_LINE / minBooksPerLine);
    setShelfWidth(width);

    setChunkedStories(
      generate(visibleStories, minBooksPerLine, MAX_BOOKS_PER_LINE).map(
        (chunk) => {
          const chunkWithRandomizedSizes = chunk.map((story) => ({
            ...story,
            width: getRandomizedWidth(chunk.length, width),
            height: getRandomizedHeight(),
          }));
          const widthsSum = chunkWithRandomizedSizes.reduce(
            (sum, story) => sum + story.width,
            0
          );
          const chunkWithNormalizedWidths = chunkWithRandomizedSizes.map(
            (story) => ({ ...story, width: (story.width / widthsSum) * width })
          );
          return chunkWithNormalizedWidths;
        }
      )
    );
  }, [visibleStories]);

  return (
    <BookshelfContainer width={shelfWidth}>
      {chunkedStories.map((storyChunk, chunkIndex) => (
        <BookshelfShelfContainer
          key={chunkIndex}
          height={BOOKSHELF_SHELF_HEIGHT}
        >
          {storyChunk.map((story, index) => (
            <BookshelfBookContainer
              width={story.width}
              height={story.height}
              title={story.title}
              onClick={() => (window.location.href = `story/${story.filename}`)}
              key={index}
            >
              {story.title}
              <BookshelfBookDetails
                key={index}
                title={story.title}
                author={story.author}
                // -1 for the last chunk unless there is only one, index of chunk for the rest
                location={
                  chunkIndex === chunkedStories.length - 1
                    ? Math.sign(-(chunkedStories.length - 1))
                    : chunkIndex
                }
                dimensions={{
                  width: story.height * BOOK_MAGNIFICATION_FACTOR * 0.6,
                  height: story.height * BOOK_MAGNIFICATION_FACTOR,
                  depth: story.width * BOOK_MAGNIFICATION_FACTOR,
                }}
              />
            </BookshelfBookContainer>
          ))}
        </BookshelfShelfContainer>
      ))}
    </BookshelfContainer>
  );
};

export default Bookshelf;
