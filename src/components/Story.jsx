import React from "react";
import ReactMarkdown from "react-markdown";
import { useParams } from "react-router-dom";

import { loadStory } from "actions/stories";

import {
  StoryContainer,
  StoryHeaderContainer,
  StoryTitle,
  StoryAuthor,
  StoryTags,
  StoryContent,
  StoryBackButton,
} from "assets/styles/Story.styles.jsx";

const Story = () => {
  const { filename } = useParams();
  const story = loadStory(filename);

  return (
    <StoryContainer>
      <StoryHeaderContainer>
        <StoryTitle>{story.title}</StoryTitle>
        {story.author && <StoryAuthor>{story.author}</StoryAuthor>}
        {story.tags && (
          <StoryTags>
            {story.tags.map((tag) => `[${tag}]`).join(", ")}
          </StoryTags>
        )}
      </StoryHeaderContainer>
      <StoryContent>
        <ReactMarkdown>{story.content}</ReactMarkdown>
      </StoryContent>
      <StoryBackButton href="/">{`< Back`}</StoryBackButton>
    </StoryContainer>
  );
};

export default Story;
