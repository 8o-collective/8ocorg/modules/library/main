import React, { useState, useEffect } from "react";

import {
  SearchContainer,
  SearchDropdown,
  SearchTags,
  SearchAuthor,
  SearchTitleInput,
  SearchClearButton,
} from "assets/styles/Search.styles.jsx";

const searchByOptions = ["tag", "author", "title"];

const Search = ({ authors, tags, stories, setVisibleStories }) => {
  const [searchBy, setSearchBy] = useState("");

  const [tag, setTag] = useState("");
  const [author, setAuthor] = useState("");
  const [title, setTitle] = useState("");

  const resetSearch = () => {
    setTag("");
    setAuthor("");
    setTitle("");
    setSearchBy("");

    setVisibleStories(stories);
  };

  useEffect(() => {
    setVisibleStories(stories);

    if (searchBy === "tag" && tag !== "") {
      setVisibleStories(stories.filter((story) => story.tags.includes(tag)));
    } else if (searchBy === "author" && author !== "") {
      setVisibleStories(stories.filter((story) => story.author === author));
    } else if (searchBy === "title" && title !== "") {
      setVisibleStories(stories.filter((story) => story.title.includes(title)));
    }
  }, [searchBy, tag, author, title]);

  return (
    <SearchContainer>
      <SearchDropdown
        header="Search by..."
        item={searchBy}
        items={searchByOptions}
        setItem={setSearchBy}
      />
      {searchBy === "tag" && (
        <SearchTags selectedTag={tag} tags={tags} setTag={setTag} />
      )}
      {searchBy === "author" && (
        <SearchAuthor
          selectedAuthor={author}
          authors={authors}
          setAuthor={setAuthor}
        />
      )}
      {searchBy === "title" && (
        <SearchTitleInput onChange={(e) => setTitle(e.target.value)} />
      )}
      <SearchClearButton onClick={() => resetSearch()}>Clear</SearchClearButton>
    </SearchContainer>
  );
};

export default Search;
