import React, { useState, useEffect } from "react";

import { AppContainer } from "assets/styles/App.styles.jsx";

import Bookshelf from "components/Bookshelf.jsx";
import Search from "components/Search.jsx";

import { loadStories } from "actions/stories";

const DEFAULT_AUTHOR = "nescio nomen";

Array.prototype.center = function (element) {
  const array = this.filter((e) => e !== element);
  return [
    ...array.slice(0, Math.floor(array.length / 2)),
    element,
    ...array.slice(Math.floor(array.length / 2)),
  ];
};

Array.prototype.shuffle = function () {
  return this.sort(() => Math.random() - 0.5);
};

const App = () => {
  const [stories, setStories] = useState([]);
  const [visibleStories, setVisibleStories] = useState([]);
  const [authors, setAuthors] = useState([]);
  const [tags, setTags] = useState([]);

  useEffect(() => {
    const loaded = loadStories({ author: DEFAULT_AUTHOR }).shuffle();
    const uniqueAuthors = [...new Set(loaded.map((story) => story.author))]
      .shuffle()
      .center(DEFAULT_AUTHOR);
    const uniqueTags = [...new Set(loaded.map((story) => story.tags).flat())]
      .shuffle()
      .filter((tag) => tag !== null);
    setStories(loaded);
    setVisibleStories(loaded);
    setAuthors(uniqueAuthors);
    setTags(uniqueTags);

    // 100 example stories
    // setStories([...Array(100).keys()].map((i) => ({
    //   title: `Story ${i}`,
    //   author: `Author ${i}`,
    // })).sort(() => Math.random() - 0.5));
  }, []);

  return (
    <AppContainer>
      <Bookshelf visibleStories={visibleStories} />
      <Search
        authors={authors}
        tags={tags}
        stories={stories}
        setVisibleStories={setVisibleStories}
      />
    </AppContainer>
  );
};

export default App;
