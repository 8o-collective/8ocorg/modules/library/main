// const webpack = require("webpack");
const path = require("path");
const fs = require("fs");

const createBuildTemplate = require("template");

const appDirectory = fs.realpathSync(process.cwd());
const resolveAppPath = (relativePath) =>
  path.resolve(appDirectory, relativePath);

module.exports = createBuildTemplate(resolveAppPath, {
  module: {
    rules: [
      {
        test: /\.(jpg|png)$/,
        use: "file-loader",
      },
      {
        test: /\.md$/,
        use: "raw-loader",
      },
    ],
  },
});
